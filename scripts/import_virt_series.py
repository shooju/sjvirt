"""
Writes virtual series into Shooju. To write series into Shooju source must implement list_series() function.
"""

import argparse
from shooju import Connection
import os
from virt import config
from sources import write_source_series

parser = argparse.ArgumentParser('Write virt series into Shooju')
parser.add_argument('--api-key', required=True, help='Shooju api key')
parser.add_argument('--user', required=True, help='Shooju username')
parser.add_argument('--source', required=False, help='Source name')

args = parser.parse_args()

if __name__ == '__main__':
    sj = Connection(config.REMOTE_URL, args.user, args.api_key)
    print('start writing virtual series...')
    sources = [f[:-3] for f in os.listdir('./sources') \
               if os.path.isfile(os.path.join('./sources', f)) and f.endswith('.py') and not f.startswith('__')]
    series_to_write = []
    for src in sources:
        if src == 'test':  # this is for internal tests
            continue
        if args.source and args.source != src:
            continue
        with sj.register_job('VirtDB {} source'.format(src), batch_size=300) as job:
            write_source_series(job, src)
        print('{} written'.format(src))

