## Background

SJVirt is a proxy and virtualization application for Shooju.  It is deployed as a [Docker](https://www.docker.com/) container.  The main features:

* authenticates via API Keys or via Shooju Web authentication,
* proxies all Web and API Shooju requests (e.g. shooju.customer.internal.com),
* allows series points to be retrieved in the proxy app, thereby acting as a virtualization layer.


## Getting started

First, [install Docker](https://docs.docker.com/engine/installation/#server). Make sure you have enough disk space allocated to Docker. Recommended volume size is >30G.

Next, use one of the methods below to run the SJVirt docker container:

#### Option 1: Single-line install:

1. [Make sure](https://docs.docker.com/compose/install/) `docker compose` (old versions) or `docker compose` command is working.
2. Use the command below to install. The command will walk through an interactive prompt to make setup easier.

```
    curl -0 https://bitbucket.org/shooju/sjvirt/raw/master/install.sh > install.sh && bash install.sh
```

Notes:

* After install, review and edit the configuration at `<install path>/docker-compose.yml` as needed.
* To upgrade or pull a different docker image, use the commands below (make sure to run `docker-compose restart` after a pull to restart the service).  When switching between tags, set the `image` to the full `shooju/shooju-virt:<tag>` in `<install path>/docker-compose.yml` to correspond, or use just `shooju/shooju-virt` for stable.

```
    docker pull shooju/shooju-virt
    docker pull shooju/shooju-virt:beta
    docker pull shooju/shooju-virt:alpha
```
    
* Use the `docker-compose` (old Docker versions) or `docker compose` command to manage the SJVirt containers.  This is most frequently used to restart the service after a configuration change or docker image pull.

```
    docker compose up -d
    docker compose stop
    docker compose restart
```

#### Option 2: Manual installation (best for local source development)

1. Install `docker-compose` (for old Docker version) or make sure `docker compose` is working.
2. Download `https://bitbucket.org/shooju/sjvirt/raw/master/docker-compose.yml` to a local folder
3. Edit docker-compose.yml file. See settings below.
4. Review `virt_app.volumes` and chose local mapping if needed.  If you use sources, you must place this file in the sources folder: `https://bitbucket.org/shooju/sjvirt/raw/master/sources/__init__.py`.  If you use logs, you must mount the actual file (any empty file).
5. Run `docker compose up`

* `${VIRT_REMOTE_URL}` - URL to Shooju account to be virtualized (REQUIRED, ex: `https://account.shooju.com/`. Easiest to test with: `https://test1.shooju.com/`)
* `${VIRT_LOCAL_URL}` - URL to access SJVirt application. Usually this is an address of the host where application is deployed. If the application is going to be accessed via load balancer, then it must be load balancer's address. (REQUIRED, ex: `http://localhost:81/`) - if you want to use a port other than 81 make sure to change it in `services.nginx.ports` as well
* `${VIRT_CUSTOM_OPERATORS}` - space separated list of custom operators
* `${EXP_RUNNER_USER}` - expression runner Shooju user (OPTIONAL, if you need expressions)
* `${EXP_RUNNER_API_KEY}` - expression runner Shooju user api key (OPTIONAL, if you need expressions)

#### Option 3: Build the image (best for core SJVirt development):

1. Clone this repository
2. In the sjvirt directory, run `docker build . -t shooju/shooju-virt` to build
3. Start from step #3 above.


## Custom sources

Custom sources define a connection to a internal source that contains series points.  For a custom series to work, the actual series with fields must be first written to Shooju.  Basic steps to define a custom source are below.  This assumes you have mapped the sources folder locally.

1. In the sources directory, make a file named using lower_case_letters_with_dashes.py - you can use `https://bitbucket.org/shooju/sjvirt/raw/master/sources/example.py` as a template
2. Edit the file and implement the two functions there; the changes are "live" as soon as they are saved
3. Check the log file or docker compose STDOUT for errors on save to ensure there were no errors in the python code
4. Keep in mind in list_series() the vrt field is required and it must have 'source' set to lower_case_letters_with_dashes that you named the file (TODO force automatically)
5. For list_series(), the 'sid' is what the shooju series ID will be. We recommend making a team for virt writers called "virt" and writing to `teams\virt\<name of py file without .py>\<whatever ID`> as a clean naming structure that anyone in the virt team can write to
6. To actually write the series, use the command below:
`sudo docker exec virt_app python scripts/import_virt_series.py --user=xxx --api-key=xxx --source=<name of py file without .py>`
7. Once the series are written, you will be able to query them using any client that points to the local sjvirt endpoint.  For example, you can use something like this in python:

```
#!python

from shooju import Connection
from datetime import date
sj = Connection('http://localhost:81/','username','api_key')
print sj.get_points('users\\sj.maxim\\virt\\1', date(2010,1,1))
```



## Additional Python libraries

To install additional Python modules (i.e. extend the site-packages directory):

1. Create a virtualenv by using `virtualenv venv`
2. Mount the site-packages folder into `/packages` by using the volumes setting in docker compose.yml like so: `"./venv/Lib/site-packages:/packages"`
3. New package installations do not require docker restart unless the module was already imported.  Restart using `docker compose restart virt_app`

## Calling custom source functions via the API

Source functions can be called via `POST /api/1/source/<source>/invoke/<func_name>` API.
As an example, see `foo()` function in `example` source. The function must be wrapped with @invocable decorator.
The decorator `teams` parameter declares the list of users teams whose members are allowed to invoke this function.
The function has the following signature:

```python
from sjtoolkit.utils.decorators import invocable


@invocable(teams=['_admin'])
def foo(virt_params, a, b=0):
    return {'sum': a + b}

```

The function can be invoked by the following request:

```http
POST /api/1/source/example/invoke/foo
{
    "vrt": {"source": "example"},
    "args": [10],
    "kwargs": {"b": 20}
}

```

API response would be:

```json
{
    "success": true,
    "result": {
        "sum": 30
    }
}

```

## On-Disk cache

Some heavy long running functions can be cached using `sjtoolkit.utils.decorators.cached` decorator. The decorator requires ttl=N parameter to be passed.
The size of underlying on-disk cache storage can be changed by setting `VIRT_CACHE_SIZE_MB` environment variable, it defaults to 1Gb.

Usage example:

```
#!python

from sjtoolkit.utils.decorators import cached

@cached(ttl=60)
def long_running_function():
    ....


```

SJVirt admins are permitted to clean cache via `POST /sjvirt/clear_cash`. SJVirt admins are users that participate teams listed in `VIRT_ADMIN_TEAM` environment variable.
It defaults to `_admin.


## Other settings

- docker-compose.yaml / `expression_runner.mem_limit` (default: 4G, examples: 4G, 2048M)
Limits memory usage on the expression runner to avoid bringing down the entire node when running RAM-intensive expressions. 
 A good idea is to leave 2gb for other SJVirt processes.  So if the node has 8gb, you can try setting this to 6gb.


## Troubleshooting

### Slowness / Gateway timeout error 504

SJVirt forwards API requests to the Shooju upstream server. Connectivity issues between SJVirt and Shooju servers may cause slowness (SJVirt retries some requests internally) and failures. 
To diagnose such issues try the following:

1. Check the xxx.shooju.com host directly. If it's unreachable directly it may be a Shooju or network issue.
2. SJVirt monitor process calls the /ping endpoint and writes timings to Shooju series under `sid:users\sjvirt.monitor l6=latency category_tree=ping not host=localhost` - please refer to views set up in Shooju to query this data
3. Check logs with the commands below (these commands dump logs from containers to 3 separate files):

```shell
docker logs virt_app  2>&1 |> virt_app.log
docker logs virt_expression_runner  2>&1 |> virt_xpr_runner.log
docker logs virt_callables_runner  2>&1 |> virt_callables_runner.log 
```

Below are examples of connectivity errors in the logs:

```shell
2020-04-28 19:00:26,132 - urllib3.connectionpool - WARNING - Retrying (Retry(total=2, connect=None, read=None, redirect=None, status=None)) after connection broken by 'ProtocolError('Connection aborted.', RemoteDisconnected('Remote end closed connection without response'))': //api/1/series?date_format=milli&v=2.0.13&location_type=python
2021-10-28 18:39:03,030 - shooju_client - WARNING - failed to perform request to POST https://xxx.shooju.com//api/1/jobs/?: HTTPSConnectionPool(host='xxx.shooju.com', port=443): Max retries exceeded with url: //api/1/jobs/?v=3.5.1&location_type=python (Caused by NewConnectionError('<urllib3.connection.VerifiedHTTPSConnection object at 0x7f78ee7a37d0>: Failed to establish a new connection: [Errno -2] Name or service not known')). will retry in 3.0s
```

4. Check virt sources slow log. Slowness maybe caused by a slow `virt` source (e.g some database being queried with poorly optimized queries). SJVirt logs this in virt_app logs:

```shell
2018-11-21 12:52:33,518 - SLOW - INFO - took 20.0s. to execute zzz.get_points(series_id=u'....', virt_params={}, df=datetime.datetime(2018, 11, 21, 12, 52, 33, 518488), dt=datetime.datetime(2018, 11, 21, 12, 52, 33, 518505), max_points=-1, include_timestamp=True, operators=None, reported_date=datetime.datetime(2018, 11, 21, 12, 52, 33, 518514), snapshot_date=datetime.datetime(2018, 11, 21, 12, 52, 33, 518523))
```

5. Check CPU utilization of the containers. Either via CPU series written by virt_monitor or manually with the `docker stats` command:

e.g on the below snippet `virt_expression_runner` took almost 100% of CPU. On a multicore CPU max utilization will be 100*<number of cores>. 100% utilized CPU accompanied by failing requests may be an indication of issues in the container. A restart of the container or host could be a quick fix in this case:

```
2ac84411bc1b   virt_expression_runner           92.00%    533.1MiB / 4.821GiB   10.80%    184kB / 11.4kB    11.3MB / 492kB    4
cfb974acebd3   virt_callables_runner            0.00%     45.43MiB / 2GiB       2.22%     1.02kB / 0B       516kB / 86kB      3
c62dc8cc581b   virt_app                         0.01%     82.75MiB / 4.821GiB   1.68%     1.02kB / 0B       3.52MB / 205kB    7
b6f2ab469d39   virt_monitor                     0.30%     52.1MiB / 4.821GiB    1.06%     21.9kB / 4.72kB   15.6MB / 86kB     7
```


### Expressions not working. Errors like "expression runner failed to process request". 

- Check if expression runner is running at <virt host>:/sjvirt/info. It should print `xpr runner working: True`. 
- Make sure `virt_expression_runner` container is running with `docker ps virt_expression_runner`.
- Check `virt_expression_runner`'s logs using `docker logs virt_expression_runner`. 
- Try restarting the container with `docker restart virt_expression_runner`.
- Make sure CPU/RAM utilization of `virt_expression_runner` looks good. 

### Callables not working. 

- Check if callables runner is running at <virt host>:/sjvirt/info. It should print `callables runner status: OK`. 
- Make sure `virt_callables_runner` container is running with `docker ps virt_callables_runner`.
- Check `virt_callables_runner`'s logs using `docker logs virt_callables_runner`. 
- Try restarting the container with `docker restart virt_callables_runner`.
- Make sure CPU/RAM utilization of `virt_callables_runner` looks good.

### Nothing working/Bad gateway 502 error

- Make sure `virt_app` container is running with `docker ps virt_app`. 
- Check `virt_app`'s logs using `docker logs virt_app`. 
- Try restarting container with `docker restart virt_app`.
- Make sure CPU/RAM utilization of `virt_app` looks good. 

App may fail to start due to disk space issues

- Check `df -H /` and `lvdisplay` for volumes with `Used` that are close to ~100%; clean up if necessary.
- Check for leaked disk space *in* containers: `docker system df -v`. If disk ran out because of docker containers, try removing containers and restarting the service:

```
docker rm -f $(docker ps -aq)
docker rmi $(docker images -q)
docker system prune
```

