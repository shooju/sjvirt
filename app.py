from gevent import monkey

monkey.patch_all()

import json
from copy import deepcopy
from importlib import import_module
import logging
from collections import defaultdict
import os
import subprocess
import re
import six
from six.moves.urllib.parse import urlparse
from six.moves import reload_module
import time

import requests
from flask import Flask, Response, stream_with_context, request, redirect, Request as _RequestClass, g, send_file
from gevent import pywsgi
from shooju import Connection
import numpy
from numpy.lib.recfunctions import append_fields
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from sjtoolkit.exceptions import NotAllowedException, UnexpectedException, ExpressionException
from sjtoolkit.series.args_parse import parse_fields, parse_series_id
from sjtoolkit.series.date_params_operators import parse_df_dt_with_operators
from sjtoolkit.utils import sentry
from sjtoolkit.utils.args_parse import parse_int, parse_bool
from sjtoolkit.utils.decorators import memoize
from sjtoolkit.utils.utils import is_relative, is_iso, id_generator
from sjtoolkit.utils.trace import UpstreamRequestContext, Limits
from sjtoolkit.utils.sjutils import BytesCallResponse
import msgpack

from virt import config
from sources import write_source_series
from virt.utils import (
    milli_to_datetime,
    render_date,
    agg_by_field_points_list,
    PARSE_DATE_OPERATORS,
    monitor_execution, SeriesParams,
)
from virt.upstream import upstream_call, load_obj, dump_obj, get_account_settings
from virt.exceptions import (
    BadRequest,
    DateParseException,
    InvalidParametersException,
    RelativeDateRangeParseException,
    ShoojuException,
    ImplementationException,
    SourceException,
)

from virt.metrics import init_metrics_reporting, metrics

from sjtoolkit.series.charts import (
    charts_data_based_on_settings,
    series_data_to_chart_table,
    generate_xlsx_file,
    generate_columns_xlsx_file,
    get_columns_data,
    viz_charts_data,
    series_data_to_chart_table_viz,
)

from sjtoolkit.expressions.cache import DiskCacheBackend
from sjtoolkit.expressions.client import (
    CachedRemoteExpressionCalculator,
    ExpressionPointsBatchReader
)

from sjtoolkit.utils.utils import (
    camel_to_underscore,
    series_query_to_sid,
    parse_date,
    generate_pts_stats,
    get_request_auth,
    MsgPack,
)
from sjtoolkit.series.suffix_processors import (
    post_process_points,
    adjust_dates,
    determine_auto_agg,
    NO_AGG_NEEDED,
    LocalizeOperator, AGGREGATE_LEVELS, BaseOperator)

from sjtoolkit.utils.decorators import cached, Invocable

try:
    from uwsgidecorators import postfork  # able to import if running in uwsgi
except ImportError:
    postfork = lambda f: f()  # immediately connect if running develop server or some other non-uwsgi script

EXPRESSION_RUNNER_ERROR_MSG = 'SJVirt Expression Runner not available;' \
                              ' please contact %s if this persists' % config.LOCAL_CONTACT

log_level = (os.environ.get('VIRT_LOG_LEVEL') or 'INFO').upper()

# init logging
log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=log_fmt, level=log_level)

fh = logging.FileHandler(f'logs/app.log')
fh.setLevel(log_level)
fh.setFormatter(logging.Formatter(fmt=log_fmt))
logging.getLogger().addHandler(fh)
logger = logging.getLogger('virt')
logger.setLevel(log_level)

slow_logger = logging.getLogger('SLOW')

logging.getLogger('apscheduler.executors.default').setLevel(logging.WARNING)

init_metrics_reporting('virt_app', config.REMOTE_URL, config.MON_WRITER_USER, config.MON_WRITER_API_KEY)

# todo meta?
# todo what is valid min/max date for each source?
# todo how to auth? in some cases we don't even touch shooju...should we keep track of users? - leave it for now...maybe some sessions
# todo source should be able to get bulk, not one by one

try:
    APP_REVISION = subprocess.Popen(['git', 'rev-parse', 'HEAD'],
                                    cwd=os.path.dirname(__file__),
                                    stdout=subprocess.PIPE).communicate()[0].strip().decode('utf-8')
except (OSError, IOError):
    APP_REVISION = None

app = Flask(__name__)
if not config.DISABLE_SENTRY:
    sentry.init_sentry(dsn='https://a9d541d7cfab4e5c8e16049232345c48:4fb4931999914395ae61c4a96d149d84@sentry.io/269550')

sources_by_name = dict()
sources_last_modified = defaultdict(int)

# this is just to fill some virt series

app.secret_key = config.SECRET_KEY

cached.reset_settings(directory=config.CACHE_LOCATION, size_mb=config.CACHE_SIZE_MB)

_missing = object()
ALL_POINTS = -1
DEFAULT_LOCALIZE_DEFAULT_FIELD = 'timezone'
ADMIN = '_admin'
PROCESSOR_ADMIN = '_processor_admin'
CALLERS = 'callers'


monitored_upstream_call = monitor_execution(upstream_call, slow_log_threshold=config.SLOW_LOG_THRESHOLD)


class RequestClass(_RequestClass):
    def on_json_loading_failed(self, e):
        """Returns none if request has no data, otherwise raises exception."""
        if self.data.strip():
            msg = "invalid json payload: "
            msg += self.data[:100]
            if len(self.data) > 100:
                msg += ' ...'
            raise BadRequest(msg)

    def get_json(self, force=False, silent=False, cache=True):
        # TODO checking the first character is { is not transparent for API user
        # return self.get_json(force=self.data is not None and self.data[0]=='{')
        if cache and self._cached_json[silent] is not Ellipsis:
            return self._cached_json[silent]

        deserializer = self._get_deserializer()
        request_charset = self.mimetype_params.get('charset')

        data = self._get_data_for_json(cache=cache)
        try:
            req = deserializer.loads(data, encoding=request_charset) \
                if request_charset is not None else deserializer.loads(data)
        except ValueError as e:
            if not silent:
                req = self.on_json_loading_failed(e)
                _, silent_req = self._cached_json
                self._cached_json = (req, silent_req)
            else:
                req = None
                normal_req, _ = self._cached_json
                self._cached_json = (normal_req, req)
        else:
            self._cached_json = (req, req)

        if req is not None and not isinstance(req, dict) and not silent:
            raise BadRequest('invalid json payload: json body must contain object')
        return req

    def _get_deserializer(self):
        send_format = self.headers.get('Sj-Send-Format', 'json')
        if send_format == 'msgpack':
            return MsgPack

        return json   # json is default


app.request_class = RequestClass


def remove_custom_operators_from_sid(series_id):
    """Removes custom virt operators to avoid sending it with relay api call"""
    if not series_id:
        return
    series_id, operators = parse_series_id(series_id, skip_validate=True)
    operators = [o for o in operators if o.split(':', 1)[0].lower() not in config.CUSTOM_OPERATORS]
    if not operators:
        return series_id
    else:
        return u'{}@{}'.format(series_id, '@'.join(operators))


def wrap_series_params(params, post=False, add_fields=None):
    """
    Adds extra fields to request url parameters or request json.  By default on each /series call adds <virt_field>.

    :param params: mapping with url parameters or json body
    :param post: Indicates this is json body
    :param add_fields: comma delimited list of fields to add.
    """
    add_fields = add_fields or []
    params = deepcopy(params)

    if post:
        for s in params.get('series', []):
            if add_fields:
                if s.get('fields') and '*' not in s['fields']:
                    s['fields'] = '{},{}'.format(s['fields'], ','.join(add_fields))
                elif not s.get('fields'):
                    s['fields'] = ','.join(add_fields)
            s['series_id'] = remove_custom_operators_from_sid(s.get('series_id'))

        if params.get('series_ids'):
            params['series_ids'] = list(map(remove_custom_operators_from_sid, params['series_ids']))

    if params.get('series_id'):
        params['series_id'] = remove_custom_operators_from_sid(params['series_id'])

    if add_fields:
        if params.get('fields') and '*' not in params['fields']:
            params['fields'] = '{},{}'.format(params['fields'], ','.join(add_fields))
        elif not params.get('fields'):
            params['fields'] = ','.join(add_fields)

    if params.get('operators'):
        params['operators'] = '@'.join([o for o in params['operators'].split('@') \
                                        if o.lower().split(':', 1)[0] not in config.CUSTOM_OPERATORS])

    return params

import typing

class XPRRequest(typing.NamedTuple):
    """Expression request"""
    xpr: str
    df: str
    dt: str
    date_format: str
    max_points: int
    g_expression: str
    operators: typing.List[str]
    fields: typing.List[str]
    include_job: bool
    include_timestamp: bool
    is_xpr_series: bool
    debug: bool
    use_upstream_fields: bool
    upstream_series: dict
    pts_params: SeriesParams
    sid: str


class ShoojuSeriesResponseHandler(object):
    def __init__(self, series_level_exception=False):
        self._series_level_exception = series_level_exception

        self._expressions_bulk_reader = ExpressionPointsBatchReader(
            calculator=cached_remote_expression_calculator,
            account_host=config.REMOTE_URL,
            expr_runner_url=config.EXPRESSION_RUNNER_HOST,
            datatype='series',
            auth=get_request_auth(request),
            upstream_request_context=g.upstream_request_context,
            debug=True,
            account_settings=get_account_settings(),
        )


    def handle(self, response, json_body=None, url_params=None):
        json_body = json_body or dict()
        url_params = url_params or dict()
        if 'series' not in response:  # a bit unexpected response...maybe error?
            return

        series = response['series']

        # generate params per series. combine common url with series level in json
        series_params = self._generate_series_params(json_body, url_params, series)

        results = []

        virt_point_batch_reader = VirtPointBatchReader()
        requests_by_idx = dict()
        for idx, (ser, s_params) in enumerate(zip(series, series_params)):
            fields = ser.get('fields', {})
            if 'error' in ser:
                results.append([ser, None])
                continue

            series_id = s_params.get('series_id') or ser['series_id']  # s_param may not contain sid in case of search

            try:
                ser, series_id = self._postprocess_response(virt_point_batch_reader.for_request(idx), ser, s_params,
                                                            fields, series_id)
                if isinstance(ser, XPRRequest):
                    requests_by_idx[idx] = ser
            except Exception as e:
                ser, series_id = self._handle_exception(e)

            results.append([ser, series_id])

        self._expressions_bulk_reader.fetch()

        for idx, xpr_request in requests_by_idx.items():
            ser = self._expressions_bulk_reader.get_response(
                xpr_request.xpr,
                df=xpr_request.df,
                dt=xpr_request.dt,
                date_format="milli",
                max_points=xpr_request.max_points,
                g_expression=xpr_request.g_expression,
                operators=xpr_request.operators,
                fields=xpr_request.fields,
                include_job=xpr_request.include_job,
                include_timestamp=xpr_request.include_timestamp,
                is_xpr_series=xpr_request.is_xpr_series,
                sid=xpr_request.sid,
                debug=xpr_request.debug,
            )

            if isinstance(ser, Exception):
                ser, series_id = self._handle_exception(ser)
                results[idx] = [ser, series_id]
            else:
                _, series_id = results[idx]
                results[idx] = [
                    SeriesSingleFormulaCalculator.post_process_expression_response(
                        seriess=ser.get('series') if ser else None,
                        is_series_xpr=xpr_request.is_xpr_series,
                        xpr=xpr_request.xpr,
                        upstream_series=xpr_request.upstream_series,
                        pts_params=xpr_request.pts_params,
                        use_upstream_fields=xpr_request.use_upstream_fields,
                        upstream_request_context=g.upstream_request_context,
                        debug=xpr_request.debug,
                    ),
                    series_id,
                ]

        for idx, (ser, e) in six.iteritems(virt_point_batch_reader.fetch()):
            if e is not None:
                ser, series_id = self._handle_exception(e)
                results[idx] = [ser, series_id]
            else:
                _, series_id = results[idx]
                results[idx] = [ser, series_id]

        has_sampled_points = False
        for idx, ((ser, series_id), s_params) in enumerate(zip(results, series_params)):
            if series_id is not None and 'error' not in ser:
                ser['series_id'] = series_id
            if ser.get('points') is None or len(ser['points']) == 0:
                ser.pop('points', None)
            if ser.pop('has_sampled_points', False):
                has_sampled_points = True
            # need to add operators to series ids, including custom

            # remove vrt if user didn't requested it
            self._remove_fields_if_not_requested(ser, s_params)

            series[idx] = ser

        if has_sampled_points:
            response['sampled'] = True

    def _handle_exception(self, e):
        if not self._series_level_exception:
            raise e
        if isinstance(e, ShoojuException):
            error = camel_to_underscore(e.__class__.__name__).replace('_exception', '')
        else:
            error = 'unexpected-exception'
        metrics.count('error', error_type=f'bulk_get_series_level', error=error)
        ser = {'success': False, 'error': error, 'description': six.text_type(e)}
        if isinstance(e, ExpressionException) and e.trace:
            ser['trace'] = e.trace
        series_id = None

        return ser, series_id

    def _postprocess_response(self, virt_point_request_processor, ser, s_params, fields, series_id):
        g_expression = None
        xpr = None
        is_series_xpr = False
        points_params = SeriesParams.from_request_params(s_params, fields)
        max_points = points_params.pre_max_points
        fields_to_fetch = None
        use_upstream_fields = False

        if series_id.startswith('='):
            xpr = series_id
            g_expression = s_params.get('g_expression')
            fields_to_fetch = (s_params.get('fields') or '').split(',') or None
        elif fields.get('xpr'):
            xpr = fields['xpr']['expression']
            is_series_xpr = True
            g_expression = fields['xpr'].get('g')
            use_upstream_fields = True

        if xpr and (fields_to_fetch or max_points):
            self._expressions_bulk_reader.add_request(
                xpr,
                df=points_params.pre_df,
                dt=points_params.pre_dt,
                date_format="milli",
                max_points=points_params.pre_max_points,
                g_expression=g_expression,
                operators=points_params.non_custom_ops,
                fields=fields_to_fetch or [],
                include_job=points_params.include_job,
                include_timestamp=points_params.include_timestamp,
                is_xpr_series=is_series_xpr,
                debug=parse_bool(s_params.get('debug')),
                series_body={'fields': fields},
                sid=series_id if is_series_xpr else None
            )

            ser = XPRRequest(
                sid=series_id if is_series_xpr else None,
                xpr=xpr,
                df=points_params.post_df,
                dt=points_params.post_dt,
                date_format="milli",
                max_points=points_params.post_max_points,
                g_expression=g_expression,
                operators=points_params.non_custom_ops,
                fields=fields_to_fetch,
                include_job=points_params.include_job,
                include_timestamp=points_params.include_timestamp,
                is_xpr_series=is_series_xpr,
                debug=parse_bool(s_params.get('debug')),
                use_upstream_fields=use_upstream_fields,
                upstream_series=ser,
                pts_params=points_params,
            )

        elif config.VIRT_FIELD in fields \
                and isinstance(fields.get(config.VIRT_FIELD), dict) and points_params.pre_max_points:
            virt_point_request_processor.set_params(ser, ser['fields'][config.VIRT_FIELD], points_params)

        if not series_id.startswith('='):
            # we had to remove custom sjvirt operators before pass to the upstream,
            # so resulting series comes w/o custom operators, we putting it back
            series_id = series_id.split('@', 1)[0]
            # add custom operators to sid
            if points_params.custom_operators:
                series_id += ''.join(['@{}{}'.format(o[0], ':{}'.format(o[1]) if o[1] else '')
                                      for o in points_params.custom_operators])

            # add Shooju builtin operators to sid
            if points_params.non_custom_ops:
                series_id = u'{}@{}'.format(series_id, '@'.join([o for o in points_params.non_custom_ops]))

        return ser, series_id

    def _generate_series_params(self, json_body, url_params, series):
        if json_body and 'series' in json_body:
            series_params = list()
            common_params = dict(url_params)
            common_params.update({k: v for k, v in six.iteritems(json_body) if k in ['g_expression']})
            for s in json_body['series']:
                s_params = dict(common_params)
                s_params.update({k: v for k, v in six.iteritems(s) if v != 'series_id'})
                series_params.append(s_params)
        else:
            series_params = [dict(url_params) for _ in range(len(series))]

        return series_params

    def _remove_fields_if_not_requested(self, ser, s_params, ):
        if not ser.get('fields'):
            return
        fields_to_return, _ = parse_fields(s_params.get('fields', ''))
        localize_default_field = get_account_settings().get('localize_default_field') or DEFAULT_LOCALIZE_DEFAULT_FIELD
        for field in [config.VIRT_FIELD, localize_default_field, 'sid', config.XPR_FIELD]:
            if field not in ser['fields']:
                continue
            if not (fields_to_return and (field in fields_to_return or ('*' in fields_to_return and field != 'sid'))):
                ser['fields'].pop(field)

        if not ser['fields']:
            ser.pop('fields')  # we never return empty fields


def _get_points_tz(operators):
    if not operators:
        return

    # from daily and higher treated as naive
    if any([o.agg_level > AGGREGATE_LEVELS['h'] for o in operators if isinstance(o, BaseOperator) and o.agg_level]):
        return

    def _first_or_none(items):
        if items:
            return items[0]
        else:
            return None

    op_localize = _first_or_none(operators.get_by_name('localize'))
    op_force_tz = _first_or_none(operators.get_by_name('forcetz'))

    op_tz_source = op_force_tz if op_force_tz else op_localize
    if op_tz_source:
        tz = op_tz_source.get_timezone()
        return {'timezone': op_tz_source.get_timezone().zone} if tz is not None else None

def _ensure_datetime(dt, date_format):
    if dt is not None and not isinstance(dt, (six.integer_types, float)):
        dt = milli_to_datetime(parse_date(dt, date_format))
    elif isinstance(dt, (six.integer_types, float)):
        dt = milli_to_datetime(dt)
    else:
        dt = None
    return dt


class VirtPointRequestProcessor(object):
    """Requests params collector for further processing in VirtPointRequestProcessor"""
    def __init__(self, batch_reader, key):
        """

        :param batch_reader: parent batch reader
        :param key: request key that will be used to find result points
        """
        self._batch_reader = batch_reader
        self._key = key

    def set_params(self, ser, virt_params, points_params):
        """
        Setups parameters for virt points request

        :param ser: series response from upstream
        :param virt_params: virt params for points to read
        :param points_params: series params from user request
        """
        self._batch_reader.add_request(self._key, ser, virt_params, points_params)


class VirtPointBatchReader(object):
    """Virt fields batch reader"""
    def __init__(self):
        self._requests = {}

    def add_request(self, key, ser, virt_params, points_params):
        """
        Registers new request to fetch points for

        :param key: request key that will be used to find result points
        :param ser: series response from upstream
        :param virt_params: virt params for points to read
        :param points_params: series params from user request
        """
        self._requests[key] = [ser, virt_params, points_params]

    def for_request(self, key):
        """
        Creates new VirtPointRequestProcessor for request

        :param key: request key that will be used to find result points
        """
        return VirtPointRequestProcessor(self, key)

    @classmethod
    def _fetch_points_for_requests(cls, requests):
        errors_by_key = {}
        points_by_key = {}
        points_params_by_requests = defaultdict(list)

        for key, req in six.iteritems(requests):
            try:
                ser, _, points_params = req
                custom_operators = points_params.custom_operators

                df_adj, dt_adj, series_id, snapshot_date, reported_date = cls.get_virt_series_params(ser, points_params)

                params_key = (series_id, snapshot_date, reported_date, tuple(custom_operators), df_adj > dt_adj)
                points_params_by_requests[params_key].append([key, req])
            except Exception as e:
                errors_by_key[key] = e

        for requests_list in points_params_by_requests.values():
            keys = [it[0] for it in requests_list]
            reqs = [it[1] for it in requests_list]

            ser, virt_params, points_params = reqs[0]

            max_points = points_params.pre_max_points
            if len(reqs) > 1:
                max_points = ALL_POINTS

            include_timestamp = points_params.include_timestamp
            include_job = points_params.include_job
            custom_operators = points_params.custom_operators

            try:

                ser, _, points_params = reqs[0]
                df, dt, series_id, snapshot_date, reported_date = cls.get_virt_series_params(ser, points_params)
                is_reversed = df > dt
                min_dt, max_dt = min(df, dt), max(df, dt)

                for req in reqs[1:]:
                    ser, _, points_params = req
                    df_adj, dt_adj, _, _, _ = cls.get_virt_series_params(ser, points_params)
                    df_adj, dt_adj = min(df_adj, dt_adj), max(df_adj, dt_adj)
                    min_dt = min(df, df_adj)
                    max_dt = max(dt, dt_adj)

                if is_reversed:
                    min_dt, max_dt = max_dt, min_dt

                points = cls.read_mod_points(
                    virt_params,
                    snapshot_date,
                    reported_date,
                    max_points,
                    include_timestamp,
                    include_job,
                    custom_operators,
                    min_dt,
                    max_dt,
                    series_id,
                )
                for key in keys:
                    points_by_key[key] = points
            except Exception as e:
                for key in keys:
                    errors_by_key[key] = e

                if not config.DISABLE_SENTRY:
                    sentry.capture_exception(extra={
                        'version': APP_REVISION,
                        'user': request.authorization.username if request.authorization else None
                    })

        return points_by_key, errors_by_key

    def fetch(self):
        """
        Fetches points for all registered requests
        """
        res = {}
        points_by_key, errors_by_key = self._fetch_points_for_requests(self._requests)

        for key, req in six.iteritems(self._requests):
            if key in errors_by_key:
                res[key] = (None, errors_by_key[key])
                continue

            try:
                points = points_by_key[key]
                ser, _, points_params = req
                ser = self.postprocess_mod_points(ser, points_params, points)

                res[key] = (ser, None)
            except Exception as e:
                res[key] = (None, e)
        return res


    @staticmethod
    def get_virt_series_params(ser, points_params):
        """
        Returns parameters for virt points from request params

        :param ser: series response from upstream
        :param points_params: series params from user request
        """
        df_adj, dt_adj = adjust_dates(points_params.pre_df, points_params.pre_dt,
                                      points_params.operators, points_params.date_format)
        if 'sid' in ser.get('fields', {}):  # this could be $sid=... query, so let's take sid from fields
            series_id = ser['fields']['sid']
        else:
            series_id = ser['series_id']

        series_id = series_id.split('@')[0]
        snapshot_date = _ensure_datetime(points_params.snapshot_date, points_params.date_format)
        reported_date = _ensure_datetime(points_params.reported_date, points_params.date_format)

        return df_adj, dt_adj, series_id, snapshot_date, reported_date

    @staticmethod
    def read_mod_points(virt_params, snapshot_date, reported_date, max_points,
                        include_timestamp, include_job, custom_operators, df, dt, series_id):
        """
        Reads virt points from local module

        :param virt_params: virt params for points to read
        :param snapshot_date: series snapshot date
        :param reported_date: series reported date
        :param max_points: maximum number of points to retrieve
        :param include_timestamp: if set, retrieves the last time each point changed as the next element in the point array (will follow job id if `include_job` is not empty)
        :param include_job: if set, retrieves the latest job id that changed each point as the next element in the point array
        :param custom_operators: custom operators
        :param df: datetime to start the points array
        :param dt: datetime to finish the points array; note that `dt` can be less than `df` by design -- this will sort dates in reverse-chronological order
        :param str series_id: id of the series
        """
        if 'source' not in virt_params or virt_params['source'] not in sources_by_name:
            raise ShoojuException('unknown source "{}"'.format(virt_params.get('source', '')))

        mod = sources_by_name[virt_params['source']]

        try:
            with metrics.timer('virt_source_get_points', source=virt_params['source']):
                points = monitor_execution(mod.get_points, slow_log_threshold=config.SLOW_LOG_THRESHOLD)(
                    series_id, virt_params,
                    df=milli_to_datetime(df),
                    dt=milli_to_datetime(dt),
                    max_points=max_points,
                    include_timestamp=include_timestamp,
                    operators=custom_operators,
                    reported_date=reported_date,
                    snapshot_date=snapshot_date,
                )
        except Exception as e:
            metrics.count('error', error_type='virt_source_get_points', source=virt_params['source'])
            logger.exception('failed to execute {}.get_points', virt_params['source'])
            raise SourceException(f'source returned an error: {e}')

        if len(points) and include_timestamp and not all(len(p) == 3 for p in points):
            raise ImplementationException("source must return 3xN size array when include_timestamp=y")

        if not isinstance(points, numpy.ndarray):
            dtype = [('dates', 'i8'), ('values', 'f8'), ]
            if include_timestamp:
                for p in points:
                    if p[2] < 0:
                        raise ImplementationException("virt series points timestamps must be positive integers")
                dtype.append(('ts', 'u8'))

            points = numpy.array(list(map(tuple, points)), dtype=numpy.dtype(dtype))

        if include_job:
            jobs = numpy.zeros(len(points), dtype='i4')
            points = append_fields(points, 'jobs', jobs)

            if include_timestamp:
                points = points[['dates', 'values', 'jobs', 'ts']]

        return points

    @staticmethod
    def postprocess_mod_points(ser, points_params, points):
        """
        Postprocesses points from local source

        :param ser: series response from upstream
        :param points_params: series params from user request
        :param points: points from local source
        """
        operators, auto_agg = determine_auto_agg(points_params.operators, points)

        points, points_stats = post_process_points(points,
                                                   operators,
                                                   points_params.date_format,
                                                   df=points_params.post_df,
                                                   dt=points_params.post_dt,
                                                   include_job=points_params.include_job,
                                                   include_timestamp=points_params.include_timestamp,
                                                   sample_points_size=points_params.sample)

        if points_stats.get('has_sampled_points'):
            ser['has_sampled_points'] = True

        if points_params.post_max_points != ALL_POINTS:
            points = points[:points_params.post_max_points]

        if points_params.post_max_points:
            tz = _get_points_tz(operators)
            if tz:
                ser['tz'] = tz

        if points_params.pts_stats_fields and points is not None and len(points):
            fields = ser.setdefault('fields', {})
            fields.update({
                'pts.%s' % k: v for k, v in six.iteritems(generate_pts_stats(points, points_params.pts_stats_fields))
            })

        ser['points'] = points

        if auto_agg is not None:
            ser['auto_agg'] = '@{}'.format(auto_agg) if auto_agg != NO_AGG_NEEDED else None

        return ser


def cached_remote_expression_calculator(*args, **kwargs):
    kwargs.pop('account_id', None)
    kwargs['cache_ttl_limit'] = config.EXPRESSION_CACHE_TTL_LIMIT
    backend = DiskCacheBackend(config.EXPRESSION_CACHE_LOCATION, config.EXPRESSION_CACHE_SIZE_MB)
    return CachedRemoteExpressionCalculator(backend, *args, **kwargs)


def drop_series_expression_cache():
    backend = DiskCacheBackend(config.EXPRESSION_CACHE_LOCATION, config.EXPRESSION_CACHE_SIZE_MB)
    backend.drop_cache()


class SeriesSingleFormulaCalculator(object):
    """
    Return single formula series result.

    :param xpr: Shooju formula.
    :param series_params: Series level params (json['series'][N] or url parameters).
    :return:
    """

    def __init__(self, xpr, points_params, fields=None, g_expression=None,
                 upstream_series=None, use_upstream_fields=None, is_series_xpr=False,
                 series_body=None, debug=False):
        self._xpr = xpr
        self._is_series_xpr = is_series_xpr
        self._points_params = points_params
        self._g_expression = g_expression
        self._upstream_series = upstream_series
        self._fields_to_fetch = fields
        self._use_upstream_fields = use_upstream_fields
        self._series_body = series_body
        self._debug = debug
        self._upstream_request_context = g.upstream_request_context
        username, password = get_request_auth(request)
        self._auth = (username, password)

    def execute(self):
        """
        Return single formula series result.

        :param xpr: Shooju formula.
        :param series_params: Series level params (json['series'][N] or url parameters).
        :return:
        """
        operators = self._points_params.non_custom_ops
        account_settings = None
        if self._is_series_xpr:
            account_settings = get_account_settings()

        pts_params = self._points_params

        def _monitored_exec(*a, **k):
            # just to have a good log message with all params
            with metrics.timer('single_expression_exec'):
                try:
                    res = cached_remote_expression_calculator(*a, **k).execute()['series']
                except Exception as e:
                    metrics.count('error', error_type='single_expression_exec', error=e.__class__.__name__)
                    raise
            return res
        df, dt = pts_params.pre_df, pts_params.pre_dt

        seriess = monitor_execution(_monitored_exec, slow_log_threshold=config.SLOW_LOG_THRESHOLD)(
            self._xpr[1:],
            config.EXPRESSION_RUNNER_HOST,
            config.REMOTE_URL,
            datatype='series', auth=self._auth,
            df=df, dt=dt,
            date_format=pts_params.date_format,
            max_points=pts_params.pre_max_points,
            g_expression=self._g_expression,
            fields=self._fields_to_fetch,
            operators=operators,
            include_job=pts_params.include_job,
            include_timestamp=pts_params.include_timestamp,
            unavailable_error=EXPRESSION_RUNNER_ERROR_MSG,
            timeout=config.EXPRESSION_RUNNER_TIMEOUT,
            account_settings=account_settings,
            series_body=self._series_body,
            is_xpr_series=self._is_series_xpr,
            debug=self._debug,
            upstream_request_context=self._upstream_request_context
        )

        return self.post_process_expression_response(
            seriess,
            self._is_series_xpr,
            self._xpr,
            self._upstream_series,
            pts_params,
            self._use_upstream_fields,
            self._debug,
            self._upstream_request_context
        )


    @staticmethod
    def post_process_expression_response(
            seriess,
            is_series_xpr,
            xpr,
            upstream_series,
            pts_params,
            use_upstream_fields,
            debug,
            upstream_request_context,
    ):

        if not seriess and not is_series_xpr:
            return {'error': 'series_not_found'}
        elif not seriess:
            return upstream_series
        else:
            series = seriess[0]

        series['series_id'] = xpr
        points = None

        if len(series.get('points', [])):
            points, points_stats = post_process_points(
                series['points'], [],
                date_format=pts_params.date_format, df=pts_params.post_df, dt=pts_params.post_dt,
                include_timestamp=pts_params.include_timestamp, include_job=pts_params.include_job,
                sample_points_size=pts_params.sample)

            if pts_params.post_max_points == ALL_POINTS and len(points):
                series['points'] = points
            elif pts_params.post_max_points and len(points):
                series['points'] = points[:pts_params.post_max_points]
            else:
                series.pop('points', None)

            if points_stats.get('has_sampled_points'):
                series['has_sampled_points'] = True
        else:
            series.pop('points', None)

        if upstream_series:
            series.update({k: v for k, v in six.iteritems(upstream_series)
                           if k not in ['points', 'series_id', 'fields']})
        if use_upstream_fields and upstream_series.get('fields'):
            series['fields'] = upstream_series['fields']

        if pts_params.pts_stats_fields and points is not None and len(points):
            fields = series.setdefault('fields', {})
            fields.update({
                'pts.%s' % k: v for k, v in six.iteritems(generate_pts_stats(points, pts_params.pts_stats_fields))
            })
        if 'fields' in series and not series['fields']:
            series.pop('fields')

        if 'xpr' in series and not debug and upstream_request_context.is_root_request:
            series['xpr'].pop('trace', None)
        return series


def get_series_by_query_formula(query, params):
    """
    Returns array of query formula results

    :param query: Query formula.
    :param params: Series request url parameters.
    :return: Response and Flag indicating is response already serialized or not.
    """
    points_params = SeriesParams.from_request_params(params)
    per_page = parse_int(params.get('per_page'), 'per_page')

    operators = points_params.non_custom_ops
    try:
        username, password = get_request_auth(request)
    except NotAllowedException:
        raise InvalidParametersException('request credentials are not provided')
    is_debug = parse_bool(params.get('debug'),)

    calc = cached_remote_expression_calculator(
        query[1:],
        config.EXPRESSION_RUNNER_HOST,
        config.REMOTE_URL,
        datatype='dataframe',
        auth=(username, password),
        df=points_params.pre_df, dt=points_params.pre_dt, date_format=points_params.date_format,
        operators=operators, max_points=points_params.pre_max_points, g_expression=points_params.g_expression,
        fields=parse_fields(params.get('fields'))[0] or None,
        max_series=per_page, sort=params.get('sort'),
        include_timestamp=points_params.include_timestamp,
        include_job=points_params.include_job,
        unavailable_error=EXPRESSION_RUNNER_ERROR_MSG,
        timeout=config.EXPRESSION_RUNNER_TIMEOUT,
        is_xpr_series=False,
        debug=parse_bool(params.get('debug'),),
        upstream_request_context=g.upstream_request_context
    )

    with metrics.timer('query_expression_exec'):
        try:
            series_resp = calc.execute()
        except Exception as e:
            metrics.count('error', error_type='query_expression_exec', error=e.__class__.__name__)
            raise

    series = list()
    response = {k: v for k, v in six.iteritems(series_resp) if k not in ['series']}
    response.update({'series': series})

    for resp in series_resp['series']:
        ser = dict(series_id=resp['series_id'])
        ser.update({k: resp[k] for k in ['fields', 'xpr', 'tz', 'warnings'] if resp.get(k)})
        points = resp.get('points', [])

        if points is not None and len(points):
            points, points_stats = post_process_points(
                points, [],
                date_format=points_params.date_format, df=points_params.post_df,
                dt=points_params.post_dt, include_timestamp=points_params.include_timestamp,
                include_job=points_params.include_job, sample_points_size=points_params.sample)

            if points_stats.get('has_sampled_points'):
                response['sampled'] = True

            if points_params.post_max_points != ALL_POINTS:
                points = points[:points_params.post_max_points]
            ser.pop('points', None)
            if len(points):
                ser['points'] = points
                if points_params.pts_stats_fields:
                    fields = ser.setdefault('fields', {})
                    fields.update({
                        'pts.%s' % k: v for k, v in
                        six.iteritems(generate_pts_stats(points, points_params.pts_stats_fields))
                    })
        else:
            ser.pop('points', None)

        if 'xpr' in ser and not is_debug and g.upstream_request_context.is_root_request:
            ser['xpr'].pop('trace', None)

        series.append(ser)

    if params.get('facets') and series_resp.get('all_queries'):
        facets = facets_request(set(series_resp['all_queries']), params)
        if facets:
            response['facets'] = facets
    return response, False


def get_series_by_query(url_params):
    """
    Performs /series?query=.. or /series?scroll_id=.. requests

    :param url_params: Shooju call url parameters
    :return: Response and Flag indicating is response already serialized or not.
    """

    # prepare url params to relay. we need to remove AGG operators and add some field to perform AGG and virt if needed
    points_params = None
    if 'scroll_id' not in url_params:
        points_params = SeriesParams.from_request_params(url_params)
        if points_params.custom_operators:
            raise InvalidParametersException('source operators are not supported by search api')

        if url_params.get('operators'):
            # in this case we need to remove AGG operators. otherwise Shooju will aggregate w/o virt points
            params_operators = [o.strip() for o in url_params['operators'].split('@') if o]
            non_custom_ops = []
            for o in points_params.non_custom_ops:
                if o in params_operators:
                    params_operators.remove(o)
                    non_custom_ops.append(o)

            url_params['operators'] = "{}{}".format('@', '@'.join(o for o in non_custom_ops)) if non_custom_ops else None
            if not url_params['operators']:
                url_params.pop('operators')
        params = wrap_series_params(url_params,
                                    add_fields=[a[1] for a in points_params.agg_operators])
    else:
        params = url_params  # this is just scroll_id. we can pass it as is

    resp = monitored_upstream_call('/api/1/series', method='GET', params=params)

    if not has_virt_field(resp):  # we can just proxy this.
        return resp.content, True

    shooju_response = load_obj(resp.content, req=request)

    search_params = shooju_response['scroll_args'] if 'scroll_args' in shooju_response else url_params
    if points_params is None:
        points_params = SeriesParams.from_request_params(search_params)

    ShoojuSeriesResponseHandler().handle(shooju_response, url_params=search_params)
    for agg_type, agg_field in points_params.agg_operators:
        shooju_response['series'] = list(agg_by_field_points_list(shooju_response['series'],
                                                                  agg_field.lower(),
                                                                  agg_type))

    return shooju_response, False


def get_single_series(url_params):
    """
    Performs /series?series_id= Api call

    :param url_params: Shooju call url parameters
    :return: Response and Flag indicating is response already serialized or not.
    """
    resp = monitored_upstream_call('/api/1/series', params=wrap_series_params(url_params))

    if not has_virt_field(resp):  # nothing to do with it. just send result to client
        return resp.content, True
    resp = load_obj(resp.content, req=request)
    ShoojuSeriesResponseHandler().handle(resp, url_params=url_params)
    return resp, False


def bulk_get_series(post_data, url_params):
    """
    Performs bulk get series. Supports series sub-requests and array of series_ids.

    :return: Response and Flag indicating is response already serialized or not.
    """
    # =<expression> is a special case. we should not pass these into Shooju,
    # b/c Shooju is not able to calculate formula if it contains custom sources series
    # so we have to remove these series, before pass to shooju

    # to simplify code we convert POST series_ids: [] into POST series: []
    if 'series_ids' in post_data:
        post_data.update(
            {
                'series': [dict(series_id=s) for s in post_data.pop('series_ids')]
            }
        )

    non_formula_idx = list()  # mapping user request index to shooju request index
    combined_response = [None] * len(post_data.get('series_ids', []) or post_data.get('series', []))

    for i, s in enumerate(post_data['series']):
        series_id = s['series_id']
        if not series_id.startswith('='):
            non_formula_idx.append(i)
        else:
            combined_response[i] = {'series_id': series_id}

    # then we pass shooju series to shooju, calculate separately formulas and combine all of this into response
    if non_formula_idx:
        relay_body = {'series': [post_data['series'][k] for k in non_formula_idx]}

        resp = monitored_upstream_call('/api/1/series',
                                       body=wrap_series_params(relay_body, post=True),
                                       method='POST',
                                       params=wrap_series_params(url_params))

        # in case we have no expression and no virt we can just return as is
        if not has_virt_field(resp):
            return resp.content, True

        res = load_obj(resp.content, req=request)
    else:
        res = {'series': []}

    if 'series' not in res:  # seems api error
        return res, False

    # put response in our combined
    for i, r in zip(non_formula_idx, res['series']):
        combined_response[i] = r

    res.update(series=combined_response)
    response_handler = ShoojuSeriesResponseHandler(series_level_exception=True)
    response_handler.handle(res, json_body=post_data, url_params=url_params)

    res.update(total=len(combined_response), success=True)

    # we have to send separate request for facets
    if url_params.get('facets'):
        all_queries = set()
        for s in res['series']:
            if s.get('xpr') and s['xpr'].get('series_queries'):
                all_queries |= set(s['xpr']['series_queries'])
            elif s.get('series_id'):
                prefix = s['series_id'][0]
                if prefix == '$':  # series query prefix
                    q, _ = parse_series_id(s['series_id'][1:], skip_validate=True)
                    all_queries.add(q)
                elif prefix not in ['$', '=']:
                    q, _ = parse_series_id(s['series_id'], skip_validate=True)
                    all_queries.add(u'sid="%s' % q)

        if all_queries:
            facets = facets_request(all_queries, url_params)
            if facets:
                res['facets'] = facets
    return res, False


def facets_request(queries, params):
    """
    Executes facets requests against of queries. Returns `facets` key of response.
    """
    resp = monitored_upstream_call('/api/1/series',
                                   body={'series_queries': list(queries)},
                                   method='POST',
                                   params={k: v for k, v in six.iteritems(params) if
                                           k in ['facets', 'max_facet_values']})
    return load_obj(resp.content, req=request).get('facets')


@app.route('/')
@app.route('/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'])
def catch_all(path='/'):
    """
    Default app.route. Actually simple proxy between client and Shooju.
    """
    # proxy
    return proxy_request(path)


def _replace_redirect_host(url):
    """Adjust redirect location"""
    remote_host = urlparse(config.REMOTE_URL)
    local_host = urlparse(config.LOCAL_URL)
    url = url.replace(remote_host.netloc, local_host.netloc, 1)
    return url.replace(remote_host.scheme, local_host.scheme, 1)


def proxy_request(path=None):
    resp = monitored_upstream_call(path or request.path, stream=True)
    if 'Location' in resp.headers:
        resp.headers['Location'] = _replace_redirect_host(resp.headers['Location'])
    return Response(stream_with_context(resp.iter_content(config.CHUNK_SIZE)),
                    headers={k: v for k, v in resp.headers.items()
                             if k not in ['Content-Length',
                                          'Content-Encoding',
                                          'Transfer-Encoding']},
                    status=resp.status_code)


@app.route('/api/1/virt_sources', methods=['GET'])
def sources_list():
    """Returns list of loaded virt sources"""
    hits = [{'id': s} for s in sorted(sources_by_name)]
    return dump_obj({'hits': hits, 'total': len(hits), 'success': True, 'request_id': ''}, req=request)


@app.route('/api/1/virt_sources/<source>/write_list', methods=['POST'])
def write_list(source):
    """Writes virt series for given source"""
    if source not in sources_by_name:
        raise ShoojuException('unknown source {}'.format(source))

    username, password = get_request_auth(request)
    sj = Connection(config.REMOTE_URL, username, password)
    with sj.register_job('VirtDB {} source'.format(source), batch_size=1000) as job:
        write_source_series(job, source)

    return dump_obj({'job_id': job.job_id, 'success': True, 'request_id': ''}, req=request)


@app.route('/api/1/series/excel', methods=('POST',))
def series_export():
    if not request.json:
        raise InvalidParametersException("JSON body is required")

    chart_settings = request.json.get('chart_settings')
    columns = request.json.get('columns')
    viz = request.json.get('viz')

    transpose = parse_bool(request.args.get('transpose', ''))
    default_tab = int(request.args.get('tab', 1))
    logo_url = config.LOGO_URL

    if viz:
        table_data = _generate_export_data_based_on_viz_settings()
        chart_settings = request.json['viz'].get('chart') or dict()
        title = viz.get('title', 'Untitled') or ''
    elif chart_settings:
        table_data = _generate_export_data_based_on_chart_settings()
        chart_settings = request.json.get('chart_settings') or dict()
        title = chart_settings.get('title', 'Untitled') or ''
    elif columns:
        global_params = {k: request.args[k] for k in ['per_page', 'query', 'sort', 'page'] if request.args.get(k)}
        table_data = get_columns_data(columns, _iterate_charts_series, global_params)
        return generate_columns_xlsx_file(table_data)
    else:
        raise InvalidParametersException('chart_settings, viz or columns required')

    return generate_xlsx_file(table_data, chart_settings, transpose, default_tab, logo_url, title=title)


def _generate_export_data_based_on_chart_settings():
    """
    Generates excel table data based on chart settings
    :return:
    """
    try:
        max_points = request.args.get('max_points') or -1
        max_points = int(max_points)
    except (ValueError, TypeError):
        raise InvalidParametersException('max_points must be an integer')

    chart_settings = request.json.get('chart_settings')
    global_params = {k: request.args[k] for k in ['per_page', 'query', 'sort', 'page'] if request.args.get(k)}

    if 'export_query_limit' in chart_settings:
        global_params['per_page'] = chart_settings['export_query_limit']

    data, _ = charts_data_based_on_settings(chart_settings,
                                            _iterate_charts_series,
                                            max_points=max_points,
                                            df=request.args.get('df'),
                                            dt=request.args.get('dt'),
                                            parse_operators_mapping=PARSE_DATE_OPERATORS,
                                            global_params=global_params)
    return series_data_to_chart_table(chart_settings, data)


def _generate_export_data_based_on_viz_settings():
    """Generates excel table data based on `viz` settings"""
    settings = request.json.get('viz') or dict()
    max_series = parse_int(request.args.get('max_series'), 'max_series')
    if max_series is None:
        max_series = config.XLSX_DEFAULT_RECORDS
    max_series = min(max_series, config.XLSX_MAX_RECORDS)
    chart_settings = settings.get('chart') or dict()
    table_settings = settings.get('table') or dict()
    query_settings = settings.get('query') or dict()
    points_settings = settings.get('points') or dict()

    data, _ = viz_charts_data(chart_settings,
                              points_settings,
                              table_settings,
                              query_settings,
                              _iterate_charts_series,
                              max_series)
    return series_data_to_chart_table_viz(data)


@app.route('/api/1/series', methods=['OPTIONS'])
def series_api_cors():
    return ''


@app.route('/api/1/series', methods=['POST'])
def series_api_post():
    url_params = {h: v for h, v in request.args.items()}

    post_data = request.json
    series_ids = post_data.get('series_ids')
    series = post_data.get('series')
    chart_settings = post_data.get('chart_settings')
    series_queries = post_data.get('series_queries')
    if series_queries:
        series = list()
        for q in series_queries:
            if isinstance(q, six.string_types):
                series.append({
                    'series_id': series_query_to_sid(q)
                })
            elif isinstance(q, dict):
                if 'query' in q:  # else fail later
                    q['series_id'] = series_query_to_sid(q.pop('query'))
                series.append(q)
            else:
                raise InvalidParametersException('"series_queries" can contain only strings and objects')
        post_data['series'] = series

    if chart_settings is not None:
        param_names = ['per_page', 'query', 'sort', 'page']
        global_params = {k: request.args[k] for k in param_names if request.args.get(k)}
        res, raw_response = charts_data_based_on_settings(chart_settings,
                                                          _iterate_charts_series,
                                                          max_points=url_params.get('max_points', -1),
                                                          g_expression=url_params.get('g_expression'),
                                                          df=url_params.get('df'),
                                                          dt=url_params.get('dt'),
                                                          parse_operators_mapping=PARSE_DATE_OPERATORS,
                                                          global_params=global_params,
                                                          facets=request.args.get('facets'),
                                                          max_facet_values=request.args.get('max_facet_values'))
        if not raw_response:
            res.update({'success': True})
    elif series_ids is not None or series is not None:
        res, raw_response = bulk_get_series(post_data, url_params)
    else:
        raise InvalidParametersException('query, series_id url params or'
                                         ' json body with either series or series_ids required')

    if raw_response:
        return res  # just return as is. this is serialized Shooju response
    else:
        _add_resolved_milli(res, url_params)
        _add_points_desc(res)
        return dump_obj(res, req=request)


@app.route('/api/1/series', methods=['GET'])
def series_api_get():
    """
    This patches Shooju /series api to support virt. series.
    """
    # add vrt field to request....logic depends on method - POST or GET
    raw_response = False
    url_params = {h: v for h, v in request.args.items()}
    query = url_params.get('query')
    scroll_id = url_params.get('scroll_id')
    series_id = url_params.get('series_id')

    if query is not None and query.startswith('='):
        res, raw_response = get_series_by_query_formula(query, url_params)
    elif query is not None or scroll_id is not None:
        res, raw_response = get_series_by_query(url_params)

    else:
        if 'series_id' in url_params and url_params['series_id'].startswith('='):
            points_params = SeriesParams.from_request_params(url_params)
            res = {'success': True, 'series': [SeriesSingleFormulaCalculator(
                url_params['series_id'], points_params, fields=parse_fields(url_params.get('fields'))[0] or None,
                g_expression=url_params.get('g_expression'),
                debug=parse_bool(url_params.get('debug'))).execute()]}
            for s in res['series']:
                if s.pop('has_sampled_points', False):
                    res['sampled'] = True
        elif series_id is not None:
            res, raw_response = get_single_series(url_params)
        else:
            raise InvalidParametersException('query, series_id url params or'
                                             ' json body with either series or series_ids required')
    if raw_response:
        return res  # just return as is. this is serialized Shooju response
    else:
        _add_resolved_milli(res, url_params)
        _add_points_desc(res)
        return dump_obj(res, req=request)


def should_return_resolved_mill(raw_dt, date_format):
    """Returns true if should return resolved_dt_milli, resolved_df_milli etc"""

    defaults = 'MIN', 'MAX', '', None   # default or undefined
    return raw_dt not in defaults and (is_relative(raw_dt) or (is_iso(raw_dt) and date_format != 'iso'))


def _add_resolved_milli(resp, req_args):
    raw_df, raw_dt = req_args.get('df', 'MIN'), req_args.get('dt', 'MAX')
    params = SeriesParams.from_request_params(req_args)

    dt = None
    if should_return_resolved_mill(raw_df.split('@')[0], params.date_format):
        df, dt = parse_df_dt_with_operators(raw_df, raw_dt, params.date_format,
                                            operators_mapping=PARSE_DATE_OPERATORS)
        resp['resolved_df_milli'] = df
    if should_return_resolved_mill(raw_dt.split('@')[0], params.date_format):
        if not dt:
            df, dt = parse_df_dt_with_operators(raw_df, raw_dt, params.date_format,
                                                operators_mapping=PARSE_DATE_OPERATORS)
        resp['resolved_dt_milli'] = dt


@app.route('/api/1/series/reported_dates', methods=['GET'])
def series_get_reported_dates():
    series_id = request.args.get('series_id')
    series_query = request.args.get('series_query')
    if not (series_id or series_query):  # just relay it
        return proxy_request()

    # check if series has vrt field
    if series_id is None and series_query:
        series_id = '${}'.format(series_query)
    res = load_obj(monitored_upstream_call('/api/1/series', params={
        'series_id': series_id,
        'fields': config.VIRT_FIELD
    }, ).content, req=request)

    if not res.get('series'):
        return proxy_request()

    date_format = request.args.get('date_format', 'milli')

    df = parse_date(request.args.get('df') or 'MIN', date_format=date_format)
    dt = parse_date(request.args.get('dt') or 'MAX', date_format=date_format)

    virt_params = res['series'][0].get('fields', {}).get(config.VIRT_FIELD)
    if not virt_params or not virt_params.get('source'):
        return proxy_request()

    if virt_params['source'] not in sources_by_name:
        raise ShoojuException('unknown source "{}"'.format(virt_params.get('source', '')))
    mod = sources_by_name[virt_params['source']]
    try:
        reported_dates = mod.get_reported_dates(series_id, virt_params,
                                                df=milli_to_datetime(df), dt=milli_to_datetime(dt))
    except AttributeError:
        raise ShoojuException('source {} does not support reported dates'.format(virt_params['source']))
    except Exception:
        logger.exception('unexpected source exception')
        raise
    return dump_obj({'reported_dates': [render_date(parse_date(d, 'datetime'), date_format) for d in reported_dates],
                     'success': True},
                    req=request)


@app.route('/api/1/source/<source>/invoke/<func_name>', methods=('POST',))
def invoke_source_func(source, func_name):
    if source not in sources_by_name:
        raise ShoojuException('unknown source {}'.format(source))

    virt_obj = request.json.get('vrt')
    if virt_obj is None:
        raise InvalidParametersException('vrt field data is required')
    args = request.json.get('args') or []
    kwargs = request.json.get('kwargs') or dict()
    try:
        source_func = getattr(sources_by_name[source], func_name)
    except AttributeError:
        raise InvalidParametersException('%s is not implemented by %s source' % (func_name, source))

    if not isinstance(source_func, Invocable):
        raise InvalidParametersException('%s.%s is not invocable' % (source, func_name))

    user_data = get_request_user_data()
    if not source_func.check_user_teams(user_data.get('team') or []):
        raise NotAllowedException('user is not allowed to invoke %s.%s' % (source, func_name))

    try:
        with metrics.timer('invoke_source', source=source, func=func_name):
            res = monitor_execution(
                source_func,
                slow_log_threshold=config.SLOW_LOG_THRESHOLD,
            )(virt_obj, *args, **kwargs)
    except Exception as e:
        metrics.count('error', error_type='invoke_source', source=source, func=func_name)
        logger.exception(f'failed to invoke {source}.{func_name}')
        raise SourceException(f'failed to invoke {source}.{func_name}: {e}')

    try:
        resp = dump_obj({'success': True, 'result': res}, req=request)
    except InvalidParametersException as e:
        raise InvalidParametersException('failed to serialize %s.%s function result: %s', (source, func_name, e))
    return resp


def _add_points_desc(resp):
    if request.args.get('df') and request.args.get('dt'):
        try:
            df, dt = parse_df_dt_with_operators(request.args['df'], request.args['dt'], 'milli',
                                                operators_mapping=PARSE_DATE_OPERATORS)
            # Avoid comparison of str and int
            df, dt = (float('-inf') if i == 'MIN' else i for i in (df, dt))  # If df or dt is MIN set it to -infinity
            df, dt = (float('inf') if i == 'MAX' else i for i in (df, dt))  # If df or dt is MAX set it to infinity
            resp['points_desc'] = dt < df
        except (RelativeDateRangeParseException, DateParseException):
            pass


def _charts_scroll(args, max_series=config.XLSX_DEFAULT_RECORDS):
    args = dict(scroll='y', **args)
    resp, _ = get_series_by_query(args)
    scroll_id = resp.get('scroll_id')
    series = list()

    for ser in resp['series']:
        series.append(ser)
        if len(series) >= max_series:
            break

    while len(series) < max_series:
        resp_, _ = get_series_by_query({'scroll_id': scroll_id})

        if not resp_['series']:
            break

        for ser in resp_['series']:
            series.append(ser)
            if len(series) >= max_series:
                break
    resp['series'] = series
    return resp


def _iterate_charts_series(args, stg, g_expression, max_series=None):
    if stg.get('series_queries'):
        sids = [series_query_to_sid(s) for s in stg['series_queries']]
        post_body = {'series_ids': sids}
        if g_expression:
            post_body['g_expression'] = g_expression
        res, raw_response = bulk_get_series(post_body, args)
    else:  # query-style
        query = args.pop('query', stg.get('query'))
        args.setdefault('per_page', stg.get('query_limit', 40))
        args.update({
            'g_expression': g_expression
        })
        if max_series is None:
            max_series = parse_int(args['per_page'], 'per_page')

        if query.startswith('='):
            res, raw_response = get_series_by_query_formula(query, args)
        else:
            res = _charts_scroll(dict(query=query, **args), max_series)
            raw_response = False

    if raw_response:
        res = load_obj(res, req=request)

    if 'series' not in res and 'error' in res:  # got some error, let's re-rase
        raise InvalidParametersException(u'{} {}'.format(res.get('error', ''),
                                                         res.get('description', '')))

    addin = {k: v for k, v in six.iteritems(res) if k not in ['error', 'description', 'success', 'request_id']}
    return [s for s in res['series'] if s and s.get('series_id') is not None], addin


def has_virt_field(resp):
    """
    Check if response has Sj-Series-Have-Vobj-Field header set to True
    """
    return True  # it's not implemented on webapp side
    return bool(resp.headers.get('Sj-Series-Have-Vobj-Field'))


@app.route('/login.html', methods=['GET'])
def login_html():
    """
    Just overriding default login.html and redirecting another entry point for authentication
    :return:
    """
    back_to = '{}vwebauth'.format(config.LOCAL_URL)
    return redirect('{}#/auth/to/{}'.format(config.REMOTE_URL, six.moves.urllib.parse.quote_plus(back_to)))


@app.route('/vwebauth', methods=['GET'])
def vwebauth():
    """
    XXXX
    """
    return redirect('{}/#/auth/onetimetoken/{}'.format(config.LOCAL_URL, request.values.get('onetimetoken', 'none')))


@app.route('/virt_ping', methods=['GET'])
def ping():
    """
    Just returns OK if dependent services are OK
    """
    try:
        requests.get('{}/ping'.format(config.CALLABLES_RUNNER_HOST), timeout=5)
    except requests.exceptions.RequestException as e:
        return "Failed to connect to callable runner: {}".format(e), 500
    try:
        requests.get('{}/ping'.format(config.EXPRESSION_RUNNER_HOST), timeout=5)
    except requests.exceptions.RequestException as e:
        return "Failed to connect to expression runner: {}".format(e), 500
    return 'OK'


@app.route('/sjvirt/info')
def info():
    """Info endpoint. Returns some basic information about setup."""

    try:
        resp = requests.post(config.EXPRESSION_RUNNER_HOST, timeout=5)
        xpr_working = resp.status_code == 200
    except Exception:
        xpr_working = False

    try:
        accounts_settings = get_account_settings()
    except Exception as e:
        accounts_settings = {
            'localize_default_field': 'failed to fetch: {}'.format(e),
            'aggs_default_e': 'failed to fetch: {}'.format(e),
        }

    info =  '''
remote: {}
revision: {}
sentry: {}
wavefront: {}
localize_field: {}
aggs_default_e: {}
xpr runner working: {}
sjtoolkit version: {}
slowlog threshold: {}
cache size max: {}
cache size on disk estimated: {}
xpr runner timeout: {}
callables host: {}
callables runner status: {}
upstream connect timeout: {}
upstream read timeout: {}
    '''.format(config.REMOTE_URL,
               APP_REVISION,
               not config.DISABLE_SENTRY,
               bool(config.WAVEFRONT_KEY),
               accounts_settings.get('localize_default_field'),
               accounts_settings.get('aggs_default_e'),
               xpr_working,
               _sjtoolkit_version(),
               config.SLOW_LOG_THRESHOLD,
               config.CACHE_SIZE_MB * 1024 * 1024,
               cached.cache.volume(),
               config.EXPRESSION_RUNNER_TIMEOUT,
               config.CALLABLES_RUNNER_HOST,
               _get_callables_runner_status(),
               config.CONNECT_TIMEOUT,
               config.READ_TIMEOUT,
               ).strip()

    return Response(info, content_type='text/plain')


def _get_callables_runner_status():
    try:
        return requests.get('{}/ping'.format(config.CALLABLES_RUNNER_HOST),
                            timeout=5.).content.decode('utf-8')
    except Exception as e:
        return 'failed to connect: {}'.format(e)


def get_request_user_data():
    user, _ = get_request_auth(request)
    user_data = load_obj(monitored_upstream_call('/api/1/users/%s' % user, method='GET').content,
                         req=request).get('user') or {}
    if not user_data:
        raise NotAllowedException('not authorized')
    return user_data


def assert_sjvirt_admin(f):
    """
    Flask router decorator that asserts that request user is SJVirt admin
    """

    def _wrapper(*args, **kwargs):
        user_data = get_request_user_data()
        user_teams = user_data.get('team') or []

        if not set(config.ADMIN_TEAM).intersection(user_teams):
            raise NotAllowedException('user must be sjvirt admin')
        return f(*args, **kwargs)

    return _wrapper


@app.route('/sjvirt/clear_cache', methods=('POST',))
@assert_sjvirt_admin
def clear_cache():
    """Clears SJVirt on-disk cache."""
    current = cached.cache.volume()
    records_removed = cached.cache.clear()
    return dump_obj({'released_bytes': current - cached.cache.volume(),
                     'records_removed': records_removed}, req=request)


@app.route('/api/1/processors/<processor_id>/call/<func>', methods=('GET', 'POST'))
def call_processor_func(processor_id, func):
    """
    Executes processors function that wrapped by @sjutils.callable
    """
    try:
        username, password = get_request_auth(request)
    except NotAllowedException:
        username, password = None, None
    request_raw_data = None
    callable_post_json = None
    settings = {}
    try:
        callable_post_json = request.json
        settings = (request.json or {}).get('settings')
    except TypeError as e:
        pass

    callable_post_headers = dict(request.headers or {})
    callable_post_headers.pop('Authorization', None)
    callable_url_params = dict(request.args or {})

    callable_post_body = request.get_data()
    if not callable_post_body and request.form:
        callable_post_body = '&'.join(['{}={}'.format(k, v) for k, v in request.form.items()])

    call_params = {
        'processor_id': processor_id,
        'func': func,
        'settings': settings,
        'user': username,
        'callable_post_headers': callable_post_headers,
        'callable_url_params': callable_url_params,
        'callable_post_json': callable_post_json,
    }
    files = {
        'callable_post_body': callable_post_body,
    }
    _check_callable_permissions(processor_id, func)  # raises error if not allowed to call @callable

    try:
        with metrics.timer('s2s_request', destination='callables_runner'):
            resp = monitor_execution(requests.post, slow_log_threshold=config.SLOW_LOG_THRESHOLD)(
                '{}/call'.format(config.CALLABLES_RUNNER_HOST),
                files=files,
                data={'payload': json.dumps(call_params)},
                timeout=config.CALLABLES_TIMEOUT,
                auth=(username, password) if username else None,
            )
            if resp.status_code != requests.status_codes.codes.ok:
                raise UnexpectedException('failed to execute function')
    except Exception as e:
        metrics.count('error', error_type='callables_runner_request', error=e.__class__.__name__)
        raise

    if resp.headers.get('Callable-Binary-Response') == 'true':
        bytes_response = BytesCallResponse(resp.content, _extract_callables_binary_response_headers(resp.headers))
        response = send_file(bytes_response.payload, 'application/octet-stream', cache_timeout=0)
        for k, v in (bytes_response.headers or {}).items():
            response.headers[k] = v
        response.headers['Sj-Response-Format'] = 'binary'  # this is flag for upper logic to return headers as is
        return response

    res = msgpack.loads(resp.content)

    return dump_obj(res, req=request)


def _extract_callables_binary_response_headers(headers):
    return {
        _remove_prefix(k, 'Callable-Binary-Response-Header-'): v
        for k, v in headers.items() if k.startswith('Callable-Binary-Response-Header-')
    }

def _remove_prefix(s, prefix):
    if s.startswith(prefix):
        return s[len(prefix):]
    return s


def _check_callable_permissions(processor_id, func):
    # check if user is allowed to invoke callables
    try:
        user_data = get_request_user_data()
    except NotAllowedException:
        # anonymous func call
        return
    proc_data = g.sj.raw.get('/processors/{}'.format(processor_id))['importer']
    user_teams = set(user_data.get('team') or [])
    callers_teams = set(proc_data.get('team_obj', {}).get(CALLERS, []))
    if not user_teams.intersection({ADMIN, PROCESSOR_ADMIN}) \
            and not callers_teams.intersection(user_teams):
        raise NotAllowedException('not allowed to invoke {}.{}'.format(processor_id, func))


@memoize
def _sjtoolkit_version():
    try:
        return re.search(r'sj-toolkit\.git@(\w+)',
                         subprocess.check_output(['pip3.7', 'freeze']).decode('utf-8'), re.DOTALL).group(1)
    except Exception as e:
        return six.text_type(e)


@app.route('/api/1/users/<user_id>/cart', methods=['POST'])
def add_user_cart(user_id):
    query = request.json.get('query')
    series_queries = request.json.get('series_queries')
    descriptions = request.json.get('descriptions', {})
    if series_queries and query or (not series_queries and not query):
        raise InvalidParametersException("either series_queries or query parameter is required")

    if query:
        if query.startswith('='):
            return _get_cart_by_query_xpr(query)
        return proxy_request(request.path)


    series_queries = request.json.get('series_queries')
    descriptions = request.json.get('descriptions', {})

    expression_queries = [q for q in series_queries if q not in descriptions and q.startswith('=')]
    desc_by_query, errors_by_query = _get_expression_series_fields(expression_queries)

    series_queries = [q for q in series_queries if q not in errors_by_query]
    descriptions.update(desc_by_query)

    if series_queries:
        resp = monitored_upstream_call(request.path, method='POST', body={'series_queries': series_queries, 'descriptions': descriptions})
        shooju_response = load_obj(resp.content, req=request)
        if not shooju_response['success']:
            return resp
    else:
        shooju_response = {
            'success': True,
            'series': [],
            'request_id': '',
        }

    shooju_response['series'].extend([{
        'query': query,
        'error': error,
    } for query, error in errors_by_query.items()])

    return dump_obj(shooju_response, req=request)


def _get_expression_series_fields(queries):
    res = {}
    errors = {}
    for q in queries:
        resp, raw_response = get_series_by_query_formula(q, params={'fields': ['description']})
        if raw_response:
            resp = load_obj(resp, req=request)

        error_text = None
        if len(resp['series']) == 0:
            error_text = "series not found"

        if len(resp['series']) > 1:
            error_text = "too many series returned"

        if error_text:
            errors[q] = error_text
            continue

        res[q] = resp['series'][0].get('fields', {}).get('description', q)

    return res, errors


def _get_cart_by_query_xpr(query):
    resp, raw_response = get_series_by_query_formula(query, params={'fields': ['description']})
    if raw_response:
        resp = load_obj(resp, req=request)

    queries = []
    descriptions = {}
    for s in resp['series']:
        query = s.get('xpr', {}).get('xpr_query')
        if not query:
            continue
        description = s.get('fields', {}).get('description', '')
        queries.append(query)
        descriptions[query] = description

    if not queries:
        return dump_obj({'series': [], 'success': True, 'request_id': ''}, req=request)

    resp = monitored_upstream_call(request.path, method='POST',
                                   body={'series_queries': queries, 'descriptions': descriptions})
    return resp.content


@app.route('/api/1/series/to_series_queries', methods=['GET'])
def query_to_series_queries():
    query = request.values.get('query')
    if not query.startswith('='):
        return proxy_request(request.path)

    fields = ['description']
    resp, raw_response = get_series_by_query_formula(query, params={'fields': fields})

    queries = []
    descriptions = {}
    for s in resp['series']:
        query = s.get('xpr', {}).get('xpr_query')
        if not query:
            continue
        description = s.get('fields', {}).get('description', '')
        queries.append(query)
        descriptions[query] = description

    return dump_obj({
        'method': 'resolve_query_by_id_field',
        'series': [{'query': q, 'description': descriptions.get(q)} for q in queries],
        'success': True
    }, req=request)


@app.errorhandler(Exception)
def error_handler(err):
    logger.exception('unexpected exception')

    username = None
    try:
        username, password = get_request_auth(request)
    except NotAllowedException:
        pass

    if isinstance(err, ShoojuException):
        error = camel_to_underscore(err.__class__.__name__).replace('_exception', '')
    else:
        error = 'unexpected-exception'
        if not config.DISABLE_SENTRY:
            sentry.capture_exception(extra={
                'version': APP_REVISION,
                'user': username
            })
    metrics.count('error', url_rule=_get_current_request_url_rule(), error_type='request', error=error)
    _stop_timer()

    resp = {'success': False, 'error': error, 'description': six.text_type(err)}

    if isinstance(err, ExpressionException) and err.trace:
        resp['xpr'] = {'trace': err.trace}

    return dump_obj(resp, request.headers.get('Sj-Receive-Format', 'json'))


@app.before_request
def before_request():
    try:
        username, password = get_request_auth(request)
        g.sj = Connection(config.REMOTE_URL,
                          username,
                          password, requests_session=upstream_call.session)
    except NotAllowedException:
        g.sj = None
    g.request_timer = metrics.timer(
        'request',
        url_rule=_get_current_request_url_rule(),
    )

    g.upstream_request_context = UpstreamRequestContext.from_request(
        request,
        f'{id_generator(15)}_virt',
        Limits(max_call_depth=config.SERIES_EXPRESSION_MAX_RECURSION_DEPTH,
               max_duration_seconds=config.MAX_REQUEST_DURATION_SECONDS)
    )
    g.upstream_request_context.assert_limits_not_exceeded()


@app.after_request
def cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE'
    response.headers['Access-Control-Max-Age'] = '604800'
    response.headers['Access-Control-Allow-Headers'] = 'content-type,authorization'
    response.headers['Access-Control-Expose-Headers'] = 'Content-Length,Sj-Duration,Content-Disposition'
    response.headers['WWW-Authenticate'] = 'Basic'
    path = request.path
    if response.headers.get('Sj-Response-Format') == 'binary':
        response.headers.pop('Sj-Response-Format')
        if not response.headers.get('Content-Type'):
            response.headers['Content-Type'] = 'application/octet-stream'
    elif path.startswith('/api/') and path not in ['/api/1/series/excel']:
        response.headers['Content-Type'] = 'application/json'
        if not response.headers.get('Sj-Send-Format') and request.headers.get('Sj-Receive-Format'):
            response.headers['Sj-Send-Format'] = request.headers['Sj-Receive-Format']
    _stop_timer()
    return response


def run_server():
    """App entry point"""
    logger.info('serving requests at: 0.0.0.0:{} with {} config'.format(config.PORT, config.ENV))
    http_server = pywsgi.WSGIServer(('0.0.0.0', config.PORT), app)
    http_server.serve_forever()


# TODO: fix pyc files for PY3
def _reload_sources_on_change():
    """Reloads source modules if its files was modified"""
    sources = [f[:-3] for f in os.listdir('./sources') \
               if os.path.isfile(os.path.join('./sources', f)) and f.endswith('.py') \
               and not f.startswith('_')]

    for src in sources:
        py_file = os.path.join('./sources', '{}.py'.format(src))
        pyc_file = os.path.join('./sources', '{}.pyc'.format(src))
        mod_time = os.stat(py_file).st_mtime
        if sources_last_modified[src] != mod_time:
            try:
                os.remove(pyc_file)
            except (IOError, OSError):
                pass
            try:
                if src in sources_by_name:
                    reload_module(sources_by_name[src])
                mod = import_module('sources.{}'.format(src))
            except Exception:
                logger.exception('Failed to load {} source'.format(src))
                continue
            finally:
                sources_last_modified[src] = mod_time
            logger.info('{} source loaded'.format(src))
            sources_by_name[src] = mod


@postfork
def start_source_watcher():
    """Starts source watcher thread that reloads sources modules on its change"""
    global sources_last_modified, sources_by_name
    _reload_sources_on_change()
    scheduler = BackgroundScheduler()
    scheduler.add_job(_reload_sources_on_change, CronTrigger(second='*/10'))
    scheduler.start()

def _get_current_request_url_rule():
    return request.url_rule.rule

def _stop_timer():
    if hasattr(g, 'request_timer') and g.request_timer:
        g.request_timer.stop()
        g.request_timer = None

if __name__ == '__main__':
    run_server()
