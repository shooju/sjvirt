#!/bin/bash


function install () {
    # put docker-compose.yml and render settings in it
    cd "$INSTALL_PATH" && { curl -O https://bitbucket.org/shooju/sjvirt/raw/master/docker-compose.yml 2> /dev/null; cd - > /dev/null; }

    if [[ -z "$VIRT_REMOTE_URL" ]]; then
        echo "Enter Shooju account url:"
        read VIRT_REMOTE_URL
    fi

    if [[ -z "$VIRT_LOCAL_URL" ]]; then
        echo "Enter local Shooju Virtualization Server url:"
        read VIRT_LOCAL_URL
    fi

    if [[ -z "$VIRT_PORT" ]]; then
        echo "Enter app listen port:"
        read VIRT_PORT
    fi

    if [[ -z "$EXP_RUNNER_USER" ]]; then
        echo "Enter expression runner username:"
        read EXP_RUNNER_USER
    fi

    if [[ -z "$EXP_RUNNER_API_KEY" ]]; then
        echo "Enter expression runner api key:"
        read EXP_RUNNER_API_KEY
    fi

    if [[ -z "CALLABLE_RUNNER_USER" ]]; then
        echo "Enter callables runner username:"
        read CALLABLE_RUNNER_USER
    fi

    if [[ -z "CALLABLE_RUNNER_API_KEY" ]]; then
        echo "Enter callables runner api key:"
        read CALLABLE_RUNNER_API_KEY
    fi

    if [[ -z "$VIRT_WAVEFRONT_KEY" ]]; then
        echo "Wavefront monitoring api key (leave empty to disable):"
        read VIRT_WAVEFRONT_KEY
    fi
}

function migrate() {
    cp /etc/shooju/docker-compose.yml "$INSTALL_PATH/docker-compose.yml"
    eval $(parse_yaml /etc/shooju/virt.yml "")
    echo "Migrating config:"
    echo "remote url: $VIRT_REMOTE_URL"
    echo "local url: $VIRT_LOCAL_URL"
    echo "port: $VIRT_PORT"
    echo "expression runner user: $EXP_RUNNER_USER"
}

parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

CLEAN_INSTALL="y"
DOCKER_ADMIN=""


if ! docker ps > /dev/null 2>&1; then
    echo "ERROR: docker is either not installed or not running"
    exit 1;
fi

# under what user we going to control the docker
if [[ "$DOCKER_ADMIN" = "" ]]; then
    while [[ "$DOCKER_ADMIN" = "" ]]; do
        echo "Enter docker admin username:"
        read DOCKER_ADMIN
        if ! ( id -u "$DOCKER_ADMIN" ) > /dev/null 2>&1 ; then
            echo "$DOCKER_ADMIN user does not exist"
            DOCKER_ADMIN=""
        elif ! ( su - "$DOCKER_ADMIN" -c "docker ps -a" ) > /dev/null 2>&1 ; then
            echo "WARNING: $DOCKER_ADMIN has no access to docker daemon"
        fi
    done
fi

if [[ "$DOCKER_ADMIN" = "root" ]]; then  # ask install path if user is root
    INSTALL_PATH=""
    while [[ "$INSTALL_PATH" = "" ]]; do
        echo "Enter install path:"
        read INSTALL_PATH
        mkdir -p "$INSTALL_PATH"
    done
else
    DOCKER_ADMIN_HOME=`bash -c "echo ~"$DOCKER_ADMIN""`  # otherwise just install into users home
    INSTALL_PATH="$DOCKER_ADMIN_HOME/sjvirt"
fi

echo "Will perform the installation into $INSTALL_PATH"

if [[ ( -f "$INSTALL_PATH/docker-compose.yml" )  ||  ( -f "$INSTALL_PATH/virt.yml" )  ||  ( -f "$INSTALL_PATH/virt.sh" ) ]]; then
    CLEAN_INSTALL=""
    while [[ "$CLEAN_INSTALL" != "y"  &&  "$CLEAN_INSTALL" != "n"  ]]
    do
    echo "There are files in $INSTALL_PATH. Do you want to overwrite them (y/n):"
    read CLEAN_INSTALL
    done
fi

echo "Pulling the latest docker image"
if  ! ( docker pull shooju/shooju-virt 1> /dev/null ); then
    echo "Failed to pull latest image"
fi

if  ! ( docker pull shooju/shooju-virt-nginx 1> /dev/null ); then
    echo "Failed to pull latest nginx image"
fi

touch /var/log/virt_app.log

mkdir -p "$INSTALL_PATH"


if [[ ! -f "$INSTALL_PATH/docker-compose.yml" || "$CLEAN_INSTALL" = "y" ]]; then
    if [[ "$MIGRATE" = "y"  && -f /etc/shooju/docker-compose.yml ]]; then
        migrate
    else
        install
    fi
    # render these config vars
    sed -i -e "s|\${VIRT_REMOTE_URL}|$VIRT_REMOTE_URL|g" "$INSTALL_PATH/docker-compose.yml"
    sed -i -e "s|\${VIRT_LOCAL_URL}|$VIRT_LOCAL_URL|g" "$INSTALL_PATH/docker-compose.yml"
    sed -i -e "s|\${VIRT_PORT}|$VIRT_PORT|g" "$INSTALL_PATH/docker-compose.yml"
    sed -i -e "s|\${EXP_RUNNER_USER}|$EXP_RUNNER_USER|g" "$INSTALL_PATH/docker-compose.yml"
    sed -i -e "s|\${EXP_RUNNER_API_KEY}|$EXP_RUNNER_API_KEY|g" "$INSTALL_PATH/docker-compose.yml"
    sed -i -e "s|\${CALLABLE_RUNNER_USER}|$CALLABLE_RUNNER_USER|g" "$INSTALL_PATH/docker-compose.yml"
    sed -i -e "s|\${CALLABLE_RUNNER_API_KEY}|$CALLABLE_RUNNER_API_KEY|g" "$INSTALL_PATH/docker-compose.yml"
    sed -i -e "s|\${VIRT_WAVEFRONT_KEY}|$VIRT_WAVEFRONT_KEY|g" "$INSTALL_PATH/docker-compose.yml"
else
    echo "$INSTALL_PATH/docker-compose.yml already exists and was not updated"
fi

chown -R "$DOCKER_ADMIN" "$INSTALL_PATH"

echo "Done. Switch to \"$DOCKER_ADMIN\" shell and execute \"docker-compose -f $INSTALL_PATH/docker-compose.yml up -d\" command to start the app."