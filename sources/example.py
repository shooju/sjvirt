from datetime import datetime, timedelta
from random import random
from sjtoolkit.utils.utils import parse_date
from sjtoolkit.utils.decorators import cached, invocable


def list_series():
    """Iterates virt series."""
    for i in range(10):
        yield {
            'sid': 'users\\sj.maxim\\virt\\{}'.format(i),
            'description': 'Example virt series {}'.format(i),
            'tsdbid': 'TEST_VIRT_{}'.format(i),
            'vrt': {
                'source': 'example'
            }
        }


def get_points(series_id, virt_params, df=None, dt=None, max_points=0,
               include_timestamp=False, operators=None, reported_date=None, snapshot_date=None):
    """
    Retrieves points from source. Returns array of [milli, value]. 

    :param series_id: Shooju series id 
    :type series_id: str
    :param virt_params: Series vrt field
    :type virt_params: dict
    :param df: Date start
    :type df: datetime
    :param dt: Date finish
    :type dt: datetime
    :param max_points: Maximum number of points
    :type max_points: int
    :param include_timestamp: Emulates /series api include_timestamp parameter (When point was written).
    :type include_timestamp: bool
    :param operators: Array of tuples with custom user operators
    :type operators: list
    :param reported_date: If not passed returns latest data, otherwise returns the historic snapshot of how the series looked as of the reported date
    :type reported_date: datetime
    :param snapshot_date: If not passed returns latest data, otherwise returns the historic snapshot of how the series looked at that datetime.
    :type snapshot_date: datetime
    :return:
    """
    points = list()
    df = df or datetime(2001, 1, 1)
    dt = dt or datetime.utcnow()
    for day in range(max_points):
        d = df + timedelta(days=day)
        if d >= dt:
            break
        point = [
            parse_date(d, 'datetime'),
            random()
        ]
        if include_timestamp:
            point.append(parse_date(datetime.utcnow(), 'datetime'))
        points.append(point)
    return points


def get_reported_dates(series_id, virt_params, df=None, dt=None):
    """
    Returns an array of reported dates for given series
    :param series_id: Shooju series id
    :type series_id: str
    :param virt_params: Series vrt field
    :type virt_params: dict
    :param df: Date start
    :type df: datetime
    :param dt: Date finish
    :type dt: datetime
    :return:
    """
    return [datetime(2001, 1, d) for d in range(30)]


@cached(ttl=15)  # result will be cached for 15s
@invocable(teams=['_admin'])
def example_source_function(virt_params, a, b=None):
    """
    Custom source function. Can be called like `sjutils.invoke_sjvirt_source_func('example_source_function', {'source': 'example'}, 10, b=20)`

    """

    b = b or 0
    return {
        'comment': 'called in source: %s' % virt_params['source'],
        'add_result': a + b
    }
