from datetime import datetime, timedelta
from sjtoolkit.utils.utils import parse_date


def list_series():
    """Iterates virt series."""

    yield {
        'sid': 'users\\sj.maxim\\virt',
        'description': 'Example virt series',
        'tsdbid': 'TEST_VIRT',
        'vrt': {
            'source': 'example_custom_operators'
        }
    }


def get_points(series_id, virt_params, df=None, dt=None, max_points=0, include_timestamp=False, operators=None, reported_date=None):
    """
    Retrieves points from source. Returns array of [milli, value]. 

    :param series_id: Shooju series id 
    :type series_id: str
    :param virt_params: Series vrt field
    :type virt_params: dict
    :param df: Date start
    :type df: datetime
    :param dt: Date finish
    :type dt: datetime
    :param max_points: Maximum number of points
    :type max_points: int
    :param include_timestamp: Emulates /series api include_timestamp parameter (When point was written).
    :type include_timestamp: bool
    :param operators: Array of tuples with custom user operators
    :type operators: list
    :param reported_date: If not passed returns latest data, otherwise returns the historic snapshot of how the series looked at that datetime.
    :type reported_date: datetime
    :return: 
    """
    _id = None
    if operators:
        operators = dict(operators)
        _id = operators.get('id')
        if _id:
            _id = float(_id)

    points = list()
    df = df or datetime(2001, 1, 1)
    dt = dt or datetime.utcnow()
    for day in range(size):
        d = df + timedelta(days=day)
        if d >= dt:
            break
        point = [
            parse_date(d, 'datetime'),
            _id
        ]
        if include_timestamp:
            point.append(parse_date(datetime.utcnow(), 'datetime'))
        points.append(point)
    return points
