from datetime import datetime, timedelta
from random import random
from dateutil.parser import parse

def get_points(series_id, params=None, df=None, dt=None, max_points=10000000, operators=None, reported_date=None):
    init_date = parse(params['min_date'])
    last_date = parse(params['max_date'])
    if df and df>init_date:
        init_date = df
    if dt and dt<last_date:
        last_date = dt
    pts = []
    maxmin = params['max_num']-params['min_num']
    for d in range(max_points):
        dt = init_date+timedelta(**{params['frequency']:d})
        if dt > last_date:
            break
        pts.append([dt, random()*maxmin+params['min_num']])
    return pts


if __name__ == '__main__':
    assert len(get_points('test', {
        'source':'randm',
        'max_num':100,
        'min_num':90,
        'min_date':'2015-01-01',
        'max_date':'2016-01-01',
        'frequency':'days'
    }, max_points=2)) == 2