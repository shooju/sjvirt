from importlib import import_module
from logging import getLogger
from virt import config

logger = getLogger('sources')


def write_source_series(job, source_name):
    """Writes virtual series into Shooju for specified source"""
    series_to_write = list()
    mod = import_module('sources.{}'.format(source_name))
    try:
        ls = mod.list_series
    except AttributeError:  # no such function
        return  # no worries
    logger.info('writing {} source'.format(mod.__name__))
    for s in ls():
        if 'sid' not in s:
            raise Exception('required key missing: sid')

        series_id = s.pop('sid')
        s[config.VIRT_FIELD]['source'] = source_name
        series_to_write.append((series_id, s,))
    if series_to_write:
        for sid, flds in series_to_write:
            job.put_fields(sid, flds)
