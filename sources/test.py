from datetime import datetime
from sjtoolkit.utils.decorators import cached, invocable
from sjtoolkit.utils.utils import parse_date


def list_series(*args, **kwargs):
    from tests.test_sjvirt import test_data, series_ids
    series_by_sid = dict((s, d) for s, d in zip(series_ids, test_data) if 'vrt' in d['fields'])  # skip non virt

    for k, v in sorted(series_by_sid.items()):
        yield dict(series_id=k, **v)


@cached(ttl=10)
def get_points(series_id, *args, **kwargs):
    from tests.test_sjvirt import test_data, series_ids, TestSeriesSnapshot
    operators = kwargs.get('operators')
    reported_date = kwargs.get('reported_date')
    snapshot_date = kwargs.get('snapshot_date')
    include_timestamp = kwargs.get('include_timestamp')
    df = kwargs.get('df')
    dt = kwargs.get('dt')
    _id = None
    if operators:
        operators = dict(operators)
        _id = operators.get('id')
        if _id:
            _id = float(_id)

    series_by_sid = dict((s, d) for s, d in zip(series_ids, test_data) if 'vrt' in d['fields'])  # skip non virt
    series_by_sid.update(TestSeriesSnapshot.TEST_DATA)
    pts = series_by_sid[series_id]['points']

    if reported_date:  # add reported date year to value
        pts = [[parse_date(p[0], 'iso'), p[1] + reported_date.year] for p in pts]
    elif snapshot_date:  # add snapshot date month to the value
        pts = [[parse_date(p[0], 'iso'), p[1] + snapshot_date.month] for p in pts]
    else:
        pts = [[parse_date(p[0], 'iso'), p[1] if _id is None else _id] for p in pts]

    if include_timestamp:
        pts = [[p[0], p[1], parse_date('2020-01-01 18:20:20', 'iso')] for p in pts]

    if df > dt:
        pts = list(reversed(pts))

    return pts


def get_reported_dates(series_id, virt_params, df=None, dt=None):
    return [datetime(2001, 1, d + 1) for d in range(30)]


def _source_function(virt_params, a, b):
    b = b or 0
    return {
        'result': a + b
    }


@invocable(teams=['tests'])
def source_function(virt_params, a, b=None):
    return _source_function(virt_params, a, b)


@invocable
def source_function_all_users(virt_params, a, b=None):
    return _source_function(virt_params, a, b)


@invocable(teams=['_none'])
def source_function_for_no_one(virt_params, a, b=None):
    return _source_function(virt_params, a, b)


def source_function_non_invocable(virt_params, a, b=None):
    return _source_function(virt_params, a, b)

