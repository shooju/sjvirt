import requests
import csv

from sjtoolkit.utils.utils import parse_date

# settings
SYMBOLS = ['EXC', 'NEE', 'PEG', 'DUK', 'PCG']
ASPECTS = ['Open', 'High', 'Low', 'Close', 'Volume', 'Adj Close']


def list_series(since=None):
    csv_content = requests.get(
        'http://download.finance.yahoo.com/d/quotes.csv?s={}&f=sxnj1j4r'.format(','.join(SYMBOLS))).content
    for row in csv.reader(csv_content.split('\n')[0:-1]):
        # ['GOOG', 'NasdaqNM', 'Google Inc.', '294.1B', '17.004B', '25.68']
        for aspect in ASPECTS:
            yield {
                'sid': 'Virtual\\YF\\{}\\{}'.format(row[0], aspect),
                'description': 'Yahoo! Finance {} {} ({})'.format(row[0], aspect, row[2]),
                'exchange': row[1],
                'tsdbid': 'YF_{}_{}'.format(row[0], aspect.upper().replace(' ', '')),
                'vrt': {
                    'symbol': row[0],
                    'aspect': aspect,
                }
            }


def get_points(series_id, params=None, df=None, dt=None, **kwargs):
    yparams = {'s': params['symbol']}
    # add date parameters
    if df:
        yparams.update({'a': df.month - 1, 'b': df.day, 'c': df.year})
    if dt:
        yparams.update({'d': dt.month - 1, 'e': dt.day, 'f': dt.year})
    # fetch from yahoo and return reversed
    csv_content = requests.get('http://ichart.yahoo.com/table.csv', params=yparams).content
    rows = csv.DictReader(csv_content.split('\n')[0:-1])
    try:
        pts = [[parse_date(r['Date'], 'iso'), float(r[params['aspect']])] for r in rows]
        return list(reversed(pts))
    except KeyError:
        return []


if __name__ == '__main__':
    from datetime import date

    print(get_points('s', {'symbol': 'AAPL', 'aspect': 'Close'}, date(2015, 1, 1), date(2015, 1, 9)))
    for n in list_series():
        print(n)
