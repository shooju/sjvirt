FROM shooju/shooju-base3

ARG tag=latest

RUN apt-get update
RUN pip3.7 install setuptools -U

RUN cd / && git clone https://bitbucket.org/shooju/sjvirt.git app
WORKDIR /app
RUN git checkout $tag

RUN touch /app/virt_restart.txt
RUN touch /app/expression_runner_restart.txt

RUN pip3.7 install -e .