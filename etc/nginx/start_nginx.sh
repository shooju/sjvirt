#!/bin/bash

# The URL to make the HTTP request to
url="http://virt_app:5001/sjvirt/info"

# Make the HTTP request and store the response
response=$(curl -s "$url")

# Check if the curl command succeeded
if [ $? -ne 0 ]; then
    echo "Curl request failed"
    exit 1
fi

# Use grep to find the line with "remote:", then use sed to extract the part after "remote: "
remote=$(echo "$response" | grep "remote:" | sed 's/remote: //')

# Exit if no remote is found
if [ -z "$remote" ]; then
    echo "Failed to fetch sjvirt configuration. Exiting."
    exit 1
fi

# Extract the upstream host and scheme
upstream_host=$(echo "$remote" | grep -oE '//([^/]*)' | cut -d'/' -f3)
upstream_url_scheme=$(echo "$remote" | grep -oE '^(http|https)')

# Debug: Print extracted values
echo "Upstream Host: $upstream_host"
echo "Upstream URL Scheme: $upstream_url_scheme"

# Export extracted values as environment variables
export UPSTREAM_HOST="$upstream_host"
export UPSTREAM_URL_SCHEME="$upstream_url_scheme"
envsubst '${UPSTREAM_HOST},${UPSTREAM_URL_SCHEME}' < /app/virt.conf.template > /etc/nginx/sites-available/default

nginx -g 'daemon off;'