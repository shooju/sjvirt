#!/bin/bash
if ! getent hosts virt_app ; then
echo $(/sbin/ip route|awk '/default/ { print $3 }') "   virt_app" >> /etc/hosts
fi

if ! getent hosts virt_expression_runner ; then
echo $(/sbin/ip route|awk '/default/ { print $3 }') "   virt_expression_runner" >> /etc/hosts
fi

nginx -g 'daemon off;'