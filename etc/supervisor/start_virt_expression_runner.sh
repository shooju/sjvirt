#!/bin/bash

if ! getent hosts virt_app ; then
echo $(/sbin/ip route|awk '/default/ { print $3 }') "   virt_app" >> /etc/hosts
fi

uwsgi --http=0.0.0.0:5015 -w bin.expression_runner:app --loop gevent --process 5 --master --gevent 100 --enable-threads --buffer-size=65535 --touch-reload /app/expression_restart.txt
