#!/bin/bash

if ! getent hosts virt_expression_runner ; then
echo $(/sbin/ip route|awk '/default/ { print $3 }') "   virt_expression_runner" >> /etc/hosts
fi


uwsgi --socket=0.0.0.0:5000 --http=0.0.0.0:5001 -w app:app --loop gevent --process 5 --master --gevent 100 --enable-threads --buffer-size=65535 --touch-reload /app/virt_restart.txt
