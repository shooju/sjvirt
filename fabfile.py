from fabric.api import *


@task
def build(tag=None, push=False):
    """Builds either all tags or given tag"""

    if tag:
        tags = [tag]
    else:
        tags = ['alpha', 'beta', 'latest']

    for t in tags:
        _build_tag(t)

    if push:
        for t in tags:
            _push_tag(t)


def _build_tag(tag):
    local('docker build . --build-arg tag={0} --no-cache -t shooju/shooju-virt:{0}'.format(tag))


def _push_tag(tag):
    local('docker push shooju/shooju-virt:{}'.format(tag))
