import json

import numpy as np
import sjts
from six.moves import urllib
import threading
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import requests
from flask import request, g

from sjtoolkit.utils.decorators import memoize
from .exceptions import InvalidParametersException, UnexpectedException
from sjtoolkit.utils.args_parse import parse_bool
from sjtoolkit.utils.utils import MsgPack

from virt import config
from virt.metrics import metrics

UPSTREAM_CALLS_RETRY = 3


class UpstreamCallFunc(object):
    def __init__(self):
        self._local = threading.local()
        self.__name__ = 'UpstreamCallFunc'

    def __call__(self, path, method=None, stream=False, body=None, params=None, headers=None):
        """
        Perform relay call to Shooju

        :param path: Shooju request path
        :param method: request method. If None request.method will be used.
        :param stream: ...
        :param body: overrides request json body. By default request.json will be used.
        :param params: overrides request url parameters. By default request.args will be used.
        :param headers: request headers
        """

        url = u'{}{}'.format(config.REMOTE_URL, urllib.parse.quote(path))
        headers = headers or dict()
        request_headers = {h: v for h, v in request.headers if h not in ('Host', 'Cookie', 'Content-Length')}
        request_headers.update(headers)

        files = {k: (v.filename, v.stream)
                 for k, v in request.files.items()}

        if files:
            # requests will take care of that
            request_headers.pop('Content-Type', None)

        data = dump_obj(body, fmt=request.headers.get('Sj-Send-Format', 'json')) if body else request.data
        method = method or request.method
        params = params or {h: v for h, v in request.args.items()} or dict()
        params['upstream_request_context'] = g.upstream_request_context.raw
        with metrics.timer('s2s_request', destination='upstream'):
            try:
                res = getattr(self.session, method.lower())(
                    url, params=params,
                    data=data if not request.files else None,
                    headers=request_headers,
                    stream=stream,
                    files=files,
                    allow_redirects=False,
                    timeout=(config.CONNECT_TIMEOUT, config.READ_TIMEOUT)
                )
                return res
            except Exception as e:
                metrics.count('error', error_type=f'upstream_request', error=e.__class__.__name__)
                raise

    @property
    def session(self):
        if not hasattr(self._local, 'session'):
            def retry_policy():
                return Retry(
                    UPSTREAM_CALLS_RETRY,
                    status_forcelist={502, 503, 504},
                    method_whitelist={
                        'HEAD', 'GET', 'PUT', 'DELETE', 'OPTIONS', 'TRACE', 'POST'
                    }
                    , backoff_factor=.1
                )

            s = requests.Session()
            s.mount('http://', HTTPAdapter(max_retries=retry_policy()))
            s.mount('https://', HTTPAdapter(max_retries=retry_policy()))
            s.headers.update({
                'Sj-Virt-Call': 'true'
            })
            self._local.session = s

        return self._local.session


upstream_call = UpstreamCallFunc()


def load_obj(raw, fmt='json', req=None):
    if req is not None:
        fmt = req.headers.get('Sj-Receive-Format', fmt)
    if fmt not in SERIALIZERS:
        raise InvalidParametersException('{} is not supported response/request format'.format(fmt))

    return SERIALIZERS[fmt].loads(raw)


def dump_obj(obj, fmt='json', req=None):
    if req is not None:
        fmt = req.headers.get('Sj-Receive-Format', fmt)
    if fmt not in SERIALIZERS:
        raise InvalidParametersException('{} is not supported response/request format'.format(fmt))

    include_job = parse_bool(req.values.get('include_job')) if req else False
    include_timestamp = parse_bool(req.values.get('include_timestamp')) if req else False

    if not include_job and obj and obj.get('scroll_args'):
        include_job = parse_bool(obj['scroll_args'].get('include_job'))
    if not include_timestamp and obj and obj.get('scroll_args'):
        include_timestamp = parse_bool(obj['scroll_args'].get('include_timestamp'))

    return SERIALIZERS[fmt].dumps(
        obj,
        include_timestamp=include_timestamp,
        include_job=include_job,
    )

@memoize
def get_account_settings():
    # Lazily return account settings. This is cached.
    try:
        resp = requests.get('{}/api/1/account/config'.format(config.REMOTE_URL), timeout=30.)
        resp.raise_for_status()
        return resp.json()['account_config']
    except requests.exceptions.RequestException as e:
        raise UnexpectedException('failed to fetch account config: {}'.format(e))
    except (ValueError, KeyError):
        raise UnexpectedException('got unexpected response while fetching account config')


class SjtsSerializer:
    def dumps(self, body, include_timestamp=True, include_job=True):
        series = body.get('series', [])
        for s in series:
            points = s.get('points', []) if s else []
            if len(points):
                include_job = include_job and 'jobs' in points.dtype.fields
                include_timestamp = include_timestamp and 'ts' in points.dtype.fields

        dtype = [
            ('dates', 'i8'),
            ('values', 'f8'),
        ]
        if include_job:
            dtype.append(('jobs', 'i4'))
        if include_timestamp:
            dtype.append(('ts', 'u8'))

        # all points must have same dimensioning, otherwise sjts produces garbage
        for s in series:
            points = s.get('points', []) if s else []
            if len(points):
                s['points'] = np.array(s['points'][[f[0] for f in dtype]], np.dtype(dtype))

        return sjts.dumps(body, include_job, include_timestamp)

    def loads(self, raw):
        return sjts.loads(raw, True)


class MsgPackSerializer:
    def dumps(self, obj, include_timestamp=True, include_job=True):
        series = obj.get('series', [])
        for s in series:
            if 'points' in s and isinstance(s['points'], np.ndarray):
                s['points'] = s['points'].tolist()
        return MsgPack.dumps(obj)

    def loads(self, obj):
        return MsgPack.loads(obj)


class JsonSerializer:
    def dumps(self, obj, include_timestamp=True, include_job=True):
        series = obj.get('series', [])
        for s in series:
            if 'points' in s and isinstance(s['points'], np.ndarray):
                s['points'] = s['points'].tolist()
        return json.dumps(obj)

    def loads(self, obj):
        return json.loads(obj)


SERIALIZERS = {
    'sjts': SjtsSerializer(),
    'msgpack': MsgPackSerializer(),
    'json': JsonSerializer()
}
