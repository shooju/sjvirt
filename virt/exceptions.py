from sjtoolkit.exceptions import *


class ImplementationException(ShoojuException):
    """Raised when source function returned unexpected or wrong result."""
    pass


class SourceException(ShoojuException):
    """Raised on sources failures"""
    pass

class UnexpectedException(Exception):
    pass
