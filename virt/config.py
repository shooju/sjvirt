import yaml
import os
import six
import sys

ENV = os.environ.get('VIRT_ENV', 'development')

custom_operators = []

base_path = os.path.dirname(os.path.dirname(__file__))
config_path = os.path.join(base_path, "config/{}.yaml".format(ENV))

yaml_config = yaml.load(open(config_path, 'r').read())
if yaml_config.get('remote_url') and not yaml_config['remote_url'].endswith('/'):
    yaml_config['remote_url'] += '/'

if yaml_config.get('local_url') and not yaml_config['local_url'].endswith('/'):
    yaml_config['local_url'] += '/'

if yaml_config.get('expression_runner_host') and not yaml_config['expression_runner_host'].endswith('/'):
    yaml_config['expression_runner_host'] += '/'

SLOW_LOG_THRESHOLD = 5.
LOCAL_CONTACT = 'local IT'
CACHE_LOCATION = '/tmp/virt_cache_{}_{}'.format(sys.version_info[0], sys.version_info[1])
CACHE_SIZE_MB = 1024
EXPRESSION_CACHE_LOCATION = '/tmp/virt_expression_cache_{}_{}'.format(sys.version_info[0], sys.version_info[1])
EXPRESSION_CACHE_SIZE_MB = CACHE_SIZE_MB
EXPRESSION_CACHE_TTL_LIMIT = 60 * 60 * 24
EXPRESSION_RUNNER_TIMEOUT = 300
SJCLIENT_DIRECT_TIMEOUT = 100
CONNECT_TIMEOUT = 6.1
READ_TIMEOUT = 500.
CALLABLES_TIMEOUT = READ_TIMEOUT
XLSX_MAX_RECORDS = 100000
XLSX_DEFAULT_RECORDS = 40
MAX_REQUEST_DURATION_SECONDS = 600
SERIES_EXPRESSION_MAX_RECURSION_DEPTH = 10
MON_WRITER_USER = None
MON_WRITER_API_KEY = None

NUMERIC_CONFIGS = {
    'slow_log_threshold',
    'cache_size_mb',
    'expression_cache_size_mb',
    'expression_cache_ttl_limit',
    'expression_runner_timeout',
    'sjclient_direct_timeout',
    'connect_timeout',
    'read_timeout',
    'callables_timeout',
    'xlsx_max_records',
    'xlsx_default_records',
    'max_request_duration_seconds',
    'series_expression_max_recursion_depth',
}

ADMIN_TEAM = ['_admin']
LOGO_URL = None

globals().setdefault('DISABLE_SENTRY', False)
globals().setdefault('WAVEFRONT_KEY', '')
globals().update({k.upper(): v for k, v in six.iteritems(yaml_config)})

# ability to override config with env.
for k, v in six.iteritems(os.environ):
    if k.startswith('VIRT_'):
        k = k[5:].lower()
        if k == 'custom_operators' and isinstance(v, str):
            v = v.strip().split()
        if k in ['local_url', 'remote_url', 'expression_runner_host'] and not v.endswith('/'):
            v += '/'

        if k in NUMERIC_CONFIGS and v:  # if overriding config variable we use the same type as a default value
            try:
                v = float(v)
            except (ValueError, TypeError) as e:
                raise e(f'failed to parse {k}: {v} is not a numeric value')
        elif k in NUMERIC_CONFIGS:
            v = None
        globals().update({k.upper(): v})

VIRT_FIELD, XPR_FIELD = 'vrt', 'xpr'


if isinstance(ADMIN_TEAM, six.string_types):
    ADMIN_TEAM = [i.strip() for i in ADMIN_TEAM.split(',') if i.strip()]
