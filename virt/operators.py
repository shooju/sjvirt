
import numpy

from sjtoolkit.series.date_params_operators import BaseCalendarDateOperator
from sjtoolkit.series.suffix_processors import FilterOperator as BaseFilterOperator, registry

from .exceptions import InvalidParametersException
from .upstream import upstream_call, request, load_obj, get_account_settings


class FetchCalendarMixin(object):

    def __init__(self, *args, **kwargs):
        super(FetchCalendarMixin, self).__init__(*args, **kwargs)
        self._calendar = None  # for cache

    def fetch_calendar(self, cal_id):

        if self._calendar is not None:
            return self._calendar

        account_stgs = get_account_settings()
        cal_query = account_stgs.get('calendar_series_query')

        if cal_query:
            series_query = u'(%s) AND (cal_id=%s)' % (cal_query, cal_id)
        else:
            series_query = u'cal_id=%s' % cal_id

        resp = load_obj(upstream_call('/api/1/series', method='GET', params={
            'series_id': '$%s' % series_query,
            'max_points': -1
        }).content, req=request)
        if 'series' not in resp:
            raise InvalidParametersException(
                'failed to fetch calendar %s: %s' % (cal_id, resp.get('description') or resp.get('error'))
            )

        if not resp['series'] or not resp['series'][0] or not resp['series'][0].get('series_id'):
            raise InvalidParametersException('calendar %s is not found' % cal_id)

        ser = resp['series'][0]
        if 'error' in ser:
            raise InvalidParametersException(ser.get('description') or ser['error'])

        pts = ser.get('points')
        if pts is None:
            pts = []

        self._calendar = numpy.array(list(map(tuple, pts)), dtype=[('dates', 'i8'), ('values', 'f8'), ])
        self._calendar = self._calendar[self._calendar['values'] == 1]
        return self._calendar


class FilterOperator(FetchCalendarMixin, BaseFilterOperator):
    pass


registry.register('FILTER', FilterOperator)


class CalendarDateOperator(FetchCalendarMixin, BaseCalendarDateOperator):
    pass


PARSE_DATE_OPERATORS = {
    'cal': CalendarDateOperator,
}
