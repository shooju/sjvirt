import socket
from sjtoolkit.utils.metrics import metrics
from pyformance.reporters.reporter import Reporter
from pyformance.registry import MetricsRegistry
from shooju import Connection
from logging import getLogger
try:
    import uwsgi
except ImportError:
    uwsgi = None

REPORT_INTERVAL = 300

logger = getLogger('metrics_reporter')


class ShoojuReporter(Reporter):
    """Sends collected metrics to a special callable"""
    def __init__(self, sjclient, registry=None, reporting_interval=30, clock=None,):
        super(ShoojuReporter, self).__init__(
            registry=registry,
            reporting_interval=reporting_interval,
            clock=clock,
        )
        self._sjclient = sjclient

    def report_now(self, registry=None, timestamp=None):
        timestamp = timestamp or int(round(self.clock.time()))
        m = registry.dump_metrics()
        to_send = list()

        # adding implicitly here b/c we init BEFORE worker forked from master process
        uwsgi_worker_id = uwsgi.worker_id() if uwsgi is not None else 'not_an_uwsgi_worker'

        for key in m.keys():
            values = m[key]
            for value_key, value in values.items():
                to_send.append({
                    'ts_milli': timestamp * 1000,
                    'key': list(key) + [['uwsgi_worker_id', str(uwsgi_worker_id)]],
                    'value_key': value_key,
                    'value': value
                })
        try:
            self._sjclient.raw.post(
                '/processors/util_metrics/call/report_app_metrics', data_json={
                'settings': {
                    'metrics': to_send
                }
            })
        except Exception as e:
            logger.warning(f'failed to send metrics data: {e}', )


def init_metrics_reporting(service_name, host, user, api_key):
    """Starts metrics if Shooju credentials are provided"""
    if not all([host, user, api_key]):
        logger.warning('failed to init metrics reporting due to missing host, user and api key')
        return
    r = MetricsRegistry()
    metrics.set_registry(r)
    metrics.set_tags(
        service=service_name,
        hostname=socket.gethostname(),
    )
    reporter = ShoojuReporter(
        sjclient=Connection(host, user, api_key),
        reporting_interval=REPORT_INTERVAL,
        registry=r,
    )
    reporter.start()
