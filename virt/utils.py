from functools import wraps
import time
import dataclasses
from typing import Optional, List
import reprlib

import numpy
from logging import getLogger
from sjtoolkit.series.args_parse import parse_operators, parse_fields, ParamsOperators, parse_series_id
from sjtoolkit.series.date_params_operators import (
    parse_date_with_operators,
    parse_df_dt_with_operators, merge_and_narrow_points_parrams,
)
from sjtoolkit.series.suffix_processors import agg_by_field, operators_to_functions, registry as ops_registry
from sjtoolkit.utils.args_parse import parse_int, parse_bool
from sjtoolkit.utils.utils import (
    MIN_PYTHON_DATE,
    MAX_PYTHON_DATE,
    render_date,
)

from virt import config
from .operators import PARSE_DATE_OPERATORS
from .exceptions import InvalidParametersException
from .upstream import get_account_settings


def milli_to_datetime(milli):
    """Converts milli timestamp to python datetime object"""
    milli = min(milli, MAX_PYTHON_DATE)  # otherwise will raise 'year out of range' exception
    milli = max(milli, MIN_PYTHON_DATE)
    return render_date(milli, 'datetime')


def agg_by_field_points_list(data, agg_field, agg_type):
    """
    Does the same as sjtoolkit.series.suffix_processors.agg_by_field but accepts points as lists not as arrays.
    :param data:
    :param agg_field:
    :param agg_type:
    :return:
    """

    for s in agg_by_field(data, agg_field, agg_type):
        yield s


def check_operators_order(operators):
    """Asserts that custom_operators goes before builtin Shooju operators"""
    builtin_indexes = [i for i, o in enumerate(operators) if o.split(':')[0] not in config.CUSTOM_OPERATORS]
    custom_indexes = [i for i, o in enumerate(operators) if o.split(':')[0] in config.CUSTOM_OPERATORS]
    if not builtin_indexes or not custom_indexes:
        return
    if max(custom_indexes) > min(builtin_indexes):
        raise InvalidParametersException('all custom operators must go before builtin operators')


def _normalize_date(dt, date_format):
    if dt is not None and dt not in ('MIN', 'MAX'):
        dt_milli = parse_date_with_operators(dt, date_format, operators_mapping=PARSE_DATE_OPERATORS)
        dt = render_date(dt_milli, date_format)
    return dt


def _date_to_milli(dt, date_format):
    return parse_date_with_operators(dt, date_format, operators_mapping=PARSE_DATE_OPERATORS)


@dataclasses.dataclass(frozen=True)
class SeriesParams:
    date_format: str
    pre_df: str
    pre_dt: str
    pre_max_points: Optional[int]
    post_df: str
    post_dt: str
    post_max_points: Optional[int]
    operators: List[str]
    include_timestamp: bool
    include_job: bool
    agg_operators: List[str]
    custom_operators: List[str]
    g_expression: Optional[str]
    snapshot_date: Optional[int]
    reported_date: Optional[int]
    sample: bool
    pts_stats_fields: List[str]
    non_custom_ops: List[str]

    @classmethod
    def from_request_params(cls, s_params: dict, fields=None) -> 'SeriesParams':
        """
        Parses series parameters mapping and return SeriesParams namedtuple.
        """
        fields = fields or dict()
        is_xpr_series = fields.get('xpr') is not None
        _, pts_stats_fields = parse_fields(s_params.get('fields') or None)

        date_format = s_params.get('date_format', 'milli')
        df, dt = parse_df_dt_with_operators(s_params.get('df', 'MIN'), s_params.get('dt', 'MAX'),
                                            date_format, operators_mapping=PARSE_DATE_OPERATORS)

        include_timestamp = parse_bool(s_params.get('include_timestamp'))
        include_job = parse_bool(s_params.get('include_job'))

        max_points = parse_int(s_params.get('max_points') or 0, 'max_points')
        operators_str, operators, agg_operators = parse_operators(
            series_id=s_params.get('series_id'),
            query=s_params.get('query'),
            global_operators=s_params.get('operators'),
            skip_validate=True,
        )

        # todo this is hack. we should use  parse_operators() for this. we only need params operators in this case
        raw_params_operators = operators
        is_expression = False
        if s_params.get('series_id') and s_params['series_id'].startswith('='):
            _, raw_params_operators = parse_series_id(s_params['series_id'])
            is_expression = True
        elif s_params.get('query') and s_params['query'].startswith('='):
            _, raw_params_operators = parse_series_id(s_params['query'])
            is_expression = True

        check_operators_order(operators)

        if is_expression:
            # this case we ignore everything except post
            pre_operators = ParamsOperators([], include_pre=True,
                                            include_post=False, pre_is_default=not is_xpr_series)
        else:
            pre_operators = ParamsOperators(raw_params_operators, include_pre=True,
                                            include_post=False, pre_is_default=not is_xpr_series)

        post_operators = ParamsOperators(raw_params_operators, include_pre=False,
                                         include_post=True, pre_is_default=not is_xpr_series)

        custom_operators = [o for o in operators if o.split(':', 1)[0].lower() in config.CUSTOM_OPERATORS]
        operators = [o for o in operators if o not in custom_operators]

        custom_operators = [(o[0].lower(), o[1]) if len(o) == 2 else (o[0], None) \
                            for o in [o.split(':', 1) for o in custom_operators]]

        # check if custom operators conflicts with existing Shooju
        for co, _ in custom_operators:
            if ops_registry.is_registered(co.upper()):
                raise InvalidParametersException('custom operator "{}" conflicts with Shooju operator'.format(co))

        snapshot_date = _normalize_date(s_params.get('snapshot_date'), date_format)
        reported_date = _normalize_date(s_params.get('reported_date'), date_format)

        repdate = post_operators.repdate or pre_operators.repdate or post_operators.reppdate or pre_operators.reppdate

        if repdate:
            reported_date = parse_date_with_operators(repdate, 'iso', operators_mapping=PARSE_DATE_OPERATORS)

        if is_xpr_series:
            asof = post_operators.asof
        else:
            asof = post_operators.asof or pre_operators.asof

        if asof:
            if not asof.startswith('j'):
                snapshot_date = parse_date_with_operators(asof, 'iso', operators_mapping=PARSE_DATE_OPERATORS)

        # at this point self.pre_* and self.post_* are the same and acually global
        pre_max_points, post_max_points, pre_df, pre_dt, post_df, post_dt = merge_and_narrow_points_parrams(
            date_format, max_points, df, dt, pre_operators, post_operators,
            is_xpr_series, operators_mapping=PARSE_DATE_OPERATORS
        )

        pre_df, pre_dt = parse_df_dt_with_operators(pre_df, pre_dt,
                                                    date_format, operators_mapping=PARSE_DATE_OPERATORS)

        post_df, post_dt = parse_df_dt_with_operators(post_df, post_dt,
                                                      date_format, operators_mapping=PARSE_DATE_OPERATORS)

        non_custom_ops = operators

        if operators:
            operators = operators_to_functions(operators,
                                               body={'fields': fields},
                                               account_settings=get_account_settings())
        sample = parse_int(s_params.get('sample'), 'sample')
        return cls(date_format, pre_df, pre_dt, pre_max_points, post_df, post_dt, post_max_points,
                   operators or [], include_timestamp, include_job, agg_operators, custom_operators,
                   s_params.get('g_expression'), snapshot_date, reported_date, sample, pts_stats_fields,
                   non_custom_ops)


SECRET_PARAMS = ('auth', )


def Repr():
    r = reprlib.Repr()
    r.maxdict = 100
    r.maxstring = 1024 * 5
    r.maxlist = 30
    r.maxarray = 30
    r.maxarray = 30
    r.maxfrozenset = 30
    r.maxdeque = 30
    r.maxset = 30
    r.maxother = 300
    return r


_repr = Repr()


def monitor_execution(f, slow_log_threshold=None):
    """
    Logs verbose debug information about the called function.
    :param f: Function to call
    :param slow_log_threshold: If provided and the function call took longer than that threshold,
    then logs slow log warning.
    :return:
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        args_repr = ', '.join(_repr.repr(a) for a in args) if args else ''
        kwargs_repr = ', '.join([
            '%s=%s' % (k, _repr.repr(v) if k not in SECRET_PARAMS else '<hidden>') for k, v in kwargs.items()
        ]) if kwargs else ''
        params_repr = ', '.join([o for o in [args_repr, kwargs_repr] if o])

        full_repr = f'{f.__module__}.{f.__name__}({params_repr})'
        _s = time.time()
        logger = getLogger('virt')

        logger.debug(f'calling {full_repr}')
        try:
            res = f(*args, **kwargs)
        except Exception:
            logger.exception(f'unexpected error while calling {full_repr}')
            raise
        finally:
            took = time.time() - _s
            logger.debug(f'executed {full_repr} in {took}s.')

        if slow_log_threshold is not None and took > slow_log_threshold:
            slow_log = getLogger('SLOW')
            slow_log.warning(f'took  {took}s. to execute {full_repr}')

        return res

    return wrapper
