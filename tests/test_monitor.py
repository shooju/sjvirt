from unittest import TestCase
from bin.monitor import metrics, compute_network_utilization, compute_cpu_utilization, rotate_series_by, \
    stats_to_series_points
from dateutil.parser import parse
import pkg_resources
import json
import os


class TestMonitor(TestCase):
    def setUp(self):
        self.monitor_hostname = monitor_hostname = os.environ.get('VIRT_MON_HOSTNAME')
        self.host_prefix = host_prefix = '{}\\{}'.format(os.environ.get('VIRT_MON_METRICS_PREFIX'), monitor_hostname)
        self.container_name = container_name = 'redis-sentinel'

        docker_stats_data_set_path = pkg_resources.resource_filename('tests', 'docker_stats_data_set.json')
        with open(docker_stats_data_set_path, 'r') as fp:
            docker_stats = json.load(fp)
            # Load test data set to series buffer
            for stat in docker_stats:
                dt = parse(stat['read'])
                period = rotate_series_by(dt)
                for sub_sid, val in stats_to_series_points(stat):
                    metrics.add(key=f'{host_prefix}\\{period}\\{container_name}\\{sub_sid}',
                                dt=dt,
                                val=val,
                                metric=sub_sid.split('\\')[-1],
                                category_tree='\\'.join(sub_sid.split('\\')[:-1]) or 'general',
                                host=monitor_hostname,
                                container=container_name,
                                period=period)

    def test_compute_cpu_utilization(self):
        cpu_utilization = compute_cpu_utilization(metrics.series_by_sid, '2021-12', self.container_name)
        assert(round(cpu_utilization['val'], 2) == 0.46)

    def test_compute_network_utilization(self):
        network_stats = compute_network_utilization(metrics.series_by_sid, '2021-12', self.container_name)
        rx, tx = network_stats if network_stats[0]['metric'] == 'rx_rate' else network_stats[::-1]
        assert ((round(rx['val'], 2), round(tx['val'], 2)) == (207.48, 310.97))
