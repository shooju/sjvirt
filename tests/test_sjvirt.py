# encoding: utf-8
from gevent import monkey

monkey.patch_all()


import json
from unittest import TestCase
from base64 import b64encode
import six
from datetime import datetime
from mock import patch, PropertyMock
import os
import sys
import xlrd
import time

import requests

import sjts
from shooju import Connection, Point
from dateutil.parser import parse
from flask_testing import LiveServerTestCase  # this is to give access to expression_runner
import random

from app import app, config, drop_series_expression_cache
from sjtoolkit.utils.utils import render_date
from sjtoolkit.utils.decorators import cached, retry
# import subprocess module the last due to monkey patching issues
from subprocess import Popen

# Used in sjvirt.sources.test for test virt source
test_data = [
    {
        'fields': {'vrt': {'source': 'test'}, 'aspect': 'Oil', 'timezone': 'America/Chicago'},
        'points': [
            ['2001-01-01 00:00:00', 100.1],
            ['2001-02-01 00:00:00', -1.90],
            ['2001-03-01 00:00:00', 0.],
            ['2002-02-01 00:00:00', -1000.22],
            ['2003-05-01 00:00:00', 909],
            ['2003-12-31 00:00:00', 103.0000009],
            ['2004-01-31 00:00:00', 0.000000000000001],
        ]
    },
    {
        'fields': {'vrt': {'source': 'test'}, 'aspect': 'Gas'},
        'points': [
            ['2010-01-01 01:00:00', 20.1],
            ['2010-01-01 02:00:00', 30.1],
            ['2010-01-01 03:00:00', 40.1],
            ['2010-01-01 04:00:00', 0.1],
            ['2010-01-01 05:00:00', -80],
            ['2010-01-01 06:00:00', -2.],
            ['2010-01-01 07:00:00', 10],
        ]
    },
    {
        'fields': {'vrt': {'source': 'test'}, 'aspect': 'Wind'},
        'points': [
            ['2001-01-01 00:00:00', 1000],
            ['2002-01-01 00:00:00', 2000],
            ['2003-01-01 00:00:00', 3000],
            ['2004-01-01 00:00:00', 4000],
            ['2005-01-01 00:00:00', 5000],
            ['2006-01-01 00:00:00', 6000],
            ['2007-01-01 00:00:00', 7000],
        ]
    },
    # these series are shooju ones (non-virt)
    {
        'fields': {'aspect': 'Sun'},
        'points': [
            ['2001-01-01 00:00:00', 1],
            ['2001-02-01 00:00:00', 2],
            ['2001-03-01 00:00:00', 3],
            ['2002-02-01 00:00:00', 3],
            ['2003-05-01 00:00:00', 5],
            ['2003-12-31 00:00:00', 6],
            ['2004-01-31 00:00:00', 7],
        ]
    },
    {
        'fields': {'aspect': 'Water'},
        'points': []
    },
    {
        'fields': {'xpr': {'expression': r'={{sid=sjvirt\tests\0}} * G.a', 'g': 'G.a = -1'},
                   'aspect': 'Water', 'timezone': 'America/Chicago'}
    }
]
# Used in sjvirt.sources.test for test virt source
series_ids = ['sjvirt\\tests\\{}'.format(i) for i in range(len(test_data))]
series_by_sid = dict(zip(series_ids, test_data))


def get_python_bin():
    ver = sys.version_info
    if ver[0] == 2:
        return "/usr/bin/python2.7"
    else:
        return "/usr/local/bin/python{}.{}".format(ver[0], ver[1])


class JsonRequestExector:
    def __init__(self, test_case):
        self.test_case = test_case

    def exec_request(self, method_func, path, headers=None, data_json=None,
                      expect_error=None, binary=False, **url_params):
        headers = headers or []
        data = method_func('/api/1{}'.format(path),
                           headers=headers + self.auth_headers,
                           data=json.dumps(data_json) if data_json is not None else None,
                           query_string=url_params).data
        if binary:
            return data

        resp = json.loads(data)
        self._check_response(resp, expect_error)
        return resp

    @property
    def auth_headers(self):
        return [
            (
                'Authorization',
                'Basic ' + b64encode('{}:{}'.format(
                    config.TEST_USER, config.TEST_API_KEY).encode('utf-8')).decode('ascii')
            )
        ]

    def _check_response(self, resp, expect_error):
        if expect_error:
            self.test_case.assertEqual(resp.get('error'), expect_error, '{} error is not raised'.format(expect_error))
        else:
            self.test_case.assertTrue(
                resp.get('success'),
                'got unexpected api error: {} - {}'.format(resp.get('error'),resp.get('description'))
            )


class SjtsRequestExecutor(JsonRequestExector):
    def exec_request(self, method_func, path, headers=None, data_json=None,
                     expect_error=None, binary=False, **url_params):
        if path != '/series':
            return super(SjtsRequestExecutor, self).exec_request(
                method_func, path, headers, data_json,
                expect_error, binary, **url_params,
            )

        do_emulate_iso = False
        if url_params.get('date_format') == 'iso':
            url_params.pop('date_format')
            do_emulate_iso = True

        headers = headers or []
        data = method_func('/api/1{}'.format(path),
                           headers=headers + self.auth_headers + [('Sj-Receive-Format', 'sjts')],
                           data=json.dumps(data_json) if data_json is not None else None,
                           query_string=url_params).data
        if binary:
            return data

        resp = sjts.loads(data)
        for s in resp.get('series', []):
            if 'points' not in s:
                continue
            for ix, p in enumerate(s['points']):
                s['points'][ix] = list(p)
                if do_emulate_iso:
                    s['points'][ix][0] = render_date(p[0], 'iso')

        self._check_response(resp, expect_error)
        return resp


class FlaskTestCase(LiveServerTestCase, TestCase):
    request_executor_cls = JsonRequestExector

    def create_app(self):
        app.config['TESTING'] = True
        app.config['LIVESERVER_PORT'] = 5001
        return app

    def _run_expression_runner(self):
        env = {
            'EXP_RUNNER_ACCOUNTS_TOTAL': "1",
            'EXP_RUNNER_ACCOUNT_1': config.REMOTE_URL,
            'EXP_RUNNER_HOST_OVERRIDE_1': 'http://localhost:5001',
            'EXP_RUNNER_USER_1': config.TEST_USER,
            'EXP_RUNNER_API_KEY_1': config.TEST_API_KEY,
        }
        self.expression_runner = Popen([get_python_bin(), 'bin/expression_runner.py'],
                                     cwd=os.path.abspath(os.path.join(__file__, '../../')),
                                     env=env)
        self.addCleanup(self.expression_runner.kill)
        @retry(times=15, delay=2, backoff=1)
        def ping():
            requests.get("http://localhost:5015/ping")
        ping()

    def setUp(self):
        self.request_executor = self.request_executor_cls(self)

        self.client = app.test_client(use_cookies=True)
        self.sj = Connection(config.REMOTE_URL, config.TEST_USER, config.TEST_API_KEY)
        with self.sj.register_job('virt test job') as job:
            self.sj.raw.delete('/series/cache')
            job.delete_by_query('sid:sjvirt\\tests')
            self.sj.raw.delete('/series/cache')
            self.sj.raw.post('/series/refresh')
            for i, s in enumerate(test_data):
                job.put_fields('sjvirt\\tests\\{}'.format(i), s['fields'])
                if 'vrt' not in s['fields'] and 'xpr' not in s['fields']:
                    job.put_points('sjvirt\\tests\\{}'.format(i), [Point(parse(p[0]), p[1]) for p in s['points']])

        self.job_id = job.job_id
        self.sj.raw.delete('/series/cache')

        self._run_expression_runner()

    def tearDown(self):
        self.client.__exit__(None, None, None)

    def post(self, path, data_json=None, expect_error=None, binary=False, **kwargs):
        return self._exec_request(
            self.client.post,
            path=path,
            hearders=[('Content-Type', 'application/json')],
            data_json=data_json,
            expect_error=expect_error,
            binary=binary,
            **kwargs
        )

    def get(self, path, expect_error=None, binary=False, **kwargs):
        return self._exec_request(
            self.client.get,
            path=path,
            expect_error=expect_error,
            binary=binary,
            **kwargs
        )

    def _exec_request(self, method_func, path, headers=None, data_json=None,
                      expect_error=None, binary=False, **url_params):
        return self.request_executor.exec_request(
            method_func,
            path,
            headers=headers,
            data_json=data_json,
            expect_error=expect_error,
            binary=binary,
            **url_params,
        )

    def delete(self, path, data_json=None, expect_error=None, binary=False, **kwargs):
        return self._exec_request(
            self.client.delete,
            path=path,
            hearders=[('Content-Type', 'application/json')],
            data_json=data_json,
            expect_error=expect_error,
            binary=binary,
            **kwargs
        )


class TestSeries(FlaskTestCase):
    request_executor_cls = JsonRequestExector

    def test_api_bearer_http_auth(self):
        headers = [('Authorization', 'Bearer ' + b64encode('{}:{}'.format(config.TEST_USER, config.TEST_API_KEY).encode('utf-8')).decode('ascii'))]
        with patch.object(self.request_executor_cls, 'auth_headers', new_callable=PropertyMock) as mock_auth_headers:
            mock_auth_headers.return_value = headers

            # Direct call
            res = self.get('/series',
                           series_id=series_ids[3], sample=3, fields='*',
                           max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso')['series'][0]
            self.assertEqual(res, {
                'series_id': 'sjvirt\\tests\\3',
                'fields': {'aspect': 'Sun'},
                'points': [['2001-01-01T00:00:00', 1.0], ['2001-03-01T00:00:00', 3.0], ['2002-02-01T00:00:00', 3.0],
                           ['2003-12-31T00:00:00', 6.0], ['2004-01-31T00:00:00', 7.0]]})

            # Call upstream
            self.get('/series/autocomplete', query='=')

        bad_auth_headers = [('Authorization', 'BadAuthType ' + b64encode('{}:{}'.format(config.TEST_USER, config.TEST_API_KEY).encode('utf-8')).decode('ascii'))]
        with patch.object(self.request_executor_cls, 'auth_headers', new_callable=PropertyMock) as mock_auth_headers:
            mock_auth_headers.return_value = bad_auth_headers

            # Direct call
            self.get('/series',
                     series_id=series_ids[3], sample=3, fields='*',
                     max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso', expect_error='not_allowed')
            # Call upstream
            self.get('/series/autocomplete', query='=', expect_error='invalid_credentials')

    def test_sjts(self):
        def raw_get(path, expect_error=None, headers=None, **kwargs):
            h = dict(self.request_executor.auth_headers)
            if headers:
                h.update(headers)
            resp = self.client.get('/api/1{}'.format(path), headers=h, query_string=kwargs).data
            return resp

        normal_series_id = series_ids[3]

        raw_res = raw_get('/series', max_points=-1, series_id=normal_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)

        self.assertEqual(res['series'][0]['points'],
                         [(978307200000, 1.0),
                          (980985600000, 2.0),
                          (983404800000, 3.0),
                          (1012521600000, 3.0),
                          (1051747200000, 5.0),
                          (1072828800000, 6.0),
                          (1075507200000, 7.0)])

        raw_res = raw_get('/series', max_points=-1, include_timestamp=True,
                          series_id=normal_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(len(res['series'][0]['points'][0]), 3)

        raw_res = raw_get('/series', max_points=-1, include_job=True,
                          series_id=normal_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(len(res['series'][0]['points'][0]), 3)

        raw_res = raw_get('/series', max_points=-1, include_timestamp=True, include_job=True,
                          series_id=normal_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(len(res['series'][0]['points'][0]), 4)

        virt_series_id = series_ids[0]

        raw_res = raw_get('/series', max_points=-1, series_id=normal_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(res['series'][0]['points'],
                         [(978307200000, 1.0),
                          (980985600000, 2.0),
                          (983404800000, 3.0),
                          (1012521600000, 3.0),
                          (1051747200000, 5.0),
                          (1072828800000, 6.0),
                          (1075507200000, 7.0)])

        raw_res = raw_get('/series', max_points=-1, include_timestamp=True,
                          series_id=virt_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(len(res['series'][0]['points'][0]), 3)

        raw_res = raw_get('/series', max_points=-1, include_job=True,
                          series_id=normal_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(len(res['series'][0]['points'][0]), 3)

        raw_res = raw_get('/series', max_points=-1, include_timestamp=True, include_job=True,
                          series_id=normal_series_id, headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(len(res['series'][0]['points'][0]), 4)

    def test_sample_operator_is_not_supported(self):
        # test simple GET

        res = self.get('/series',
                       query='sid={}'.format(series_ids[0], ''),
                       max_points=-1, df='2001-01-01', dt='2016-01-01',
                       date_format='iso', operators='@sample:3', expect_error='invalid_parameters')
        self.assertEqual(res['description'], u'operator SAMPLE is not supported')

        # test POST

        res = self.post('/series', data_json={
            'series_ids': [series_ids[0], series_ids[-3]]
        }, max_points=-1, date_format='iso', operators='@sample:2', expect_error='invalid_parameters')
        self.assertEqual(res['description'], u'operator SAMPLE is not supported')

    def test_sample_parameter(self):
        # test simple GET
        res = self.get('/series',
                       series_id=series_ids[0], sample=3,
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso')
        self.assertEqual(res['series'][0], {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                                        [u'2001-02-01T00:00:00', -1.9],
                                                        [u'2002-02-01T00:00:00', -1000.22],
                                                        [u'2003-05-01T00:00:00', 909.0],
                                                        [u'2004-01-31T00:00:00', 1e-15]],
                                            u'series_id': u'sjvirt\\tests\\0'})
        self.assertTrue(res.get('sampled'))

        res = self.get('/series',
                       series_id=series_ids[0], sample=300,
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso')
        self.assertEqual(res['series'][0], {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                                        [u'2001-02-01T00:00:00', -1.9],
                                                        ['2001-03-01T00:00:00', 0.0],
                                                        [u'2002-02-01T00:00:00', -1000.22],
                                                        [u'2003-05-01T00:00:00', 909.0],
                                                        ['2003-12-31T00:00:00', 103.0000009],
                                                        [u'2004-01-31T00:00:00', 1e-15]],
                                            u'series_id': u'sjvirt\\tests\\0'})
        self.assertFalse(res.get('sampled'))

        # test POST
        res = self.post('/series', data_json={
            'series_ids': [series_ids[0], series_ids[-3]]
        }, max_points=-1, date_format='iso', sample=2)

        self.assertEqual(res['series'],
                         [{u'points': [[u'2001-01-01T00:00:00', 100.1],
                                       [u'2002-02-01T00:00:00', -1000.22],
                                       [u'2003-05-01T00:00:00', 909.0],
                                       [u'2004-01-31T00:00:00', 1e-15]],
                           u'series_id': u'sjvirt\\tests\\0'},
                          {u'points': [[u'2001-01-01T00:00:00', 1.0],
                                       [u'2001-03-01T00:00:00', 3.0],
                                       [u'2003-05-01T00:00:00', 5.0],
                                       [u'2004-01-31T00:00:00', 7.0]],
                           u'series_id': u'sjvirt\\tests\\3'}])
        self.assertTrue(res.get('sampled'))

        res = self.post('/series', data_json={
            'series_ids': [series_ids[0], series_ids[-3]]
        }, max_points=-1, date_format='iso', sample=200)

        self.assertEqual(res['series'],
                         [{u'points': [[u'2001-01-01T00:00:00', 100.1],
                                       [u'2001-02-01T00:00:00', -1.9],
                                       [u'2001-03-01T00:00:00', 0.0],
                                       [u'2002-02-01T00:00:00', -1000.22],
                                       [u'2003-05-01T00:00:00', 909.0],
                                       [u'2003-12-31T00:00:00', 103.0000009],
                                       [u'2004-01-31T00:00:00', 1e-15]],
                           u'series_id': u'sjvirt\\tests\\0'},
                          {u'points': [[u'2001-01-01T00:00:00', 1.0],
                                       [u'2001-02-01T00:00:00', 2.0],
                                       [u'2001-03-01T00:00:00', 3.0],
                                       [u'2002-02-01T00:00:00', 3.0],
                                       [u'2003-05-01T00:00:00', 5.0],
                                       [u'2003-12-31T00:00:00', 6.0],
                                       [u'2004-01-31T00:00:00', 7.0]],
                           u'series_id': u'sjvirt\\tests\\3'}])
        self.assertFalse(res.get('sampled'))

    def test_sample_operator_in_parameters_is_not_supported(self):
        # test simple GET
        res = self.get('/series',
                       series_id=series_ids[0], operators='@sample:3',
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso', expect_error='invalid_parameters')
        self.assertEqual(res['description'], u'operator SAMPLE is not supported')

        res = self.post('/series', data_json={
            'series_ids': [series_ids[0], series_ids[-3]]
        }, max_points=-1, date_format='iso', operators='@sample:2', expect_error='invalid_parameters')

        self.assertEqual(res['description'], u'operator SAMPLE is not supported')

    def test_sample_operator_in_expressions_is_not_supported(self):
        # test simple GET
        res = self.get('/series', series_id='={{sid=%s@sample:3}}' % series_ids[0],
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso', expect_error='expression')
        self.assertEqual(res['description'], u'operator SAMPLE is not supported')

    def test_sample_parameter_in_expressions(self):
        # test simple GET
        res = self.get('/series',
                       series_id='={{sid=%s}}' % series_ids[0], sample=2,
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso')['series'][0]
        self.assertEqual(res['points'],
                         [[u'2001-01-01T00:00:00', 100.1],
                          [u'2002-02-01T00:00:00', -1000.22],
                          [u'2003-05-01T00:00:00', 909.0],
                          [u'2004-01-31T00:00:00', 1e-15]])

        # test POST

        series = self.post('/series', data_json={
            'series_ids': ['={{sid=%s}}' % series_ids[0], '={{sid=%s}}' % series_ids[-3]]
        }, max_points=-1, date_format='iso', sample=2)['series']

        self.assertEqual(series[0]['points'],
                         [[u'2001-01-01T00:00:00', 100.1],
                          [u'2002-02-01T00:00:00', -1000.22],
                          [u'2003-05-01T00:00:00', 909.0],
                          [u'2004-01-31T00:00:00', 1e-15]])
        self.assertEqual(series[1]['points'],
                         [[u'2001-01-01T00:00:00', 1.0],
                          [u'2001-03-01T00:00:00', 3.0],
                          [u'2003-05-01T00:00:00', 5.0],
                          [u'2004-01-31T00:00:00', 7.0]])

    def test_sample_operator_in_parameters_in_expressions_is_not_supported(self):
        # test simple GET
        res = self.get('/series',
                       series_id='={{sid=%s}}' % series_ids[0], operators='@sample:2',
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso', expect_error='invalid_parameters')
        self.assertEqual(res['description'], u'operator SAMPLE is not supported')

        # test POST

        res = self.post('/series', data_json={
            'series_ids': ['={{sid=%s}}' % series_ids[0], '={{sid=%s}}' % series_ids[-3]]
        }, max_points=-1, date_format='iso', operators='@sample:2', expect_error='invalid_parameters')
        self.assertEqual(res['description'], "operator SAMPLE is not supported")

    def test_max_points_operator(self):
        res = self.get('/series', series_id="{}@max_points:-1".format(series_ids[0]), date_format='iso')['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0],
                                           [u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0],
                                           [u'2003-12-31T00:00:00', 103.0000009],
                                           [u'2004-01-31T00:00:00', 1e-15]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1'})

        # operator overrides parameter if operator is less
        res = self.get('/series', series_id="{}@max_points:5".format(series_ids[0]), date_format='iso', max_points=10)['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0],
                                           [u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:5'})

        # operator overrides parameter if operator is greater
        res = self.get('/series', series_id="{}@max_points:5".format(series_ids[0]), date_format='iso', max_points=2)['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0],
                                           [u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:5'})

        # we read series using max_points:-1, but then cut to 3
        res = self.get('/series', series_id="={{{{sid={}@max_points:-1}}}}".format(series_ids[0]),
                       date_format='iso', max_points=3)['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0], ],
                               u'series_id': u'={{sid=sjvirt\\tests\\0@max_points:-1}}',
                               u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        res = self.get('/series', series_id="={{{{sid={}@max_points:1}}}}".format(series_ids[0]),
                       date_format='iso', max_points=-1)['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1]],
                               u'series_id': u'={{sid=sjvirt\\tests\\0@max_points:1}}',
                               u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

    def test_df_dt_operator(self):
        # reversed interval
        res = self.get('/series',
                       series_id="{}@max_points:-1@df:2004-01-31@dt:2001-04-01".format(series_ids[0]),
                       date_format='iso')['series'][0]
        self.assertEqual(res, {u'points': [[u'2004-01-31T00:00:00', 1e-15],
                                           [u'2003-12-31T00:00:00', 103.0000009],
                                           [u'2003-05-01T00:00:00', 909.0],
                                           [u'2002-02-01T00:00:00', -1000.22]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@df:2004-01-31@dt:2001-04-01'})

        # conflicting interval directions - operator preferred
        res = self.get('/series',
                       series_id="{}@max_points:-1@df:2004-01-31@dt:2001-04-01".format(series_ids[0]),
                       df='2001-04-01',
                       dt='2004-01-31',
                       date_format='iso')['series'][0]
        self.assertEqual(res, {u'points': [[u'2004-01-31T00:00:00', 1e-15],
                                           [u'2003-12-31T00:00:00', 103.0000009],
                                           [u'2003-05-01T00:00:00', 909.0],
                                           [u'2002-02-01T00:00:00', -1000.22]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@df:2004-01-31@dt:2001-04-01'})

        # null intersection
        res = self.get('/series',
                     series_id='{}@max_points:-1@df:2001-04-01@dt:2004-01-31'.format(series_ids[0]),
                     df='2010-04-11',
                     date_format='iso')
        self.assertNotIn('points', res['series'][0])

        res = self.get('/series',
                       series_id="{}@max_points:-1@df:2001-04-01".format(series_ids[0]),
                       date_format='iso', )['series'][0]
        self.assertEqual(res, {u'points': [[u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0],
                                           [u'2003-12-31T00:00:00', 103.0000009],
                                           [u'2004-01-31T00:00:00', 1e-15]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@df:2001-04-01'})

        res = self.get('/series',
                       series_id="={{{{sid={}@max_points:-1@df:2001-04-01}}}}".format(series_ids[0]),
                       date_format='iso', max_points=-1)['series'][0]
        self.assertEqual(res, {u'points': [[u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0],
                                           [u'2003-12-31T00:00:00', 103.0000009],
                                           [u'2004-01-31T00:00:00', 1e-15]],
                               u'series_id': u'={{sid=sjvirt\\tests\\0@max_points:-1@df:2001-04-01}}',
                               u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        res = self.get('/series',
                       series_id="={{{{sid={}@max_points:2@df:2001-04-01}}}}".format(series_ids[0]),
                       date_format='iso', max_points=-1)['series'][0]
        self.assertEqual(res, {u'points': [[u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0]],
                               u'series_id': u'={{sid=sjvirt\\tests\\0@max_points:2@df:2001-04-01}}',
                               u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        # parameter overrides operator if df operator is less
        res = self.get('/series',
                       series_id="{}@max_points:-1@df:2001-04-01".format(series_ids[0]),
                       date_format='iso',
                       df='2003-12-31')['series'][0]
        self.assertEqual(res, {u'points': [[u'2003-12-31T00:00:00', 103.0000009],
                                           [u'2004-01-31T00:00:00', 1e-15]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@df:2001-04-01'})

        # operator overrides parameter if df operator is greater
        res = self.get('/series',
                       series_id="{}@max_points:-1@df:2001-04-01".format(series_ids[0]),
                       date_format='iso',
                       df='2001-01-01')['series'][0]
        self.assertEqual(res, {u'points': [[u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0],
                                           [u'2003-12-31T00:00:00', 103.0000009],
                                           [u'2004-01-31T00:00:00', 1e-15]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@df:2001-04-01'})

        res = self.get('/series',
                       series_id="{}@max_points:-1@dt:2003-05-01".format(series_ids[0]),
                       date_format='iso')['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0],
                                           [u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@dt:2003-05-01'})

        res = self.get('/series',
                       series_id="={{{{sid={}@max_points:-1@dt:2003-05-01}}}}".format(series_ids[0]),
                       date_format='iso', max_points=-1)['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0],
                                           [u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0]],
                               u'series_id': u'={{sid=sjvirt\\tests\\0@max_points:-1@dt:2003-05-01}}',
                               u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        res = self.get('/series',
                       series_id="={{{{sid={}@max_points:2@dt:2003-05-01}}}}".format(series_ids[0]),
                       date_format='iso', max_points=-1)['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9]],
                               u'series_id': u'={{sid=sjvirt\\tests\\0@max_points:2@dt:2003-05-01}}',
                               u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        # operator overrides parameter if dt operator is less
        res = self.get('/series',
                       series_id="{}@max_points:-1@dt:2003-05-01".format(series_ids[0]),
                       date_format='iso',
                       dt='2010-01-01')['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0],
                                           [u'2002-02-01T00:00:00', -1000.22],
                                           [u'2003-05-01T00:00:00', 909.0]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@dt:2003-05-01'})

        # parameter overrides operator if dt operator is greater
        res = self.get('/series',
                       series_id="{}@max_points:-1@dt:2003-05-01".format(series_ids[0]),
                       date_format='iso',
                       dt='2001-03-01')['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@dt:2003-05-01'})

        # regression test for datetime with hours fraction
        res = self.get('/series',
                       series_id="{}@max_points:-1@dt:2003-05-01T00:00:00".format(series_ids[0]),
                       date_format='iso',
                       dt='2001-03-01')['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-01T00:00:00', 100.1],
                                           [u'2001-02-01T00:00:00', -1.9],
                                           [u'2001-03-01T00:00:00', 0.0]],
                               u'series_id': u'sjvirt\\tests\\0@max_points:-1@dt:2003-05-01T00:00:00'})

    def test_localize_operator(self):
        # test simple GET
        res = self.get('/series',
                       series_id='{}@localize:America/Chicago'.format(series_ids[0]),
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso')['series'][0]
        self.assertEqual(res, {u'series_id': u'sjvirt\\tests\\0@localize:America/Chicago',
                               u'tz': {u'timezone': u'America/Chicago'},
                               u'points': [[u'2001-01-31T18:00:00', -1.9],
                                           [u'2001-02-28T18:00:00', 0.0],
                                           [u'2002-01-31T18:00:00', -1000.22],
                                           [u'2003-04-30T19:00:00', 909.0],
                                           [u'2003-12-30T18:00:00', 103.0000009],
                                           [u'2004-01-30T18:00:00', 1e-15]]})
        # test override of returned tz meta
        res = self.get('/series',
                       series_id='{}@localize:America/Chicago@forcetz:UTC'.format(series_ids[0]),
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso')['series'][0]
        self.assertEqual(res, {u'series_id': u'sjvirt\\tests\\0@localize:America/Chicago@forcetz:UTC',
                               u'tz': {u'timezone': u'UTC'},
                               u'points': [[u'2001-01-31T18:00:00', -1.9],
                                           [u'2001-02-28T18:00:00', 0.0],
                                           [u'2002-01-31T18:00:00', -1000.22],
                                           [u'2003-04-30T19:00:00', 909.0],
                                           [u'2003-12-30T18:00:00', 103.0000009],
                                           [u'2004-01-30T18:00:00', 1e-15]]})

        # test simple GET
        res = self.get('/series',
                       series_id='{}@localize'.format(series_ids[0]),
                       max_points=-1, df='2001-01-01', dt='2016-01-01', date_format='iso')['series'][0]
        self.assertEqual(res, {u'points': [[u'2001-01-31T18:00:00', -1.9],
                                           [u'2001-02-28T18:00:00', 0.0],
                                           [u'2002-01-31T18:00:00', -1000.22],
                                           [u'2003-04-30T19:00:00', 909.0],
                                           [u'2003-12-30T18:00:00', 103.0000009],
                                           [u'2004-01-30T18:00:00', 1e-15]],
                               u'series_id': u'sjvirt\\tests\\0@localize',
                               u'tz': {u'timezone': u'America/Chicago'}})

        # test POST

        series = self.post('/series', data_json={
            'series_ids': [series_ids[0], series_ids[-3]]
        }, max_points=-1, date_format='iso', operators='@localize', sort='l3 asc')['series']

        # second series has no timezone field, so we ignore it
        self.assertEqual(series,
                         [{u'points': [[u'2000-12-31T18:00:00', 100.1],
                                       [u'2001-01-31T18:00:00', -1.9],
                                       [u'2001-02-28T18:00:00', 0.0],
                                       [u'2002-01-31T18:00:00', -1000.22],
                                       [u'2003-04-30T19:00:00', 909.0],
                                       [u'2003-12-30T18:00:00', 103.0000009],
                                       [u'2004-01-30T18:00:00', 1e-15]],
                           u'series_id': u'sjvirt\\tests\\0@localize',
                           u'tz': {u'timezone': u'America/Chicago'}},
                          {u'points': [[u'2001-01-01T00:00:00', 1.0],
                                       [u'2001-02-01T00:00:00', 2.0],
                                       [u'2001-03-01T00:00:00', 3.0],
                                       [u'2002-02-01T00:00:00', 3.0],
                                       [u'2003-05-01T00:00:00', 5.0],
                                       [u'2003-12-31T00:00:00', 6.0],
                                       [u'2004-01-31T00:00:00', 7.0]],
                           u'series_id': u'sjvirt\\tests\\3@localize'}])
        # we don't return tz key on more than daily agg
        series = self.post('/series', data_json={
            'series_ids': ['%s@S:M' % series_ids[0], series_ids[-3]]
        }, max_points=-1, date_format='iso', operators='@localize', sort='l3 asc')['series']

        self.assertEqual(series,
                         [{u'points': [[u'2000-12-01T00:00:00', 100.1],
                                       [u'2001-01-01T00:00:00', -1.9],
                                       [u'2001-02-01T00:00:00', 0.0],
                                       [u'2002-01-01T00:00:00', -1000.22],
                                       [u'2003-04-01T00:00:00', 909.0],
                                       [u'2003-12-01T00:00:00', 103.0000009],
                                       [u'2004-01-01T00:00:00', 1e-15]],
                           u'series_id': u'sjvirt\\tests\\0@S:M@localize'},
                          {u'points': [[u'2001-01-01T00:00:00', 1.0],
                                       [u'2001-02-01T00:00:00', 2.0],
                                       [u'2001-03-01T00:00:00', 3.0],
                                       [u'2002-02-01T00:00:00', 3.0],
                                       [u'2003-05-01T00:00:00', 5.0],
                                       [u'2003-12-31T00:00:00', 6.0],
                                       [u'2004-01-31T00:00:00', 7.0]],
                           u'series_id': u'sjvirt\\tests\\3@localize'}])

        self.get('/series', series_id='={{sid=%s}} * {{sid=%s}}' % (series_ids[0], series_ids[1]),
                 fields='aspect', date_format='iso',
                 max_points=-1, operators='@localize', expect_error='expression', sort='l3 asc')

    def test_mget_responses_order(self):
        sids = [series_ids[0], '=1+1', '=1+1', series_ids[0], series_ids[-1], series_ids[-2], '=1+1']
        resp = self.post('/series', data_json={
            'series_ids': sids
        }, df='1970-01-01', dt='2018-01-01', date_format='iso')
        self.assertEqual([s['series_id'] for s in resp['series']], sids)

    def test_simple_read_operations(self):
        # test simple GET
        res = self.get('/series',
                       series_id=series_ids[0], max_points=10, df='2001-01-01', dt='2016-01-01', date_format='iso')
        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 100.1],
                                                      [u'2001-02-01T00:00:00', -1.9],
                                                      [u'2001-03-01T00:00:00', 0.0],
                                                      [u'2002-02-01T00:00:00', -1000.22],
                                                      [u'2003-05-01T00:00:00', 909],
                                                      [u'2003-12-31T00:00:00', 103.0000009],
                                                      [u'2004-01-31T00:00:00', 1e-15]])

        # check snapshot date (should raise invalid params exception)
        # self.get('/series',
        #          series_id=series_ids[0], max_points=10, df='2001-01-01', dt='2016-01-01',
        #          date_format='iso', snapshot_date='2000-01-01', expect_error='invalid_parameters')

        # check reported dates
        res = self.get('/series',
                       series_id=series_ids[0] + '@repdate:2000-01-01', max_points=10, df='2001-01-01', dt='2016-01-01',
                       date_format='iso')

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 2100.1],
                                                      [u'2001-02-01T00:00:00', 1998.1],
                                                      [u'2001-03-01T00:00:00', 2000.0],
                                                      [u'2002-02-01T00:00:00', 999.78],
                                                      [u'2003-05-01T00:00:00', 2909.0],
                                                      [u'2003-12-31T00:00:00', 2103.0000009],
                                                      [u'2004-01-31T00:00:00', 2000.0]])

        res = self.get('/series',
                       series_id='{}@repdate:2000-01-01'.format(series_ids[0]),
                       max_points=10, df='2001-01-01', dt='2016-01-01',
                       date_format='iso')

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 2100.1],
                                                      [u'2001-02-01T00:00:00', 1998.1],
                                                      [u'2001-03-01T00:00:00', 2000.0],
                                                      [u'2002-02-01T00:00:00', 999.78],
                                                      [u'2003-05-01T00:00:00', 2909.0],
                                                      [u'2003-12-31T00:00:00', 2103.0000009],
                                                      [u'2004-01-31T00:00:00', 2000.0]])

        # with timezone
        res = self.get('/series',
                       series_id='{}@repdate:2000-01-01:UTC'.format(series_ids[0]),
                       max_points=10, df='2001-01-01', dt='2016-01-01',
                       date_format='iso')

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 2100.1],
                                                      [u'2001-02-01T00:00:00', 1998.1],
                                                      [u'2001-03-01T00:00:00', 2000.0],
                                                      [u'2002-02-01T00:00:00', 999.78],
                                                      [u'2003-05-01T00:00:00', 2909.0],
                                                      [u'2003-12-31T00:00:00', 2103.0000009],
                                                      [u'2004-01-31T00:00:00', 2000.0]])

        # get reported_dates
        expected = [978307200000, 978393600000, 978480000000, 978566400000, 978652800000, 978739200000, 978825600000,
                    978912000000, 978998400000, 979084800000, 979171200000, 979257600000, 979344000000, 979430400000,
                    979516800000, 979603200000, 979689600000, 979776000000, 979862400000, 979948800000, 980035200000,
                    980121600000, 980208000000, 980294400000, 980380800000, 980467200000, 980553600000, 980640000000,
                    980726400000, 980812800000]
        res = self.get('/series/reported_dates', series_id=series_ids[0])
        self.assertEqual(res, {u'reported_dates': expected, u'success': True})

        res = self.get('/series/reported_dates', series_query='sid={}'.format(series_ids[0]))
        self.assertEqual(res, {u'reported_dates': expected, u'success': True})

        # should have empty
        self.assertEqual(self.get('/series/reported_dates', series_id=series_ids[3],)['reported_dates'], [])

        # check time range
        res = self.get('/series',
                       series_id=series_ids[0], max_points=10, df='2001-01-01', dt='2001-12-31', date_format='iso')
        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 100.1],
                                                      [u'2001-02-01T00:00:00', -1.9],
                                                      [u'2001-03-01T00:00:00', 0.0]])
        days = (parse('2001-01-01') - datetime.utcnow()).days

        # check relative delta
        res = self.get('/series',
                       series_id=series_ids[0], max_points=10, df='{}d'.format(days),
                       dt='{}d'.format(days + 100), date_format='iso', )['series'][0]['points']
        self.assertEqual(res, [[u'2001-01-01T00:00:00', 100.1],
                               [u'2001-02-01T00:00:00', -1.9],
                               [u'2001-03-01T00:00:00', 0.0]])

        res = self.get('/series',
                       series_id=series_ids[0], max_points=10, df='{}d'.format(days),
                       dt='{}d'.format(days + 50), date_format='iso', )['series'][0]['points']
        self.assertEqual(res, [[u'2001-01-01T00:00:00', 100.1],
                               [u'2001-02-01T00:00:00', -1.9]])

        self.get('/series', series_id=series_ids[0], df='-1z', expect_error='invalid_parameters')

        # check max points
        res = self.get('/series',
                       series_id=series_ids[0], max_points=1, df='2001-01-01', dt='2001-12-31', date_format='iso')

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 100.1]])
        self.assertFalse(res['points_desc'])

        # check points order
        res = self.get('/series',
                       series_id=series_ids[0], max_points=10, df='2001-12-31', dt='2001-01-01', date_format='iso')

        self.assertEqual(res['series'][0]['points'], [[u'2001-03-01T00:00:00', 0.0],
                                                      [u'2001-02-01T00:00:00', -1.9],
                                                      [u'2001-01-01T00:00:00', 100.1]])

        # check points desc
        self.assertTrue(res['points_desc'])

        # try sid query
        res = self.get('/series',
                       series_id='$sid={}'.format(series_ids[0]),
                       max_points=10, df='2001-12-31', dt='2001-01-01', date_format='iso')

        self.assertEqual(res['series'][0]['points'], [[u'2001-03-01T00:00:00', 0.0],
                                                      [u'2001-02-01T00:00:00', -1.9],
                                                      [u'2001-01-01T00:00:00', 100.1]])

        # check non-virt shooju series
        res = self.get('/series',
                       series_id=series_ids[3], max_points=10, df='2001-12-31', dt='2001-01-01', date_format='iso')

        self.assertEqual(res['series'][0]['points'],
                         [[u'2001-03-01T00:00:00', 3.0],
                          [u'2001-02-01T00:00:00', 2.0],
                          [u'2001-01-01T00:00:00', 1.0]])

        # test fields
        res = self.get('/series',
                       series_id=series_ids[0], fields='*')['series'][0]
        self.assertEqual(res['fields'], series_by_sid[series_ids[0]]['fields'])

        # we only get vrt field if we explicitly requested it
        res = self.get('/series',
                       series_id=series_ids[0])['series'][0]
        self.assertNotIn('fields', res)

        res = self.get('/series',
                       series_id=series_ids[0], fields='vrt,aspect')['series'][0]
        self.assertEqual(res['fields'], {u'aspect': u'Oil', u'vrt': {u'source': u'test'}})

        ser = self.get('/series', query='sid=%s' % series_ids[0], fields='*')['series'][0]

        self.assertEquals(ser, {u'series_id': u'sjvirt\\tests\\0',
                                u'fields': {u'timezone': u'America/Chicago',
                                            u'aspect': u'Oil', u'vrt': {u'source': u'test'}}})

        ser = self.get('/series', query='sid=%s' % series_ids[0], fields='timezone, vrt.source')['series'][0]

        self.assertEquals(ser, {u'series_id': u'sjvirt\\tests\\0',
                                u'fields': {u'timezone': u'America/Chicago', u'vrt.source': u'test'}})

        ser = self.get('/series', query='sid=%s' % series_ids[0], fields='timezone, ')['series'][0]

        self.assertEquals(ser, {u'series_id': u'sjvirt\\tests\\0',
                                u'fields': {u'timezone': u'America/Chicago'}})

        # test fields in multi get
        res = self.post('/series', data_json={
            'series_queries': [
                'sid={}'.format(series_ids[0]),
                {'query': 'sid={}'.format(series_ids[-3]), 'max_points': 2,
                 'date_format': 'milli', 'fields': 'meta.updated_at'},
            ]
        }, max_points=-1, date_format='iso', fields='aspect, ')

        self.assertEqual(res['series'][0]['fields'], {u'aspect': u'Oil'})
        self.assertEqual(list(res['series'][1]['fields'].keys()), ['meta.updated_at'])

        res = self.post('/series', data_json={
            'series_queries': [
                'sid={}'.format(series_ids[0]),
            ]
        }, max_points=-1, date_format='iso', fields='*')

        self.assertEqual(res['series'][0]['fields'], {u'timezone': u'America/Chicago',
                                                      u'aspect': u'Oil',
                                                      u'vrt': {u'source': u'test'}})
        # test queries
        res = self.get('/series', query='not sid:system (aspect=Wind OR aspect=Sun)', per_page=2, max_points=3,
                       date_format='iso', sort='l3 asc', fields='sid ,aspect')

        self.assertEqual(res['series'][0]['fields'], {u'aspect': u'Wind', u'sid': u'sjvirt\\tests\\2'})
        self.assertEqual(res['series'][1]['fields'], {u'aspect': u'Sun', u'sid': u'sjvirt\\tests\\3'})

        # one of these is virt, another one is regular series
        res = self.get('/series', query='sid:sjvirt\\tests (aspect=Wind OR aspect=Sun)', per_page=2, max_points=3,
                       date_format='iso', sort='l3 asc')
        self.assertEqual(res['total'], 2)
        self.assertEqual(len(res['series']), 2)

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 1000],
                                                      [u'2002-01-01T00:00:00', 2000],
                                                      [u'2003-01-01T00:00:00', 3000]])

        self.assertEqual(res['series'][1]['points'], [[u'2001-01-01T00:00:00', 1.0],
                                                      [u'2001-02-01T00:00:00', 2.0],
                                                      [u'2001-03-01T00:00:00', 3.0]])

        self.assertEqual([s['series_id'] for s in res['series']], series_ids[2:4])

        # sorting is working of course
        res = self.get('/series', query='not sid:system (aspect=Wind OR aspect=Sun)',
                       per_page=2, max_points=3, date_format='iso', sort='series_id desc')

        self.assertEqual([s['series_id'] for s in res['series']], list(reversed(series_ids[2:4])))

        # test POST series_ids

        # one virt, one not here
        res = self.post('/series', data_json={
            'series_ids': [series_ids[0], series_ids[-3]]
        }, max_points=2, date_format='iso', sort='l3 asc')

        self.assertEqual(res['total'], 2)
        self.assertEqual(len(res['series']), 2)

        self.assertEqual(res['series'][0]['points'],
                         [[u'2001-01-01T00:00:00', 100.1], [u'2001-02-01T00:00:00', -1.9]])

        self.assertEqual(res['series'][1]['points'],
                         [[u'2001-01-01T00:00:00', 1.0], [u'2001-02-01T00:00:00', 2.0]])

        # test POST series
        res = self.post('/series', data_json={
            'series': [
                {'series_id': series_ids[0], 'max_points': 1, 'date_format': 'iso'},
                {'series_id': series_ids[-3], 'max_points': 2, 'date_format': 'milli'},
            ]
        })

        self.assertEqual(res['total'], 2)
        self.assertEqual(len(res['series']), 2)

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 100.1]])
        self.assertEqual(res['series'][1]['points'], [[978307200000, 1.0], [980985600000, 2.0]])

        # test series queries
        res = self.post('/series', data_json={
            'series_queries': [
                'sid={}'.format(series_ids[0]),
                {'query': 'sid={}'.format(series_ids[-3]), 'max_points': 2, 'date_format': 'milli'},
            ]
        }, max_points=-1, date_format='iso')

        self.assertEqual(res['total'], 2)
        self.assertEqual(len(res['series']), 2)

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 100.1],
                                                      [u'2001-02-01T00:00:00', -1.9],
                                                      [u'2001-03-01T00:00:00', 0.0],
                                                      [u'2002-02-01T00:00:00', -1000.22],
                                                      [u'2003-05-01T00:00:00', 909.0],
                                                      [u'2003-12-31T00:00:00', 103.0000009],
                                                      [u'2004-01-31T00:00:00', 1e-15]])
        self.assertEqual(res['series'][1]['points'], [[978307200000, 1.0], [980985600000, 2.0]])

        # test series queries stripped
        res = self.post('/series', data_json={
            'series_queries': [
                '\r\nsid={}\n'.format(series_ids[0]),
            ]
        }, max_points=-1, date_format='iso')

        self.assertEqual(res['total'], 1)
        self.assertEqual(len(res['series']), 1)

        self.assertEqual(res['series'][0]['points'], [[u'2001-01-01T00:00:00', 100.1],
                                                      [u'2001-02-01T00:00:00', -1.9],
                                                      [u'2001-03-01T00:00:00', 0.0],
                                                      [u'2002-02-01T00:00:00', -1000.22],
                                                      [u'2003-05-01T00:00:00', 909.0],
                                                      [u'2003-12-31T00:00:00', 103.0000009],
                                                      [u'2004-01-31T00:00:00', 1e-15]])

    def test_read_multiple_series_with_same_sid(self):
        res = self.post('/series', data_json={
            'series_ids': [
                "{}@df:2001-01-01@dt:2002-01-01".format(series_ids[0]),
                "{}@df:2003-01-01@dt:2004-01-01".format(series_ids[0]),
                "{}@df:2010-01-01T04:00:00@dt:2010-01-01T01:00:00".format(series_ids[1]),
                "{}@df:2010-01-01T03:00:00@dt:2010-01-01T02:00:00".format(series_ids[1]),
                "{}".format(series_ids[1]),
                "{}".format(series_ids[2]),
            ]
        }, max_points=-1, date_format='iso')['series']

        self.assertEqual(res, [
            {
                'series_id': 'sjvirt\\tests\\0@df:2001-01-01@dt:2002-01-01',
                'points': [['2001-01-01T00:00:00', 100.1], ['2001-02-01T00:00:00', -1.9], ['2001-03-01T00:00:00', 0.0]]
            },
            {
                'series_id': 'sjvirt\\tests\\0@df:2003-01-01@dt:2004-01-01',
                'points': [['2003-05-01T00:00:00', 909.0], ['2003-12-31T00:00:00', 103.0000009]]
            },
            {
                'series_id': 'sjvirt\\tests\\1@df:2010-01-01T04:00:00@dt:2010-01-01T01:00:00',
                'points': [['2010-01-01T04:00:00', 0.1], ['2010-01-01T03:00:00', 40.1],
                           ['2010-01-01T02:00:00', 30.1], ['2010-01-01T01:00:00', 20.1]]
            },
            {
                'series_id': 'sjvirt\\tests\\1@df:2010-01-01T03:00:00@dt:2010-01-01T02:00:00',
                'points': [['2010-01-01T03:00:00', 40.1], ['2010-01-01T02:00:00', 30.1]]
            },
            {
                'series_id': 'sjvirt\\tests\\1',
                'points': [['2010-01-01T01:00:00', 20.1], ['2010-01-01T02:00:00', 30.1], ['2010-01-01T03:00:00', 40.1],
                           ['2010-01-01T04:00:00', 0.1], ['2010-01-01T05:00:00', -80.0], ['2010-01-01T06:00:00', -2.0],
                           ['2010-01-01T07:00:00', 10.0]]
            },
            {
                'series_id': 'sjvirt\\tests\\2',
                'points': [['2001-01-01T00:00:00', 1000.0], ['2002-01-01T00:00:00', 2000.0],
                           ['2003-01-01T00:00:00', 3000.0], ['2004-01-01T00:00:00', 4000.0],
                           ['2005-01-01T00:00:00', 5000.0], ['2006-01-01T00:00:00', 6000.0],
                           ['2007-01-01T00:00:00', 7000.0]]}
        ])

    def test_xpr_global_operators(self):
        obj = {
            "fields": {'xpr': {'expression': r'={{aspect=Oil@lag:9y 6h}} + {{aspect=Gas@lag:5h}}'}},
            "points": {}
        }
        series_id = r'sjvirt\tests\xpr_global_operators'

        bulk_body = {'requests': [{'type': 'POST', 'id': series_id, 'body': obj}]}
        r = self.post('/series/bulk', job_id=self.job_id, date_format='iso', data_json=bulk_body)

        # check queries
        self.sj.raw.post('/series/refresh')

        res = self.get('/series', series_id=r'={{sid=%s}}@S:y' % (series_id,), max_points=-1, date_format='iso')['series'][0]
        self.assertEqual(res['points'], [['2010-01-01T00:00:00', 120.19999999999999]])

        bulk_body = {'requests': [{'id': series_id, 'type': 'DELETE'}]}
        self.post('/series/bulk', data_json=bulk_body)

    def test_scroll_with_operators(self):
        res = self.get('/series', query='sid:sjvirt\\tests aspect=Gas@lag:-1h', operators='@S:y', scroll='y', max_points=-1, date_format='iso')['series'][0]
        self.assertEqual(res, {
            'series_id': "sjvirt\\tests\\1@lag:-1h@S:y",
            'points': [['2010-01-01T00:00:00', 18.400000000000006]],
        })

    @retry(times=3)
    def test_scroll_with_include_something(self):
        def raw_get(path, expect_error=None, headers=None, **kwargs):
            h = dict(self.request_executor.auth_headers)
            if headers:
                h.update(headers)
            resp = self.client.get('/api/1{}'.format(path), headers=h, query_string=kwargs).data
            return resp

        raw_res = raw_get('/series', query='sid:sjvirt\\tests and not set:xpr',
                          include_timestamp='y',
                          scroll='y',
                          max_points=-1,
                          scroll_batch_size=1,
                          headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)

        scroll_id = res['scroll_id']
        self.assertEqual(len(res['series'][0]['points'][0]), 3)

        raw_res = raw_get('/series',
                          scroll_id=scroll_id,
                          headers=[('Sj-Receive-Format', 'sjts')])
        res = sjts.loads(raw_res)
        self.assertEqual(len(res['series'][0]['points'][0]), 3)

    def test_operators(self):
        # TODO:
        # don't support job id snapshot for virt series
        # points = self.get('/series', series_id='{}@asof:j1000000'.format(series_ids[5]),
        #                   date_format='iso', max_points=10, df='2001-01-01', dt='2016-01-01', expect_error='expression')

        # test single get
        # monthly average
        points = self.get('/series', series_id='{}@YA'.format(series_ids[0]),
                          date_format='iso', max_points=10, df='2001-01-01', dt='2016-01-01')['series'][0]['points']

        self.assertEqual(points, [[u'2001-01-01T00:00:00', 32.73333333333333],
                                  [u'2002-01-01T00:00:00', -1000.22],
                                  [u'2003-01-01T00:00:00', 506.00000045],
                                  [u'2004-01-01T00:00:00', 1e-15]])

        points = self.get('/series', series_id='{}@A:1y'.format(series_ids[0]),
                          date_format='iso', max_points=10, df='2001-01-01', dt='2016-01-01')['series'][0]['points']

        self.assertEqual(points, [[u'2001-01-01T00:00:00', 32.73333333333333],
                                  [u'2002-01-01T00:00:00', -1000.22],
                                  [u'2003-01-01T00:00:00', 506.00000045],
                                  [u'2004-01-01T00:00:00', 1e-15]])

        # hourly average
        points = self.get('/series', series_id='{}@HA'.format(series_ids[1]),
                          date_format='iso', max_points=10, df='2001-01-01', dt='2016-01-01')['series'][0]['points']

        self.assertEqual(points, [[u'2010-01-01T01:00:00', 20.1],
                                  [u'2010-01-01T02:00:00', 30.1],
                                  [u'2010-01-01T03:00:00', 40.1],
                                  [u'2010-01-01T04:00:00', 0.1],
                                  [u'2010-01-01T05:00:00', -80],
                                  [u'2010-01-01T06:00:00', -2.0],
                                  [u'2010-01-01T07:00:00', 10]])

        points = self.get('/series', series_id='{}@A:1h'.format(series_ids[1]),
                          date_format='iso', max_points=10, df='2001-01-01', dt='2016-01-01')['series'][0]['points']

        self.assertEqual(points, [[u'2010-01-01T01:00:00', 20.1],
                                  [u'2010-01-01T02:00:00', 30.1],
                                  [u'2010-01-01T03:00:00', 40.1],
                                  [u'2010-01-01T04:00:00', 0.1],
                                  [u'2010-01-01T05:00:00', -80],
                                  [u'2010-01-01T06:00:00', -2.0],
                                  [u'2010-01-01T07:00:00', 10]])

        # check @filter operator
        points = self.get('/series', series_id='{}@filter:1h'.format(series_ids[1]), date_format='iso', max_points=-1)['series'][0]['points']
        self.assertEqual(points, [[u'2010-01-01T01:00:00', 20.1]])

        # check @filter operator for points for points
        points = self.get('/series', series_id='{}@filter:>10'.format(series_ids[1]), date_format='iso', max_points=-1)['series'][0]['points']
        self.assertEqual(points, [[u'2010-01-01T01:00:00', 20.1],
                                  [u'2010-01-01T02:00:00', 30.1],
                                  [u'2010-01-01T03:00:00', 40.1]])

        # check how auto agg works
        resp = self.get('/series', series_id='{}@AS:1'.format(series_ids[1]),
                        date_format='iso', max_points=1000)['series'][0]

        # this should be daily agg
        self.assertEqual(resp, {u'series_id': u'sjvirt\\tests\\1@AS:1', u'auto_agg': u'@DS',
                                u'points': [[u'2010-01-01T00:00:00', 18.400000000000006]]})

        # no aggs needed for second series id

        resp = self.post('/series', data_json={
            'series_ids': ['{}@AS:1'.format(series_ids[0]), '{}@AA'.format(series_ids[-3])]
        }, date_format='iso', max_points=1000)['series']

        self.assertEqual(resp, [{u'series_id': u'sjvirt\\tests\\0@AS:1',
                                 u'points': [[u'2001-01-01T00:00:00', 98.19999999999999],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 1012.0000009],
                                             [u'2004-01-01T00:00:00', 1e-15]],
                                 u'auto_agg': u'@YS'},
                                {u'series_id': u'sjvirt\\tests\\3@AA',
                                 u'auto_agg': None,
                                 u'points': [[u'2001-01-01T00:00:00', 1.0],
                                             [u'2001-02-01T00:00:00', 2.0],
                                             [u'2001-03-01T00:00:00', 3.0],
                                             [u'2002-02-01T00:00:00', 3.0],
                                             [u'2003-05-01T00:00:00', 5.0],
                                             [u'2003-12-31T00:00:00', 6.0],
                                             [u'2004-01-31T00:00:00', 7.0]]}])

        # test also shooju series
        points = self.get('/series', series_id='{}@YA'.format(series_ids[-3]),
                          date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')['series'][0]['points']

        self.assertEqual(points,
                         [[u'2001-01-01T00:00:00', 2.0],
                          [u'2002-01-01T00:00:00', 3.0],
                          [u'2003-01-01T00:00:00', 5.5],
                          [u'2004-01-01T00:00:00', 7.0]])

        # test with filter
        points = self.get('/series', series_id='{}@YA'.format(series_ids[0]),
                          date_format='iso', max_points=10, df='2001-01-01', dt='2003-05-01')['series'][0]['points']

        self.assertEqual(
            points,
            [[u'2001-01-01T00:00:00', 32.73333333333333],
             [u'2002-01-01T00:00:00', -1000.22],
             [u'2003-01-01T00:00:00', 506.00000045]]
        )

        # test different date format
        points = self.get('/series', series_id='{}@YA'.format(series_ids[0]),
                          date_format='milli', max_points=10, df=0, dt=1451606400000)['series'][0]['points']

        self.assertEqual(points, [[978307200000, 32.73333333333333],
                                  [1009843200000, -1000.22],
                                  [1041379200000, 506.00000045],
                                  [1072915200000, 1e-15]])

        # test high/low operators
        points = self.get('/series', series_id='{}@YL'.format(series_ids[0]),
                          date_format='iso', max_points=10, df='2003-01-01', dt='2003-12-31')['series'][0]['points']

        self.assertEqual(points, [[u'2003-01-01T00:00:00', 103.0000009]])

        points = self.get('/series', series_id='{}@L:1y'.format(series_ids[0]),
                          date_format='iso', max_points=10, df='2003-01-01', dt='2003-12-31')['series'][0]['points']

        self.assertEqual(points, [[u'2003-01-01T00:00:00', 103.0000009]])

        points = self.get('/series', series_id='{}@YH'.format(series_ids[0]),
                          date_format='iso', max_points=10, df='2003-01-01', dt='2003-12-31')['series'][0]['points']

        self.assertEqual(points, [[u'2003-01-01T00:00:00', 909]])

        # test queries
        resp = self.get('/series', query='(aspect=Oil OR aspect=Sun) AND sid:sjvirt', operators='@YA',
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01', sort='l3 asc')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])

        resp = self.get('/series', query='(aspect=Oil OR aspect=Sun) AND sid:sjvirt', operators='@A:1y',
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01', sort='l3 asc')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])

        # should be the same if we use in-query operators
        resp = self.get('/series', query='(aspect=Oil OR aspect=Sun) AND sid:sjvirt@A:1y',
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01', sort='l3 asc')['series']
        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])

        # they should be combined with request level operators
        resp = self.get('/series', query='(aspect=Oil OR aspect=Sun) AND sid:sjvirt@A:1y',
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01', operators='@localize',
                        sort='l3 asc')['series']

        self.assertTrue(resp, [
            {u'series_id': u'sjvirt\\tests\\0@A:1y@A:1y@localize',
             u'points': [[u'2001-01-01T00:00:00', -0.95],
                         [u'2002-01-01T00:00:00', -1000.22],
                         [u'2003-01-01T00:00:00', 506.00000045],
                         [u'2004-01-01T00:00:00', 1e-15]]},
            {u'series_id': u'sjvirt\\tests\\3@A:1y@A:1y@localize',
             u'points': [[u'2001-01-01T00:00:00', 2.0],
                         [u'2002-01-01T00:00:00', 3.0],
                         [u'2003-01-01T00:00:00', 5.5],
                         [u'2004-01-01T00:00:00', 7.0]]}])

        # test high low operators
        resp = self.get('/series', query='(aspect=Oil OR aspect=Sun) AND sid:sjvirt', operators='@YL',
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01', sort='l3 asc')['series']

        self.assertEqual(resp[0]['points'],
                         [[u'2001-01-01T00:00:00', -1.9],
                          [u'2002-01-01T00:00:00', -1000.22],
                          [u'2003-01-01T00:00:00', 103.0000009],
                          [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'],
                         [[u'2001-01-01T00:00:00', 1.0],
                          [u'2002-01-01T00:00:00', 3.0],
                          [u'2003-01-01T00:00:00', 5.0],
                          [u'2004-01-01T00:00:00', 7.0]])

        resp = self.get('/series', query='(aspect=Oil OR aspect=Sun) AND sid:sjvirt', operators='@L:1y',
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01', sort='l3 asc')['series']

        self.assertEqual(resp[0]['points'],
                         [[u'2001-01-01T00:00:00', -1.9],
                          [u'2002-01-01T00:00:00', -1000.22],
                          [u'2003-01-01T00:00:00', 103.0000009],
                          [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'],
                         [[u'2001-01-01T00:00:00', 1.0],
                          [u'2002-01-01T00:00:00', 3.0],
                          [u'2003-01-01T00:00:00', 5.0],
                          [u'2004-01-01T00:00:00', 7.0]])

        # multi get
        resp = self.post('/series', data_json={
            'series_ids': ['{}@YA'.format(series_ids[0]), '{}@YA'.format(series_ids[-3])]
        }, date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])

        # the same with series queries
        resp = self.post('/series', data_json={
            'series_queries': ['sid={}@YA'.format(series_ids[0]), 'sid={}@YA'.format(series_ids[-3])]
        }, date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])

        resp = self.post('/series', data_json={
            'series': [
                dict(series_id='{}@YA'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}@YA'.format(series_ids[-3]), max_points=1, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2001-01-01'),
            ]
        }, date_format='iso')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0]])

        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 100.1]])

        # with series queries
        resp = self.post('/series', data_json={
            'series_queries': [
                dict(query='sid={}@YA'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2016-01-01'),
                dict(query='sid={}@YA'.format(series_ids[-3]), max_points=1, df='2001-01-01', dt='2016-01-01'),
                dict(query='sid={}'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2001-01-01'),
            ]
        }, date_format='iso')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0]])

        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 100.1]])

        # new operators
        resp = self.post('/series', data_json={
            'series_ids': ['{}@A:1y'.format(series_ids[0]), '{}@A:1y'.format(series_ids[-3])]
        }, date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])
        resp = self.post('/series', data_json={
            'series': [
                dict(series_id='{}@A:1y'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}@A:1y'.format(series_ids[-3]), max_points=1, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2001-01-01'),
            ]
        }, date_format='iso')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 32.73333333333333],
                                             [u'2002-01-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 506.00000045],
                                             [u'2004-01-01T00:00:00', 1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0]])

        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 100.1]])

        # custom operators. multi get
        resp = self.post('/series', data_json={
            'series': [
                dict(series_id='{}@id:1@YA'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}@id:2@YA'.format(series_ids[-3]), max_points=1, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}@id:3'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2001-01-01'),
                dict(series_id='{}'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2001-01-01'),
            ]
        }, date_format='iso')['series']

        self.assertEqual(resp[0]['points'],
                         [[u'2001-01-01T00:00:00', 1.0],
                          [u'2002-01-01T00:00:00', 1.0],
                          [u'2003-01-01T00:00:00', 1.0],
                          [u'2004-01-01T00:00:00', 1.0]])
        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0]])
        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 3.0]])
        self.assertEqual(resp[3]['points'], [[u'2001-01-01T00:00:00', 100.1]])
        self.assertEqual([r['series_id'] for r in resp],
                         [u'sjvirt\\tests\\0@id:1@YA', u'sjvirt\\tests\\3@id:2@YA', u'sjvirt\\tests\\0@id:3', u'sjvirt\\tests\\0'])

        resp = self.post('/series', data_json={
            'series': [
                dict(series_id='{}@id:1@A:1y'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}@id:2@A:1y'.format(series_ids[-3]), max_points=1, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='{}@id:3'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2001-01-01'),
                dict(series_id='{}'.format(series_ids[0]), max_points=10, df='2001-01-01', dt='2001-01-01'),
            ]
        }, date_format='iso')['series']

        self.assertEqual(resp[0]['points'],
                         [[u'2001-01-01T00:00:00', 1.0],
                          [u'2002-01-01T00:00:00', 1.0],
                          [u'2003-01-01T00:00:00', 1.0],
                          [u'2004-01-01T00:00:00', 1.0]])
        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0]])
        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 3.0]])
        self.assertEqual(resp[3]['points'], [[u'2001-01-01T00:00:00', 100.1]])
        self.assertEqual([r['series_id'] for r in resp],
                         [u'sjvirt\\tests\\0@id:1@A:1y', u'sjvirt\\tests\\3@id:2@A:1y', u'sjvirt\\tests\\0@id:3', u'sjvirt\\tests\\0'])

        # if custom operator conflicts with existing Shooju operator we get an error
        self.get('/series', series_id='{}@id:1@wa:y'.format(series_ids[0]),
                 max_points=10, df='2001-01-01', dt='2016-01-01', expect_error='invalid_parameters')

        # by series list
        resp = self.post('/series', data_json={
            'series_ids': [
                '{}@id:1@YA'.format(series_ids[0]),
                '{}@id:2@YA'.format(series_ids[-3]),
                '{}@id:3'.format(series_ids[0]),
                '{}'.format(series_ids[0])
            ]
        }, date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')['series']

        self.assertEqual(resp[0]['points'],
                         [[u'2001-01-01T00:00:00', 1.0],
                          [u'2002-01-01T00:00:00', 1.0],
                          [u'2003-01-01T00:00:00', 1.0],
                          [u'2004-01-01T00:00:00', 1.0]])
        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])

        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 3.0],
                                             [u'2001-02-01T00:00:00', 3.0],
                                             [u'2001-03-01T00:00:00', 3.0],
                                             [u'2002-02-01T00:00:00', 3.0],
                                             [u'2003-05-01T00:00:00', 3.0],
                                             [u'2003-12-31T00:00:00', 3.0],
                                             [u'2004-01-31T00:00:00', 3.0]])
        self.assertEqual(resp[3]['points'], [[u'2001-01-01T00:00:00', 100.1],
                                             [u'2001-02-01T00:00:00', -1.9],
                                             [u'2001-03-01T00:00:00', 0.0],
                                             [u'2002-02-01T00:00:00', -1000.22],
                                             [u'2003-05-01T00:00:00', 909],
                                             [u'2003-12-31T00:00:00', 103.0000009],
                                             [u'2004-01-31T00:00:00', 1e-15]])
        self.assertEqual([r['series_id'] for r in resp],
                         [u'sjvirt\\tests\\0@id:1@YA', u'sjvirt\\tests\\3@id:2@YA', u'sjvirt\\tests\\0@id:3', u'sjvirt\\tests\\0'])

        resp = self.post('/series', data_json={
            'series_ids': [
                '{}@id:1@A:1y'.format(series_ids[0]),
                '{}@id:2@A:1y'.format(series_ids[-3]),
                '{}@id:3'.format(series_ids[0]),
                '{}'.format(series_ids[0])
            ]
        }, date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')['series']

        self.assertEqual(resp[0]['points'],
                         [[u'2001-01-01T00:00:00', 1.0],
                          [u'2002-01-01T00:00:00', 1.0],
                          [u'2003-01-01T00:00:00', 1.0],
                          [u'2004-01-01T00:00:00', 1.0]])
        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                             [u'2002-01-01T00:00:00', 3.0],
                                             [u'2003-01-01T00:00:00', 5.5],
                                             [u'2004-01-01T00:00:00', 7.0]])

        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 3.0],
                                             [u'2001-02-01T00:00:00', 3.0],
                                             [u'2001-03-01T00:00:00', 3.0],
                                             [u'2002-02-01T00:00:00', 3.0],
                                             [u'2003-05-01T00:00:00', 3.0],
                                             [u'2003-12-31T00:00:00', 3.0],
                                             [u'2004-01-31T00:00:00', 3.0]])
        self.assertEqual(resp[3]['points'], [[u'2001-01-01T00:00:00', 100.1],
                                             [u'2001-02-01T00:00:00', -1.9],
                                             [u'2001-03-01T00:00:00', 0.0],
                                             [u'2002-02-01T00:00:00', -1000.22],
                                             [u'2003-05-01T00:00:00', 909],
                                             [u'2003-12-31T00:00:00', 103.0000009],
                                             [u'2004-01-31T00:00:00', 1e-15]])
        self.assertEqual([r['series_id'] for r in resp],
                         [u'sjvirt\\tests\\0@id:1@A:1y', u'sjvirt\\tests\\3@id:2@A:1y', u'sjvirt\\tests\\0@id:3', u'sjvirt\\tests\\0'])

        resp = self.get('/series', series_id='{}@id:1@YA'.format(series_ids[0]),
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')
        self.assertEqual(resp['series'][0]['series_id'], '{}@id:1@YA'.format(series_ids[0]))
        self.assertEqual(resp['series'][0]['points'], [[u'2001-01-01T00:00:00', 1.0],
                                                       [u'2002-01-01T00:00:00', 1.0],
                                                       [u'2003-01-01T00:00:00', 1.0],
                                                       [u'2004-01-01T00:00:00', 1.0]])

        resp = self.get('/series', series_id='{}@id:2@YA'.format(series_ids[-3]),
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')

        self.assertEqual(resp['series'][0]['series_id'], '{}@id:2@YA'.format(series_ids[-3]))
        self.assertEqual(resp['series'][0]['points'], [[u'2001-01-01T00:00:00', 2.0],
                                                       [u'2002-01-01T00:00:00', 3.0],
                                                       [u'2003-01-01T00:00:00', 5.5],
                                                       [u'2004-01-01T00:00:00', 7.0]])

        resp = self.get('/series', series_id='{}@id:3'.format(series_ids[0]),
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')
        self.assertEqual(resp['series'][0]['series_id'], '{}@id:3'.format(series_ids[0]))
        self.assertEqual(resp['series'][0]['points'], [[u'2001-01-01T00:00:00', 3.0],
                                                       [u'2001-02-01T00:00:00', 3.0],
                                                       [u'2001-03-01T00:00:00', 3.0],
                                                       [u'2002-02-01T00:00:00', 3.0],
                                                       [u'2003-05-01T00:00:00', 3.0],
                                                       [u'2003-12-31T00:00:00', 3.0],
                                                       [u'2004-01-31T00:00:00', 3.0]])

        resp = self.get('/series', series_id='{}'.format(series_ids[0]),
                        date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01')

        self.assertEqual(resp['series'][0]['series_id'], '{}'.format(series_ids[0]))
        self.assertEqual(resp['series'][0]['points'], [[u'2001-01-01T00:00:00', 100.1],
                                                       [u'2001-02-01T00:00:00', -1.9],
                                                       [u'2001-03-01T00:00:00', 0.0],
                                                       [u'2002-02-01T00:00:00', -1000.22],
                                                       [u'2003-05-01T00:00:00', 909],
                                                       [u'2003-12-31T00:00:00', 103.0000009],
                                                       [u'2004-01-31T00:00:00', 1e-15]])

        # let's check how facets works
        facets = self.get('/series', query='sid:sjvirt\\tests', facets='set')['facets']['set']['terms']
        self.assertEqual(facets, [{'count': 6, 'term': 'aspect'},
                                  {'count': 6, 'term': 'meta.fieldsbytes_num'},
                                  {'count': 6, 'term': 'meta.points_count'},
                                  {'count': 3, 'term': 'vrt'},
                                  {'count': 3, 'term': 'vrt.source'},
                                  {'count': 2, 'term': 'timezone'},
                                  {'count': 1, 'term': 'meta.dates_max'},
                                  {'count': 1, 'term': 'meta.dates_min'},
                                  {'count': 1, 'term': 'meta.freq'},
                                  {'count': 1, 'term': 'meta.points_max'},
                                  {'count': 1, 'term': 'meta.points_min'},
                                  {'count': 1, 'term': 'xpr'},
                                  {'count': 1, 'term': 'xpr.expression'},
                                  {'count': 1, 'term': 'xpr.g'}])

        facets = self.post('/series', data_json={'series_ids': series_ids}, facets='set')['facets']['set']['terms']
        self.assertEqual(facets, [{'count': 6, 'term': 'aspect'},
                                  {'count': 6, 'term': 'meta.fieldsbytes_num'},
                                  {'count': 6, 'term': 'meta.points_count'},
                                  {'count': 3, 'term': 'vrt'},
                                  {'count': 3, 'term': 'vrt.source'},
                                  {'count': 2, 'term': 'timezone'},
                                  {'count': 1, 'term': 'meta.dates_max'},
                                  {'count': 1, 'term': 'meta.dates_min'},
                                  {'count': 1, 'term': 'meta.freq'},
                                  {'count': 1, 'term': 'meta.points_max'},
                                  {'count': 1, 'term': 'meta.points_min'},
                                  {'count': 1, 'term': 'xpr'},
                                  {'count': 1, 'term': 'xpr.expression'},
                                  {'count': 1, 'term': 'xpr.g'}])

        facets = \
            self.post('/series', data_json={'series': [{'series_id': s} for s in series_ids]}, facets='set')['facets'][
                'set']['terms']
        self.assertEqual(facets, [{'count': 6, 'term': 'aspect'},
                                  {'count': 6, 'term': 'meta.fieldsbytes_num'},
                                  {'count': 6, 'term': 'meta.points_count'},
                                  {'count': 3, 'term': 'vrt'},
                                  {'count': 3, 'term': 'vrt.source'},
                                  {'count': 2, 'term': 'timezone'},
                                  {'count': 1, 'term': 'meta.dates_max'},
                                  {'count': 1, 'term': 'meta.dates_min'},
                                  {'count': 1, 'term': 'meta.freq'},
                                  {'count': 1, 'term': 'meta.points_max'},
                                  {'count': 1, 'term': 'meta.points_min'},
                                  {'count': 1, 'term': 'xpr'},
                                  {'count': 1, 'term': 'xpr.expression'},
                                  {'count': 1, 'term': 'xpr.g'}])

        # test series stats
        stats = [s.get('fields') for s in self.get('/series', query='sid:sjvirt\\tests not set:xpr',
                                                   fields='pts.avg,pts.max,pts.std',
                                                   max_points=10000, sort='l3 asc')['series']]

        for stat1, stat2 in zip(
                stats,
                [{u'pts.max': 909.0, u'pts.avg': 15.711428700000003, u'pts.std': 513.479130977079},
                 {u'pts.max': 40.1, u'pts.avg': 2.6285714285714294, u'pts.std': 36.59366511475193},
                 {u'pts.max': 7000.0, u'pts.avg': 4000.0, u'pts.std': 2000.0},
                 {u'pts.max': 7.0, u'pts.avg': 3.857142857142857, u'pts.std': 2.030381486221699}, None],
        ):
            if stat1 is None:
                self.assertEqual(stat1, stat2)
            else:
                for k, v in stat1.items():
                    self.assertAlmostEqual(v, stat2[k])

        stats = self.get('/series', query='sid:{}'.format(series_ids[0]),
                         fields='pts.avg,pts.max,pts.std', max_points=10000)['series'][0]['fields']
        for k, v in {u'pts.std': 513.479130977079, u'pts.max': 909.0, u'pts.avg': 15.711428700000003}.items():
            self.assertAlmostEqual(v, stats[k])

        # not supported
        self.get('/series', query=r'sid:{}'.format(series_ids[0]), operators="@id:3",
                 date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01',
                 expect_error='invalid_parameters')

    def test_formulas(self):
        # test single series
        ser = self.get('/series', series_id='={{sid=%s}} * G.a' % (series_ids[0],),
                       date_format='iso', max_points=10, g_expression='G.a=100', df='2001-01-01', dt='2015-01-01')['series'][0]

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                         [u'2001-02-01T00:00:00', -190.0],
                                         [u'2001-03-01T00:00:00', 0.0],
                                         [u'2002-02-01T00:00:00', -100022.0],
                                         [u'2003-05-01T00:00:00', 90900.0],
                                         [u'2003-12-31T00:00:00', 10300.00009],
                                         [u'2004-01-31T00:00:00', 1e-13]])

        self.assertNotIn('fields', ser)

        # regression test
        ser = self.get('/series', query='=sjsordf(r"sid:sjvirt\\tests l3=0", max_points=10) * G.a',
                       date_format='iso',  g_expression='G.a=100',
                       max_points=1, df='2001-01-01', dt='2015-01-01')['series'][0]

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 10010.0]])

        # test include things
        ser = self.get('/series', series_id='={{sid=%s}} * 100' % (series_ids[3],),
                       date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01', include_job='y')['series'][0]

        self.assertLessEqual(ser['points'][0][2], self.job_id)

        ser = self.get('/series', series_id='={{sid=%s}} * 100' % (series_ids[3],),
                       max_points=10, df='2001-01-01', dt='2015-01-01',
                       include_job='y', include_timestamp='y')['series'][0]

        def _pts_to_iso(pts, indices):
            for p in pts:
                for ix in indices:
                    p[ix] = render_date(p[ix], 'iso')

        _pts_to_iso(ser['points'], [0, 3])

        self.assertLessEqual(ser['points'][0][2], self.job_id)
        self.assertTrue(ser['points'][0][3].startswith(datetime.today().isoformat()[:10]))

        # only ts will be included
        ser = self.get('/series', series_id='={{sid=%s@B:M}} * 100' % (series_ids[3],),
                       max_points=10, df='2001-01-01', dt='2015-01-01',
                       include_job='y', include_timestamp='y')['series'][0]

        _pts_to_iso(ser['points'], [0, 2])
        self.assertTrue(ser['points'][0][2].startswith(datetime.today().isoformat()[:10]))

        # should be the same with query
        resp = self.get('/series', query='={{sid=%s}} * G.a' % (series_ids[3],),
                        date_format='iso', max_points=10, df='2001-01-01',
                        dt='2015-01-01', include_job='y', per_page=2, g_expression='G.a=100')

        # and check root response btw
        self.assertEquals(
            {k: v for k, v in six.iteritems(resp) if k not in ['series', 'resolved_df_milli', 'resolved_dt_milli']},
            {u'points_desc': False, u'total': 1, u'success': True, u'all_queries': [u'sid=sjvirt\\tests\\3']}
        )

        ser = resp['series'][0]

        self.assertLessEqual(ser['points'][0][2], self.job_id)

        ser = self.get('/series', query='={{sid=%s}} * 100' % (series_ids[3],),
                        max_points=10, df='2001-01-01', dt='2015-01-01',
                       include_job='y', include_timestamp='y')['series'][0]
        _pts_to_iso(ser['points'], [0, 3])

        self.assertTrue(ser['points'][0][3].startswith(datetime.today().isoformat()[:10]))

        # and with POST

        ser = self.post('/series', data_json={
            'series_queries': ['={{sid=%s}} * 100' % (series_ids[3],),]
        }, max_points=10, df='2001-01-01', dt='2015-01-01',
                       include_job='y', include_timestamp='y')['series'][0]

        _pts_to_iso(ser['points'], [0, 3])
        self.assertTrue(ser['points'][0][3].startswith(datetime.today().isoformat()[:10]))

        ser = self.post('/series', data_json={
            'series_queries': ['={{sid=%s}} * G.a' % (series_ids[3],), ]
        }, date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01',
                        g_expression='G.a=100',)['series'][0]

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 100.0],
                                         [u'2001-02-01T00:00:00', 200.0],
                                         [u'2001-03-01T00:00:00', 300.0],
                                         [u'2002-02-01T00:00:00', 300.0],
                                         [u'2003-05-01T00:00:00', 500.0],
                                         [u'2003-12-31T00:00:00', 600.0],
                                         [u'2004-01-31T00:00:00', 700.0]])

        ser = self.post('/series', data_json={
            'series_queries': ['={{sid=%s}} * G.a' % (series_ids[3],), ],
            'g_expression': 'G.a=100'
        }, date_format='iso', max_points=10, df='2001-01-01', dt='2015-01-01',)['series'][0]

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 100.0],
                                         [u'2001-02-01T00:00:00', 200.0],
                                         [u'2001-03-01T00:00:00', 300.0],
                                         [u'2002-02-01T00:00:00', 300.0],
                                         [u'2003-05-01T00:00:00', 500.0],
                                         [u'2003-12-31T00:00:00', 600.0],
                                         [u'2004-01-31T00:00:00', 700.0]])

        # test the same with fields
        ser = self.get('/series', series_id='={{sid=%s}} * 100' % (series_ids[0],),
                       date_format='iso', fields='aspect', max_points=10, df='2001-01-01', dt='2015-01-01')['series'][0]

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                         [u'2001-02-01T00:00:00', -190.0],
                                         [u'2001-03-01T00:00:00', 0.0],
                                         [u'2002-02-01T00:00:00', -100022.0],
                                         [u'2003-05-01T00:00:00', 90900.0],
                                         [u'2003-12-31T00:00:00', 10300.00009],
                                         [u'2004-01-31T00:00:00', 1e-13]])

        self.assertEqual(ser['fields'], {'aspect': 'Oil'})

        # check stats field
        ser = self.get('/series', series_id='={{sid=%s}} * 100' % (series_ids[0],),
                       date_format='iso', fields='aspect,pts.max', max_points=10, df='2001-01-01', dt='2015-01-01')['series'][0]

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                         [u'2001-02-01T00:00:00', -190.0],
                                         [u'2001-03-01T00:00:00', 0.0],
                                         [u'2002-02-01T00:00:00', -100022.0],
                                         [u'2003-05-01T00:00:00', 90900.0],
                                         [u'2003-12-31T00:00:00', 10300.00009],
                                         [u'2004-01-31T00:00:00', 1e-13]])

        self.assertEqual(ser['fields'], {u'pts.max': 90900.0, u'aspect': u'Oil'})

        # same but query
        ser = self.get('/series', query='={{sid=%s}} * G.a' % (series_ids[0],),
                       date_format='iso', fields='pts.max', g_expression='G.a=100',
                       max_points=10, df='2001-01-01', dt='2015-01-01')['series'][0]

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                         [u'2001-02-01T00:00:00', -190.0],
                                         [u'2001-03-01T00:00:00', 0.0],
                                         [u'2002-02-01T00:00:00', -100022.0],
                                         [u'2003-05-01T00:00:00', 90900.0],
                                         [u'2003-12-31T00:00:00', 10300.00009],
                                         [u'2004-01-31T00:00:00', 1e-13]])

        self.assertEqual(ser['fields'], {u'pts.max': 90900.0,})

        # and now fields only
        ser = self.get('/series', series_id='={{sid=%s}} * 100' % (series_ids[0],),
                       date_format='iso', fields='aspect',)['series'][0]
        self.assertNotIn('points', ser)
        self.assertEqual(ser['fields'], {'aspect': 'Oil'})

        # mix virt and shooju series in query
        resp = self.get('/series',
                        query=r"=sjdf('sid:sjvirt\\tests and (l3={} or l3={})') * 100".format(
                            series_ids[0].split('\\')[-1], series_ids[-3].split('\\')[-1]),
                        date_format='iso', max_points=10, operators='@localize:America/Chicago',
                        df='2001-01-01', dt='2016-01-01', sort='l3 asc')['series']

        self.assertEqual(resp, [
            {u'series_id': u'sjvirt\\tests\\0@localize:America/Chicago',
             u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']},
             u'points': [[u'2001-01-31T18:00:00', -190.0],
                         [u'2001-02-28T18:00:00', 0.0],
                         [u'2002-01-31T18:00:00', -100022.0],
                         [u'2003-04-30T19:00:00', 90900.0],
                         [u'2003-12-30T18:00:00', 10300.00009],
                         [u'2004-01-30T18:00:00', 1e-13]],
             u'tz': {u'timezone': u'America/Chicago'}},
            {u'series_id': u'sjvirt\\tests\\3@localize:America/Chicago',
             u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\3']},
             u'points': [[u'2001-01-31T18:00:00', 200.0],
                         [u'2001-02-28T18:00:00', 300.0],
                         [u'2002-01-31T18:00:00', 300.0],
                         [u'2003-04-30T19:00:00', 500.0],
                         [u'2003-12-30T18:00:00', 600.0],
                         [u'2004-01-30T18:00:00', 700.0]],
             u'tz': {u'timezone': u'America/Chicago'}}])

        resp = self.get('/series',
                        query=r"=sjdf('sid:sjvirt\\tests and (l3={} or l3={})') * 100".format(
                            series_ids[0].split('\\')[-1], series_ids[-3].split('\\')[-1]),
                        date_format='iso', max_points=10, operators='@localize:America/Chicago@S:1y',
                        df='2001-01-01', dt='2016-01-01', sort='l3 asc')['series']

        self.assertEqual(resp, [
            {u'series_id': u'sjvirt\\tests\\0@localize:America/Chicago@S:1y', u'xpr': {
                u'series_queries': [u'sid=sjvirt\\tests\\0'],
            },
             u'points': [[u'2001-01-01T00:00:00', -190.0],
                         [u'2002-01-01T00:00:00', -100022.0],
                         [u'2003-01-01T00:00:00', 101200.00009],
                         [u'2004-01-01T00:00:00', 1e-13]]},

            {u'series_id': u'sjvirt\\tests\\3@localize:America/Chicago@S:1y',
             u'xpr': {
                u'series_queries': [u'sid=sjvirt\\tests\\3'],
             },
             u'points': [[u'2001-01-01T00:00:00', 500.0],
                         [u'2002-01-01T00:00:00', 300.0],
                         [u'2003-01-01T00:00:00', 1100.0],
                         [u'2004-01-01T00:00:00', 700.0]]}
        ])

        resp = self.get('/series',
                        query=r"=sjdf('sid:sjvirt\\tests and (l3={} or l3={})') * 100".format(
                            series_ids[0].split('\\')[-1], series_ids[-3].split('\\')[-1]),
                        date_format='iso', max_points=10,
                        df='2001-01-01', dt='2016-01-01', sort='l3 asc')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                             [u'2001-02-01T00:00:00', -190.0],
                                             [u'2001-03-01T00:00:00', 0.0],
                                             [u'2002-02-01T00:00:00', -100022.0],
                                             [u'2003-05-01T00:00:00', 90900.0],
                                             [u'2003-12-31T00:00:00', 10300.00009],
                                             [u'2004-01-31T00:00:00', 1e-13]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 100.0],
                                             [u'2001-02-01T00:00:00', 200.0],
                                             [u'2001-03-01T00:00:00', 300.0],
                                             [u'2002-02-01T00:00:00', 300.0],
                                             [u'2003-05-01T00:00:00', 500.0],
                                             [u'2003-12-31T00:00:00', 600.0],
                                             [u'2004-01-31T00:00:00', 700.0]])

        self.assertNotIn('fields', resp[0])
        self.assertNotIn('fields', resp[1])

        # the same with fields
        resp = self.get('/series',
                        query=r"=sjdf('sid:sjvirt\\tests and (l3={} or l3={})') * 100".format(
                            series_ids[0].split('\\')[-1], series_ids[-3].split('\\')[-1]),
                        date_format='iso', max_points=10, fields='*',
                        df='2001-01-01', dt='2016-01-01', sort='l3 asc')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                             [u'2001-02-01T00:00:00', -190.0],
                                             [u'2001-03-01T00:00:00', 0.0],
                                             [u'2002-02-01T00:00:00', -100022.0],
                                             [u'2003-05-01T00:00:00', 90900.0],
                                             [u'2003-12-31T00:00:00', 10300.00009],
                                             [u'2004-01-31T00:00:00', 1e-13]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 100.0],
                                             [u'2001-02-01T00:00:00', 200.0],
                                             [u'2001-03-01T00:00:00', 300.0],
                                             [u'2002-02-01T00:00:00', 300.0],
                                             [u'2003-05-01T00:00:00', 500.0],
                                             [u'2003-12-31T00:00:00', 600.0],
                                             [u'2004-01-31T00:00:00', 700.0]])

        self.assertEqual(resp[0]['fields'], {u'timezone': u'America/Chicago', u'aspect': u'Oil', u'vrt': {u'source': u'test'}})
        self.assertEqual(resp[1]['fields'], {u'aspect': u'Sun'})

        # try inline operators
        resp = self.get('/series',
                        query=r"=sjdf('sid:sjvirt\\tests and (l3={} or l3={})') * 100@S:y".format(
                            series_ids[0].split('\\')[-1], series_ids[-3].split('\\')[-1]),
                        date_format='iso', max_points=10, fields='*',
                        df='2001-01-01', dt='2016-01-01', sort='l3 asc')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 9819.999999999998],
                                             [u'2002-01-01T00:00:00', -100022.0],
                                             [u'2003-01-01T00:00:00', 101200.00009],
                                             [u'2004-01-01T00:00:00', 1e-13]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 600.0],
                                             [u'2002-01-01T00:00:00', 300.0],
                                             [u'2003-01-01T00:00:00', 1100.0],
                                             [u'2004-01-01T00:00:00', 700.0]])

        # and the same with fields only
        resp = self.get('/series',
                        query=r"=sjdf('sid:sjvirt\\tests and (l3={} or l3={})') * 100".format(
                            series_ids[0].split('\\')[-1], series_ids[-3].split('\\')[-1]),
                        date_format='iso', fields='aspect', sort='l3 asc')['series']
        self.assertEqual(resp[0]['fields'], {u'aspect': u'Oil'})
        self.assertEqual(resp[1]['fields'], {u'aspect': u'Sun'})

        # test multi get
        resp = self.post('/series', data_json={
            'series_ids': ["={{sid=%s}} * 100" % (series_ids[0],), '=1*1']
        }, max_points=1, df=0, dt=1451606400000, date_format='milli')['series']

        self.assertEqual(resp[0]['points'], [[978307200000, 10010.0]])
        self.assertEqual(resp[1]['points'][0], [0, 1.0])

        # the same with fields
        resp = self.post('/series', data_json={
            'series_ids': ["={{sid=%s}} * 100" % (series_ids[0],), '=1*1']
        }, max_points=1, df=0, dt=1451606400000, date_format='milli', fields='aspect')['series']

        self.assertEqual(resp[0]['points'], [[978307200000, 10010.0]])
        self.assertEqual(resp[1]['points'][0], [0, 1.0])
        self.assertEqual(resp[0]['fields'], {u'aspect': u'Oil'})
        self.assertNotIn('fields', resp[1])

        # with stats field
        resp = self.post('/series', data_json={
            'series_ids': ["={{sid=%s}} * G.a" % (series_ids[0],), '=1*1']
        }, max_points=1, df=0, dt=1451606400000, g_expression='G.a=100',
                         date_format='milli', fields='aspect,pts.min')['series']

        self.assertEqual(resp[0]['points'], [[978307200000, 10010.0]])
        self.assertEqual(resp[1]['points'][0], [0, 1.0])
        self.assertEqual(resp[0]['fields'], {u'aspect': u'Oil', u'pts.min': 10010.0})

        resp = self.post('/series', data_json={
            'series_ids': ["={{sid=%s}} * G.a" % (series_ids[0],), '=1*1'],
            'g_expression': 'G.a=100'
        }, max_points=1, df=0, dt=1451606400000,
                         date_format='milli', fields='aspect,pts.min')['series']

        self.assertEqual(resp[0]['points'], [[978307200000, 10010.0]])
        self.assertEqual(resp[1]['points'][0], [0, 1.0])
        self.assertEqual(resp[0]['fields'], {u'aspect': u'Oil', u'pts.min': 10010.0})

        # the same without points
        resp = self.post('/series', data_json={
            'series_ids': ["={{sid=%s}} * 100" % (series_ids[0],), '=1*1']
        }, date_format='milli', fields='*')['series']

        self.assertNotIn('points', resp[0])
        self.assertEqual(resp[0]['fields'], {u'aspect': u'Oil',
                                             u'timezone': u'America/Chicago',
                                             u'vrt': {u'source': u'test'}})

        resp = self.post('/series', data_json={
            'series': [
                dict(series_id='={{sid=%s}}*-1' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='={{sid=%s}}*0' % (series_ids[-3],), max_points=1, df='2001-01-01', dt='2016-01-01'),
                dict(series_id='={{sid=%s}}/100' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2001-01-01'),
                dict(series_id='={{sid=%s}}/100' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2001-01-01', operators='@YA'),
                dict(series_id='={{sid=%s}}/100' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2001-01-01', operators='@id:1'),
                dict(series_id='={{sid=%s}}/100' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2001-01-01', operators='@id:1@YS'),
                dict(series_id='={{sid=%s}}/100@A:1y' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2001-01-01'),
                dict(series_id='={{sid=%s}}/100' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2001-01-01', operators='@id:1'),
                dict(series_id='={{sid=%s}}/100' % (series_ids[0],), max_points=10, df='2001-01-01', dt='2001-01-01', operators='@id:1@S:1y')
            ]
        }, date_format='iso')['series']

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', -100.1],
                                             [u'2001-02-01T00:00:00', 1.9],
                                             [u'2001-03-01T00:00:00', -0.0],
                                             [u'2002-02-01T00:00:00', 1000.22],
                                             [u'2003-05-01T00:00:00', -909.0],
                                             [u'2003-12-31T00:00:00', -103.0000009],
                                             [u'2004-01-31T00:00:00', -1e-15]])

        self.assertEqual(resp[1]['points'], [[u'2001-01-01T00:00:00', 0.0]])

        self.assertEqual(resp[2]['points'], [[u'2001-01-01T00:00:00', 1.001]])
        self.assertEqual(resp[3]['points'], [[u'2001-01-01T00:00:00', 0.32733333333333325]])
        self.assertEqual(resp[4]['points'], [[u'2001-01-01T00:00:00', 1.001]])
        self.assertEqual(resp[5]['points'], [[u'2001-01-01T00:00:00', 0.9819999999999999]])
        self.assertEqual(resp[6]['points'], [[u'2001-01-01T00:00:00', 0.32733333333333325]])
        self.assertEqual(resp[7]['points'], [[u'2001-01-01T00:00:00', 1.001]])
        self.assertEqual(resp[8]['points'], [[u'2001-01-01T00:00:00', 0.9819999999999999]])

    def test_resolved_milli(self):
        ser = self.get('/series', series_id=series_ids[0],
                       date_format='iso', max_points=10, df='-10y', dt='-1d')
        self.assertIn('resolved_df_milli', ser)
        self.assertIn('resolved_dt_milli', ser)

        ser = self.get('/series', series_id='={{sid=%s}}' % series_ids[0],
                       date_format='iso', max_points=10, df='-10y', dt='-1d')
        self.assertIn('resolved_df_milli', ser)
        self.assertIn('resolved_dt_milli', ser)

        ser = self.get('/series', query='sid=%s' % series_ids[0],
                       date_format='iso', max_points=10, df='-10y', dt='-1d')
        self.assertIn('resolved_df_milli', ser)
        self.assertIn('resolved_dt_milli', ser)

        ser = self.get('/series', query='={{sid=%s}}' % series_ids[0],
                       date_format='iso', max_points=10, df='-10y', dt='-1d')
        self.assertIn('resolved_df_milli', ser)
        self.assertIn('resolved_dt_milli', ser)

        ser = self.post('/series', data_json={
            'series_queries': [series_ids[0]]
        }, date_format='iso', max_points=10, df='-10y', dt='-1d')
        self.assertIn('resolved_df_milli', ser)
        self.assertIn('resolved_dt_milli', ser)

        ser = self.post('/series', data_json={
            'series_queries': ['={{sid=%s}}' % (series_ids[0])]
        }, date_format='iso', max_points=10, df='-10y', dt='-1d')
        self.assertIn('resolved_df_milli', ser)
        self.assertIn('resolved_dt_milli', ser)

    def test_cached_operator(self):
        drop_series_expression_cache()

        original_post = requests.post

        with patch('sjtoolkit.expressions.client.requests.post') as post_mock:
            post_mock.side_effect = original_post

            ser = self.get('/series', series_id='={{sid=%s}} * 100@cached:12m' % (series_ids[0],),
                           date_format='iso', fields='aspect', max_points=10, df='2001-01-01', dt='2015-01-01')['series'][0]

            self.assertEqual(post_mock.call_count, 1)

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                         [u'2001-02-01T00:00:00', -190.0],
                                         [u'2001-03-01T00:00:00', 0.0],
                                         [u'2002-02-01T00:00:00', -100022.0],
                                         [u'2003-05-01T00:00:00', 90900.0],
                                         [u'2003-12-31T00:00:00', 10300.00009],
                                         [u'2004-01-31T00:00:00', 1e-13]])

        self.assertEqual(ser['fields'], {'aspect': 'Oil'})

        # time range found in cache
        with patch('sjtoolkit.expressions.client.requests.post') as post_mock:
            post_mock.side_effect = original_post

            ser = self.get('/series', series_id='={{sid=%s}} * 100@cached:12m' % (series_ids[0],),
                           date_format='iso', fields='aspect', max_points=10, df='2001-01-01', dt='2015-01-01')['series'][0]

            self.assertEqual(post_mock.call_count, 0)

        self.assertEqual(ser['points'], [[u'2001-01-01T00:00:00', 10010.0],
                                         [u'2001-02-01T00:00:00', -190.0],
                                         [u'2001-03-01T00:00:00', 0.0],
                                         [u'2002-02-01T00:00:00', -100022.0],
                                         [u'2003-05-01T00:00:00', 90900.0],
                                         [u'2003-12-31T00:00:00', 10300.00009],
                                         [u'2004-01-31T00:00:00', 1e-13]])

        self.assertEqual(ser['fields'], {'aspect': 'Oil'})

        drop_series_expression_cache()

    def test_cart(self):
        cart_path = '/users/{}/cart'.format(config.TEST_USER)

        def make_sid(postfix):
            return 'users\\{}\\test_cart\\a\\{}'.format(config.TEST_USER, postfix)

        def make_alt_sid(postfix):
            return 'users\\{}\\test_cart\\b\\{}'.format(config.TEST_USER, postfix)

        series = [
            {
                'series_id': make_sid(1),
                'fields': {'description': 'Desc One'}
            },
            {
                'series_id': make_sid(2),
                'fields': {'description': 'Desc Two'}
            },
            {
                'series_id': make_sid(3),
            },
            {
                'series_id': make_alt_sid(1),
                'fields': {'description': 'Desc Alt'}
            },
        ]
        r = self.post('/series/write', job_id=self.job_id, data_json={'series': series}, date_format='iso')

        # check queries
        self.sj.raw.post('/series/refresh')

        self.delete(cart_path)

        res = self.get(cart_path)
        self.assertEqual(res['series'], [])
        res = self.post(cart_path, data_json={'series_queries': [make_sid(1), make_sid(2), make_sid(3), make_sid(4)]})
        six.assertCountEqual(self, res['series'], [
            {'query': make_sid(1), 'description': 'Desc One', 'new': True},
            {'query': make_sid(2), 'description': 'Desc Two', 'new': True},
            {'query': make_sid(3), 'description': make_sid(3), 'new': True},
            {'query': make_sid(4), 'error': 'series not found'},
        ])
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': make_sid(1), 'description': 'Desc One'},
            {'query': make_sid(2), 'description': 'Desc Two'},
            {'query': make_sid(3), 'description': make_sid(3)},
        ])
        self.delete(cart_path, data_json={'series_queries': [make_sid(1)]})
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': make_sid(2), 'description': 'Desc Two'},
            {'query': make_sid(3), 'description': make_sid(3)},
        ])
        res = self.post(cart_path, data_json={'series_queries': [make_sid(5)]})
        six.assertCountEqual(self, res['series'], [
            {'query': make_sid(5), 'error': 'series not found'},
        ])
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': make_sid(2), 'description': 'Desc Two'},
            {'query': make_sid(3), 'description': make_sid(3)},
        ])
        res = self.post(cart_path, data_json={'series_queries': [make_sid(2)]})
        six.assertCountEqual(self, res['series'], [
            {'query': make_sid(2), 'description': 'Desc Two', 'new': False},
        ])
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': make_sid(2), 'description': 'Desc Two'},
            {'query': make_sid(3), 'description': make_sid(3)},
        ])
        self.delete(cart_path)
        res = self.get(cart_path)
        self.assertEqual(res['series'], [])

        res = self.post(cart_path, data_json={'query': make_sid('').rstrip('\\')})
        six.assertCountEqual(self, res['series'], [
            {'query': 'sid={}'.format(make_sid(1)), 'description': 'Desc One', 'new': True},
            {'query': 'sid={}'.format(make_sid(2)), 'description': 'Desc Two', 'new': True},
            {'query': 'sid={}'.format(make_sid(3)), 'description': 'sid={}'.format(make_sid(3)), 'new': True},
        ])
        # field based query with operator
        res = self.post(cart_path, data_json={'query': "{}@localize".format(make_sid('').rstrip('\\'))})
        six.assertCountEqual(self, res['series'], [
            {'query': 'sid={}@localize'.format(make_sid(1)), 'description': 'Desc One', 'new': True},
            {'query': 'sid={}@localize'.format(make_sid(2)), 'description': 'Desc Two', 'new': True},
            {'query': 'sid={}@localize'.format(make_sid(3)), 'description': 'sid={}@localize'.format(make_sid(3)), 'new': True},
        ])
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': 'sid={}'.format(make_sid(1)), 'description': 'Desc One',},
            {'query': 'sid={}'.format(make_sid(2)), 'description': 'Desc Two'},
            {'query': 'sid={}'.format(make_sid(3)), 'description': 'sid={}'.format(make_sid(3))},

            {'query': 'sid={}@localize'.format(make_sid(1)), 'description': 'Desc One'},
            {'query': 'sid={}@localize'.format(make_sid(2)), 'description': 'Desc Two'},
            {'query': 'sid={}@localize'.format(make_sid(3)), 'description': 'sid={}@localize'.format(make_sid(3))},
        ])
        res = self.post(cart_path, data_json={'query': make_alt_sid('').rstrip('\\')})
        six.assertCountEqual(self, res['series'], [
            {'query': 'sid={}'.format(make_alt_sid(1)), 'description': 'Desc Alt', 'new': True},
        ])
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': 'sid={}'.format(make_alt_sid(1)), 'description': 'Desc Alt'},

            {'query': 'sid={}'.format(make_sid(1)), 'description': 'Desc One', },
            {'query': 'sid={}'.format(make_sid(2)), 'description': 'Desc Two'},
            {'query': 'sid={}'.format(make_sid(3)), 'description': 'sid={}'.format(make_sid(3))},

            {'query': 'sid={}@localize'.format(make_sid(1)), 'description': 'Desc One'},
            {'query': 'sid={}@localize'.format(make_sid(2)), 'description': 'Desc Two'},
            {'query': 'sid={}@localize'.format(make_sid(3)), 'description': 'sid={}@localize'.format(make_sid(3))},
        ])
        self.delete(cart_path)
        res = self.get(cart_path)
        self.assertEqual(res['series'], [])

        q = "={{sid:%s}}" % make_alt_sid('').rstrip('\\')
        res = self.post(cart_path, data_json={'query': q})
        self.assertEqual(res['series'], [])

        self.post('/series/write', job_id=self.job_id, date_format='iso', data_json={
            'series': [dict(series_id=s['series_id'], type='DELETE') for s in series]
        })

    def test_cart_expressoins(self):
        cart_path = '/users/{}/cart'.format(config.TEST_USER)

        def make_sid(postfix):
            return 'users\\{}\\test_cart\\a\\{}'.format(config.TEST_USER, postfix)

        def make_alt_sid(postfix):
            return 'users\\{}\\test_cart\\b\\{}'.format(config.TEST_USER, postfix)

        def make_xpr_sid(postfix):
            return '={{users\\%s\\test_cart\\a\\%s}}' % (config.TEST_USER, postfix)

        def make_alt_xpr_sid(postfix):
            return '={{users\\%s\\test_cart\\b\\%s}}' % (config.TEST_USER, postfix)

        series = [
            {
                'series_id': make_sid(1),
                'fields': {'description': 'Desc One'}
            },
            {
                'series_id': make_sid(2),
                'fields': {'description': 'Desc Two'}
            },
            {
                'series_id': make_sid(3),
            },
            {
                'series_id': make_alt_sid(1),
                'fields': {'description': 'Desc Alt'}
            },
        ]
        r = self.post('/series/write', job_id=self.job_id, data_json={'series': series}, date_format='iso')

        # check queries
        self.sj.raw.post('/series/refresh')

        self.delete(cart_path)
        res = self.get(cart_path)
        self.assertEqual(res['series'], [])

        self.delete(cart_path)
        res = self.get(cart_path)
        self.assertEqual(res['series'], [])
        res = self.post(cart_path, data_json={'series_queries': [make_xpr_sid(1), make_xpr_sid(2), make_xpr_sid(3), make_xpr_sid(4)]})
        six.assertCountEqual(self, res['series'], [
            {'query': make_xpr_sid(1), 'description': 'Desc One', 'new': True},
            {'query': make_xpr_sid(2), 'description': 'Desc Two', 'new': True},
            {'query': make_xpr_sid(3), 'description': make_xpr_sid(3), 'new': True},
            {'query': make_xpr_sid(4), 'error': 'series not found'},
        ])
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': make_xpr_sid(1), 'description': 'Desc One'},
            {'query': make_xpr_sid(2), 'description': 'Desc Two'},
            {'query': make_xpr_sid(3), 'description': make_xpr_sid(3)},
        ])
        res = self.post(cart_path, data_json={'series_queries': [make_xpr_sid(5)]})
        six.assertCountEqual(self, res['series'], [
            {'query': make_xpr_sid(5), 'error': 'series not found'},
        ])
        res = self.post(cart_path, data_json={'series_queries': [make_xpr_sid(2)]})
        six.assertCountEqual(self, res['series'], [
            {'query': make_xpr_sid(2), 'description': 'Desc Two', 'new': False},
        ])
        res = self.get(cart_path)
        six.assertCountEqual(self, res['series'], [
            {'query': make_xpr_sid(1), 'description': 'Desc One'},
            {'query': make_xpr_sid(2), 'description': 'Desc Two'},
            {'query': make_xpr_sid(3), 'description': make_xpr_sid(3)},
        ])
        self.delete(cart_path)
        res = self.get(cart_path)
        self.assertEqual(res['series'], [])

    def test_facets_for_expressions(self):
        resp = self.post('/series', data_json={
            'series_queries': ['sid="%s"' % series_ids[5],
                               '={{sid="%s"}}' % series_ids[0],
                               'sid="%s"' % series_ids[3]]
        }, facets='l3')

        self.assertEqual(resp['facets'], {u'l3': {
            u'total': 3,
            u'terms': [{u'count': 1, u'term': u'0'},
                       {u'count': 1, u'term': u'3'},
                       {u'count': 1, u'term': u'5'}],
            u'other': 0, u'missing': 0}})

        # test queries
        resp = self.get('/series',
                        query='={{sid="%s"}} * {{sid="%s"}}' % (series_ids[0], series_ids[3]),
                        facets='l3',
                        max_facet_values=1)

        self.assertEqual(
            {u'l3': {u'total': 2,
                     u'terms': [{u'count': 1, u'term': u'0'}],
                     u'other': 1,
                     u'missing': 0}},
            resp['facets'])

    def test_expression_series(self):
        resp = self.get('/series', series_id=series_ids[-1], max_points=10, date_format='iso')['series'][0]
        self.assertEqual(resp['points'], [[u'2001-01-01T00:00:00', -100.1],
                                          [u'2001-02-01T00:00:00', 1.9],
                                          [u'2001-03-01T00:00:00', -0.0],
                                          [u'2002-02-01T00:00:00', 1000.22],
                                          [u'2003-05-01T00:00:00', -909.0],
                                          [u'2003-12-31T00:00:00', -103.0000009],
                                          [u'2004-01-31T00:00:00', -1e-15]])

        resp = self.post('/series', data_json={
            'series_ids': [series_ids[-1], series_ids[1], series_ids[0]]
        }, max_points=2, fields='*')['series']

        self.assertEqual(resp[0], {u'fields': {u'aspect': u'Water',
                                               u'timezone': u'America/Chicago',
                                               u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a',
                                                        u'g': u'G.a = -1'}},
                                   u'points': [[978307200000, -100.1], [980985600000, 1.9]],
                                   u'series_id': u'sjvirt\\tests\\5',
                                   u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        self.assertEqual(resp[1],
                         {u'series_id': u'sjvirt\\tests\\1', u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
                          u'points': [[1262307600000, 20.1], [1262311200000, 30.1]]})

        self.assertEqual(resp[-1], {u'series_id': u'sjvirt\\tests\\0',
                                    u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Oil',
                                                u'vrt': {u'source': u'test'}},
                                    u'points': [[978307200000, 100.1], [980985600000, -1.9]]})

        # the same but without fields
        resp = self.post('/series', data_json={
            'series_ids': [series_ids[-1], series_ids[1], series_ids[0]]
        }, max_points=2, )['series']

        self.assertEqual(resp[0], {u'points': [[978307200000, -100.1], [980985600000, 1.9]],
                                   u'series_id': u'sjvirt\\tests\\5',
                                   u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        self.assertEqual(resp[1],
                         {u'series_id': u'sjvirt\\tests\\1',
                          u'points': [[1262307600000, 20.1], [1262311200000, 30.1]]})

        self.assertEqual(resp[-1], {u'series_id': u'sjvirt\\tests\\0',
                                    u'points': [[978307200000, 100.1], [980985600000, -1.9]]})

        # and again the same but fields only
        resp = self.post('/series', data_json={
            'series_ids': [series_ids[-1], series_ids[1], series_ids[0]]
        }, fields='aspect, ', )['series']

        self.assertEqual(resp, [{u'series_id': u'sjvirt\\tests\\5', u'fields': {u'aspect': u'Water'}},
                                {u'series_id': u'sjvirt\\tests\\1', u'fields': {u'aspect': u'Gas'}},
                                {u'series_id': u'sjvirt\\tests\\0', u'fields': {u'aspect': u'Oil'}}])

        # the same but series queries
        resp = self.post('/series', data_json={
            'series_queries': ['sid="%s"' % series_ids[-1], 'sid="%s"' % series_ids[1], 'sid="%s"' % series_ids[0]]
        }, max_points=2, fields='*')['series']

        self.assertEqual(resp, [
            {u'series_id': u'$sid="sjvirt\\tests\\5"',
             u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']},
             u'points': [[978307200000, -100.1], [980985600000, 1.9]],
             u'fields': {u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a', u'g': u'G.a = -1'}, u'aspect': u'Water', u'timezone': u'America/Chicago',}},

            {u'series_id': u'$sid="sjvirt\\tests\\1"', u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
             u'points': [[1262307600000, 20.1], [1262311200000, 30.1]]},

            {u'series_id': u'$sid="sjvirt\\tests\\0"', u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Oil', u'vrt': {u'source': u'test'}},
             u'points': [[978307200000, 100.1], [980985600000, -1.9]]}])

        # let's also try some queries
        resp = self.get('/series', query='sid:sjvirt\\tests', sort='l3 asc')['series']  # nothing at all

        self.assertEqual(resp, [{u'series_id': u'sjvirt\\tests\\0'},
                                {u'series_id': u'sjvirt\\tests\\1'},
                                {u'series_id': u'sjvirt\\tests\\2'},
                                {u'series_id': u'sjvirt\\tests\\3'},
                                {u'series_id': u'sjvirt\\tests\\4'},
                                {u'series_id': u'sjvirt\\tests\\5'}])

        # fields only
        resp = self.get('/series', query='sid:sjvirt\\tests', sort='l3 asc', fields='aspect')['series']
        self.assertEqual(resp, [{u'series_id': u'sjvirt\\tests\\0', u'fields': {u'aspect': u'Oil'}},
                                {u'series_id': u'sjvirt\\tests\\1', u'fields': {u'aspect': u'Gas'}},
                                {u'series_id': u'sjvirt\\tests\\2', u'fields': {u'aspect': u'Wind'}},
                                {u'series_id': u'sjvirt\\tests\\3', u'fields': {u'aspect': u'Sun'}},
                                {u'series_id': u'sjvirt\\tests\\4', u'fields': {u'aspect': u'Water'}},
                                {u'series_id': u'sjvirt\\tests\\5', u'fields': {u'aspect': u'Water'}}])

        # fields and points
        resp = self.get('/series', query='sid:sjvirt\\tests', sort='l3 asc', fields='aspect, timezone', max_points=1, date_format='iso')['series']
        self.assertEqual(resp,
                         [
                             {u'series_id': u'sjvirt\\tests\\0', u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Oil'}, u'points': [[u'2001-01-01T00:00:00', 100.1]]},
                             {u'series_id': u'sjvirt\\tests\\1', u'fields': {u'aspect': u'Gas'}, u'points': [[u'2010-01-01T01:00:00', 20.1]]},
                             {u'series_id': u'sjvirt\\tests\\2', u'fields': {u'aspect': u'Wind'}, u'points': [[u'2001-01-01T00:00:00', 1000.0]]},
                             {u'series_id': u'sjvirt\\tests\\3', u'fields': {u'aspect': u'Sun'}, u'points': [[u'2001-01-01T00:00:00', 1.0]]},
                             {u'series_id': u'sjvirt\\tests\\4', u'fields': {u'aspect': u'Water'}},
                             {u'series_id': u'sjvirt\\tests\\5', u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']},
                              u'points': [[u'2001-01-01T00:00:00', -100.1]],
                              u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Water'}}])

        # localize and other operators
        resp = self.post('/series', data_json={
            'series_ids': [series_ids[-1], series_ids[1], '={{sid=%s}}' % series_ids[0], series_ids[-3]]
        }, max_points=2, fields='*', operators='@localize', date_format='iso')['series']

        self.assertEqual(resp,
                         [
                             {u'series_id': u'sjvirt\\tests\\5@localize',
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']},
                              u'points': [[u'2000-12-31T18:00:00', -100.1],
                                          [u'2001-01-31T18:00:00', 1.9]],
                              u'tz': {u'timezone': u'America/Chicago'},

                              u'fields': {u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a', u'g': u'G.a = -1'}, u'aspect': u'Water', u'timezone': u'America/Chicago',}},

                             {u'series_id': u'sjvirt\\tests\\1@localize',
                              u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
                              u'points': [[u'2010-01-01T01:00:00', 20.1],
                                          [u'2010-01-01T02:00:00', 30.1]]},

                             {u'series_id': u'={{sid=sjvirt\\tests\\0}}',
                              u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Oil', u'vrt': {u'source': u'test'}},
                              u'points': [[u'2000-12-31T18:00:00', 100.1],
                                          [u'2001-01-31T18:00:00', -1.9]],
                              u'tz': {u'timezone': u'America/Chicago'},
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}},

                             {u'series_id': u'sjvirt\\tests\\3@localize',
                              u'fields': {u'aspect': u'Sun'},
                              u'points': [[u'2001-01-01T00:00:00', 1.0],
                                          [u'2001-02-01T00:00:00', 2.0]]}])

        # this is identical
        resp = self.post('/series', data_json={
            'series_ids': [series_ids[-1] + '@localize', series_ids[1] + '@localize', '={{sid=%s}}@localize' % series_ids[0], series_ids[-3] + '@localize']
        }, max_points=2, fields='*', date_format='iso')['series']

        self.assertEqual(resp,
                         [
                             {u'series_id': u'sjvirt\\tests\\5@localize',
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']},
                              u'points': [[u'2000-12-31T18:00:00', -100.1],
                                          [u'2001-01-31T18:00:00', 1.9]],
                              u'tz': {u'timezone': u'America/Chicago'},
                              u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Water',
                                          u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a', u'g': u'G.a = -1'}}},

                             {u'series_id': u'sjvirt\\tests\\1@localize',
                              u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
                              u'points': [[u'2010-01-01T01:00:00', 20.1],
                                          [u'2010-01-01T02:00:00', 30.1]]},

                             {u'series_id': u'={{sid=sjvirt\\tests\\0}}@localize',
                              u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Oil',
                                          u'vrt': {u'source': u'test'}},
                              u'points': [[u'2000-12-31T18:00:00', 100.1],
                                          [u'2001-01-31T18:00:00', -1.9]],
                              u'tz': {u'timezone': u'America/Chicago'},
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}},

                             {u'series_id': u'sjvirt\\tests\\3@localize', u'fields': {u'aspect': u'Sun'},
                              u'points': [[u'2001-01-01T00:00:00', 1.0],
                                          [u'2001-02-01T00:00:00', 2.0]]}
                         ])

        # test w/o localizing
        resp = self.post('/series', data_json={
            'series_ids': [series_ids[-1], series_ids[1], '={{sid=%s}}' % series_ids[0], series_ids[-3]]
        }, max_points=2, fields='*', date_format='iso')['series']

        self.assertEqual(resp,
                         [{'fields': {'aspect': 'Water',
                                      'timezone': 'America/Chicago',
                                      'xpr': {'expression': '={{sid=sjvirt\\tests\\0}} * G.a', 'g': 'G.a = -1'}},
                           'points': [['2001-01-01T00:00:00', -100.1], ['2001-02-01T00:00:00', 1.9]],
                           'series_id': 'sjvirt\\tests\\5',
                           'xpr': {'series_queries': ['sid=sjvirt\\tests\\0']}},
                          {'fields': {'aspect': 'Gas', 'vrt': {'source': 'test'}},
                           'points': [['2010-01-01T01:00:00', 20.1], ['2010-01-01T02:00:00', 30.1]],
                           'series_id': 'sjvirt\\tests\\1'},
                          {'fields': {'aspect': 'Oil',
                                      'timezone': 'America/Chicago',
                                      'vrt': {'source': 'test'}},
                           'points': [['2001-01-01T00:00:00', 100.1], ['2001-02-01T00:00:00', -1.9]],
                           'series_id': '={{sid=sjvirt\\tests\\0}}',
                           'xpr': {'series_queries': ['sid=sjvirt\\tests\\0']}},
                          {'fields': {'aspect': 'Sun'},
                           'points': [['2001-01-01T00:00:00', 1.0], ['2001-02-01T00:00:00', 2.0]],
                           'series_id': 'sjvirt\\tests\\3'}])

        resp = self.post('/series', data_json={
            'series_ids': ['%s@localize@S:d' % series_ids[-1], '%s@localize@S:y' % series_ids[1],
                           '={{sid=%s}}' % series_ids[0], '%s@localize@S:y' % series_ids[-3]]
        }, max_points=-1, fields='*', sample='1', date_format='iso')['series']

        self.assertEqual(resp,
                         [{u'fields': {u'aspect': u'Water',
                                       u'timezone': u'America/Chicago',
                                       u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a',
                                                u'g': u'G.a = -1'}},
                           'points': [['2000-12-31T00:00:00', -100.1],
                                      ['2002-01-31T00:00:00', 1000.22],
                                      ['2003-04-30T00:00:00', -909.0],
                                      ['2004-01-30T00:00:00', -1e-15]],
                           u'series_id': u'sjvirt\\tests\\5@localize@S:d',
                           u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}},
                          {u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
                           u'points': [[u'2010-01-01T00:00:00', 18.400000000000006]],
                           u'series_id': u'sjvirt\\tests\\1@localize@S:y'},
                          {u'fields': {u'aspect': u'Oil',
                                       u'timezone': u'America/Chicago',
                                       u'vrt': {u'source': u'test'}},
                           u'points': [[u'2001-01-01T00:00:00', 100.1],
                                       [u'2002-02-01T00:00:00', -1000.22],
                                       [u'2003-05-01T00:00:00', 909.0],
                                       [u'2004-01-31T00:00:00', 1e-15]],
                           u'series_id': u'={{sid=sjvirt\\tests\\0}}',
                           u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0'],}},
                          {u'fields': {u'aspect': u'Sun'},
                           u'points': [[u'2001-01-01T00:00:00', 6.0],
                                       [u'2002-01-01T00:00:00', 3.0],
                                       [u'2003-01-01T00:00:00', 11.0],
                                       [u'2004-01-01T00:00:00', 7.0]],
                           u'series_id': u'sjvirt\\tests\\3@localize@S:y'}])

        resp = self.post('/series', data_json={
            'series_ids': ['%s@localize@S:d' % series_ids[-1], '%s@localize@S:y' % series_ids[1],
                           '={{sid=%s}}' % series_ids[0], '%s@localize@S:y' % series_ids[-3]]
        }, max_points=-1, fields='*', sample='1', date_format='iso', operators='@lag:-1d')['series']

        self.assertEqual(resp,
                         [
                             {u'series_id': u'sjvirt\\tests\\5@localize@S:d@lag:-1d',
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0'],},
                              'points': [['2000-12-30T00:00:00', -100.1],
                                         ['2002-01-30T00:00:00', 1000.22],
                                         ['2003-04-29T00:00:00', -909.0],
                                         ['2004-01-29T00:00:00', -1e-15]],
                              u'fields': {u'timezone': u'America/Chicago',
                                          u'aspect': u'Water', u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a', u'g': u'G.a = -1'}}},
                             {u'series_id': u'sjvirt\\tests\\1@localize@S:y@lag:-1d',
                              u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
                              u'points': [[u'2009-12-31T00:00:00', 18.400000000000006]]},

                             {u'series_id': u'={{sid=sjvirt\\tests\\0}}',
                              u'fields': {u'timezone': u'America/Chicago',
                                          u'aspect': u'Oil', u'vrt': {u'source': u'test'}},
                              u'points': [[u'2000-12-31T00:00:00', 100.1],
                                          [u'2002-01-31T00:00:00', -1000.22],
                                          [u'2003-04-30T00:00:00', 909.0],
                                          [u'2004-01-30T00:00:00', 1e-15]],
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}},

                             {u'series_id': u'sjvirt\\tests\\3@localize@S:y@lag:-1d',
                              u'fields': {u'aspect': u'Sun'},
                              u'points': [[u'2000-12-31T00:00:00', 6.0],
                                          [u'2001-12-31T00:00:00', 3.0],
                                          [u'2002-12-31T00:00:00', 11.0],
                                          [u'2003-12-31T00:00:00', 7.0]]}])

        # identical
        resp = self.post('/series', data_json={
            'series_ids': ['%s@localize@S:d@lag:-1d' % series_ids[-1], '%s@localize@S:y@lag:-1d' % series_ids[1],
                           '={{sid=%s}}@lag:-1d' % series_ids[0], '%s@localize@S:y@lag:-1d' % series_ids[-3]]
        }, max_points=-1, fields='*', sample='1', date_format='iso')['series']

        self.assertEqual(resp,
                         [
                             {u'series_id': u'sjvirt\\tests\\5@localize@S:d@lag:-1d',
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0'],},
                              'points': [['2000-12-30T00:00:00', -100.1],
                                         ['2002-01-30T00:00:00', 1000.22],
                                         ['2003-04-29T00:00:00', -909.0],
                                         ['2004-01-29T00:00:00', -1e-15]],
                              u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Water',
                                          u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a', u'g': u'G.a = -1'}}},
                             {u'series_id': u'sjvirt\\tests\\1@localize@S:y@lag:-1d',
                              u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
                              u'points': [[u'2009-12-31T00:00:00', 18.400000000000006]]},
                             {u'series_id': u'={{sid=sjvirt\\tests\\0}}@lag:-1d',
                              u'fields': {u'timezone': u'America/Chicago', u'aspect': u'Oil',
                                          u'vrt': {u'source': u'test'}},
                              u'points': [[u'2000-12-31T00:00:00', 100.1],
                                          [u'2002-01-31T00:00:00', -1000.22],
                                          [u'2003-04-30T00:00:00', 909.0],
                                          [u'2004-01-30T00:00:00', 1e-15]],
                              u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0'],}},
                             {u'series_id': u'sjvirt\\tests\\3@localize@S:y@lag:-1d', u'fields': {u'aspect': u'Sun'},
                              u'points': [[u'2000-12-31T00:00:00', 6.0],
                                          [u'2001-12-31T00:00:00', 3.0],
                                          [u'2002-12-31T00:00:00', 11.0],
                                          [u'2003-12-31T00:00:00', 7.0]]}
                         ])

        # localize on series level
        resp = self.post('/series', data_json={
            'series_ids': ['%s@localize' % series_ids[-1], '%s@localize' % series_ids[1],
                           '={{sid=%s}}' % series_ids[0], '%s@localize' % series_ids[-3]]
        }, max_points=-1, fields='*', sample='1', date_format='iso')['series']

        self.assertEqual(resp,
                         [{u'fields': {u'aspect': u'Water',
                                       u'timezone': u'America/Chicago',
                                       u'xpr': {u'expression': u'={{sid=sjvirt\\tests\\0}} * G.a',
                                                u'g': u'G.a = -1'}},
                           'points': [['2000-12-31T18:00:00', -100.1],
                                      ['2002-01-31T18:00:00', 1000.22],
                                      ['2003-04-30T19:00:00', -909.0],
                                      ['2004-01-30T18:00:00', -1e-15]],
                           u'series_id': u'sjvirt\\tests\\5@localize',
                           u'tz': {u'timezone': u'America/Chicago'},
                           u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}},
                          {u'fields': {u'aspect': u'Gas', u'vrt': {u'source': u'test'}},
                           u'points': [[u'2010-01-01T01:00:00', 20.1],
                                       [u'2010-01-01T03:00:00', 40.1],
                                       [u'2010-01-01T05:00:00', -80.0],
                                       [u'2010-01-01T07:00:00', 10.0]],
                           u'series_id': u'sjvirt\\tests\\1@localize'},
                          {u'fields': {u'aspect': u'Oil',
                                       u'timezone': u'America/Chicago',
                                       u'vrt': {u'source': u'test'}},
                           u'points': [[u'2001-01-01T00:00:00', 100.1],
                                       [u'2002-02-01T00:00:00', -1000.22],
                                       [u'2003-05-01T00:00:00', 909.0],
                                       [u'2004-01-31T00:00:00', 1e-15]],
                           u'series_id': u'={{sid=sjvirt\\tests\\0}}',
                           u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}},
                          {u'fields': {u'aspect': u'Sun'},
                           u'points': [[u'2001-01-01T00:00:00', 1.0], [u'2004-01-31T00:00:00', 7.0]],
                           u'series_id': u'sjvirt\\tests\\3@localize'}])

        # try search
        resp = self.get('/series', query='sid:sjvirt\\tests', sort='l3 asc', fields='aspect', max_points=1, date_format='iso', operators='@localize')['series']
        self.assertEqual(resp,
                         [{u'series_id': u'sjvirt\\tests\\0@localize', u'fields': {u'aspect': u'Oil'},
                           u'points': [[u'2000-12-31T18:00:00', 100.1]], u'tz': {u'timezone': u'America/Chicago'}},

                          {u'series_id': u'sjvirt\\tests\\1@localize', u'fields': {u'aspect': u'Gas'},
                           u'points': [[u'2010-01-01T01:00:00', 20.1]]},

                          {u'series_id': u'sjvirt\\tests\\2@localize', u'fields': {u'aspect': u'Wind'},
                           u'points': [[u'2001-01-01T00:00:00', 1000.0]]},

                          {u'series_id': u'sjvirt\\tests\\3@localize', u'fields': {u'aspect': u'Sun'},
                           u'points': [[u'2001-01-01T00:00:00', 1.0]]},

                          {u'series_id': u'sjvirt\\tests\\4@localize', u'fields': {u'aspect': u'Water'}},

                          {u'series_id': u'sjvirt\\tests\\5@localize',
                           u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']},
                           u'points': [['2000-12-31T18:00:00', -100.1]], u'tz': {u'timezone': u'America/Chicago'},
                           u'fields': {u'aspect': u'Water'}}])

        resp = self.get('/series', series_id='={{sid=%s}} * 2' % series_ids[0],
                        fields='aspect', max_points=5, date_format='iso', operators='@localize')['series'][0]
        self.assertEqual(resp, {u'series_id': u'={{sid=sjvirt\\tests\\0}} * 2', u'fields': {u'aspect': u'Oil'},
                                u'points': [[u'2000-12-31T18:00:00', 200.2],
                                            [u'2001-01-31T18:00:00', -3.8],
                                            [u'2001-02-28T18:00:00', 0.0],
                                            [u'2002-01-31T18:00:00', -2000.44],
                                            [u'2003-04-30T19:00:00', 1818.0]],
                                u'tz': {u'timezone': u'America/Chicago'},
                                u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        # identical
        resp = self.get('/series', series_id='={{sid=%s}} * 2@localize' % series_ids[0],
                        fields='aspect', max_points=5, date_format='iso')['series'][0]
        self.assertEqual(resp, {u'series_id': u'={{sid=sjvirt\\tests\\0}} * 2@localize',
                                u'fields': {u'aspect': u'Oil'},
                                u'points': [[u'2000-12-31T18:00:00', 200.2],
                                            [u'2001-01-31T18:00:00', -3.8],
                                            [u'2001-02-28T18:00:00', 0.0],
                                            [u'2002-01-31T18:00:00', -2000.44],
                                            [u'2003-04-30T19:00:00', 1818.0]],
                                u'tz': {u'timezone': u'America/Chicago'},
                                u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        # no tz on more than daily aggr
        resp = self.get('/series', series_id='={{sid=%s}} * 2' % series_ids[0],
                        fields='aspect', max_points=5, date_format='iso', operators='@localize@S:M')['series'][0]

        self.assertEqual(resp, {u'series_id': u'={{sid=sjvirt\\tests\\0}} * 2', u'fields': {u'aspect': u'Oil'},
                                u'points': [[u'2000-12-01T00:00:00', 200.2],
                                            [u'2001-01-01T00:00:00', -3.8],
                                            [u'2001-02-01T00:00:00', 0.0],
                                            [u'2002-01-01T00:00:00', -2000.44],
                                            [u'2003-04-01T00:00:00', 1818.0]],
                                u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})
        # identical
        resp = self.get('/series', series_id='={{sid=%s}} * 2@S:M' % series_ids[0],
                        fields='aspect', max_points=5, date_format='iso', operators='@localize')['series'][0]

        self.assertEqual(resp, {u'series_id': u'={{sid=sjvirt\\tests\\0}} * 2@S:M', u'fields': {u'aspect': u'Oil'},
                                u'points': [[u'2000-12-01T00:00:00', 200.2],
                                            [u'2001-01-01T00:00:00', -3.8],
                                            [u'2001-02-01T00:00:00', 0.0],
                                            [u'2002-01-01T00:00:00', -2000.44],
                                            [u'2003-04-01T00:00:00', 1818.0]],
                                u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']}})

        # error when different timezones used
        resp = self.get('/series', series_id='={{sid=%s}} * {{sid=%s}}' % (series_ids[0], series_ids[0]),
                        fields='aspect', max_points=5, date_format='iso', operators='@localize@S:M')['series'][0]

    def test_to_series_queries(self):
        processor_id = 'test'
        processor_path = '/processors/{}'.format(processor_id)
        self.post(processor_path, data_json={
            'team_obj': {'expression_executors': ['_admin', 'tests']},
            'code': "import pandas\n"
            "from datetime import datetime\n\n"
            "def event_check():\n"
            "   pass\n"
            "@sjutils.expressionable(apply_operators=True)\n"
            "def foo():\n"
            "    sjutils.xpr_query('=TEST.foo()', 'myquery1')\n"
            "    sjutils.xpr_fields('=TEST.foo()', {'description': 'desc1', 'field2': 'value2', 'inner': {'field3': 'value3'}})\n"
            "    return pandas.Series({\n"
                    "    datetime(2001, 1, 1): 1,\n"
                    "    datetime(2001, 1, 2): 2,\n"
                    "    datetime(2001, 1, 3): 3\n"
                    "})",
            "description": "Processor",
            "save_description": "hey",
            "notes": "",
            "settings": {},

        })

        # Rerun expression runner to pick up new processor
        self.expression_runner.kill()
        self._run_expression_runner()

        self.sj.raw.post('/series/refresh')

        res = self.get('/series',
                       series_id='=TEST.foo()',
                       max_points=-1,
                       date_format='iso')['series'][0]
        self.assertEqual(res['xpr']['xpr_query'], 'myquery1')
        # self.assertEqual(res['fields'], {'description': 'desc1', 'field2': 'value2', 'inner': {'field3': 'value3'}})

        res = self.get('/series',
                       series_id='=TEST.foo()',
                       max_points=-1,
                       fields='*',
                       date_format='iso')['series'][0]
        self.assertEqual(res['xpr']['xpr_query'], 'myquery1')
        self.assertEqual(res['fields'], {'description': 'desc1', 'field2': 'value2', 'inner': {'field3': 'value3'}})

        r = self.get('/series/to_series_queries', query='=TEST.foo()')
        self.assertEqual(r['series'][0]['query'], 'myquery1')
        self.assertEqual(r['series'][0]['description'], u'desc1')

    def test_agg_operators(self):
        resp = self.get('/series', query='(set:aspect not set:xpr not set:cal_id) AND sid:sjvirt', operators='AGG:S:aspect',
                        max_points=100, date_format='iso', df='2001-01-01', dt='2016-01-01')['series']

        series = {s['series_id']: s for s in resp}
        self.assertEqual(set(series), set(['Water', 'Sun', 'Oil', 'Gas', 'Wind']))
        self.assertNotIn('points', series['Water'])

        self.assertEqual(series['Sun']['points'], [[u'2001-01-01T00:00:00', 1.0], [u'2001-02-01T00:00:00', 2.0],
                                                   [u'2001-03-01T00:00:00', 3.0], [u'2002-02-01T00:00:00', 3.0],
                                                   [u'2003-05-01T00:00:00', 5.0], [u'2003-12-31T00:00:00', 6.0],
                                                   [u'2004-01-31T00:00:00', 7.0]])

        # we even can actually aggregate by vrt.source
        resp = self.get('/series', query='(set:aspect not set:xpr not set:cal_id) AND sid:sjvirt', operators='AGG:S:vrt.source',
                        max_points=100, date_format='iso', df='2001-01-01', dt='2016-01-01')['series']

        self.assertEqual(len(resp), 1)

        self.assertEqual(resp[0]['points'], [[u'2001-01-01T00:00:00', 1100.1],
                                             [u'2001-02-01T00:00:00', -1.9],
                                             [u'2001-03-01T00:00:00', 0.0],
                                             [u'2002-01-01T00:00:00', 2000],
                                             [u'2002-02-01T00:00:00', -1000.22],
                                             [u'2003-01-01T00:00:00', 3000],
                                             [u'2003-05-01T00:00:00', 909],
                                             [u'2003-12-31T00:00:00', 103.0000009],
                                             [u'2004-01-01T00:00:00', 4000],
                                             [u'2004-01-31T00:00:00', 1e-15],
                                             [u'2005-01-01T00:00:00', 5000],
                                             [u'2006-01-01T00:00:00', 6000],
                                             [u'2007-01-01T00:00:00', 7000],
                                             [u'2010-01-01T01:00:00', 20.1],
                                             [u'2010-01-01T02:00:00', 30.1],
                                             [u'2010-01-01T03:00:00', 40.1],
                                             [u'2010-01-01T04:00:00', 0.1],
                                             [u'2010-01-01T05:00:00', -80],
                                             [u'2010-01-01T06:00:00', -2.0],
                                             [u'2010-01-01T07:00:00', 10]])

    def test_error_handling(self):
        # entire request should fail with invalid_parameters on following requests
        self.get('/series', expect_error='invalid_parameters')  # what do we want?
        self.get('/series', series_id='HEY', date_format='what',
                 expect_error='invalid_parameters', )  # invalid date format
        # self.get('/series', series_id=series_ids[0], date_format='milli', df='2015-01-01', max_points=10, expect_error='date_parse')  # invalid dates
        self.get('/series', series_id='{}@HEY'.format(series_ids[0]),
                 expect_error='invalid_parameters')  # invalid operators

        # same works for queries
        self.get('/series', query='', date_format='what', expect_error='invalid_parameters', )
        # self.get('/series', query='',  date_format='milli', df='2015-01-01', max_points=10, expect_error='date_parse')
        self.get('/series', query='', operators='@HEY', expect_error='invalid_parameters')

        # malformed queries fails with query_parse exception
        self.get('/series', query='(bad query', expect_error='query_parsing')

        # errors like 'not found', 'too many' and similar ones (actually these ones comes directly from Shooju)
        self.get('/series', series_id='never_find', expect_error='series_not_found')
        self.get('/series', series_id='$not sid:system', expect_error='too_many_series_returned')

        # following requests should fail on series level
        resp = self.post('/series', data_json={
            'series': [
                {'series_id': series_ids[0], 'date_format': 'what'},  # invalid date format
                {'series_id': series_ids[0], 'date_format': 'milli', 'df': 'MA', 'max_points': 10}, # invalid date format
                {'series_id': '{}@HEY'.format(series_ids[0])},  # invalid operator
                {'series_id': 'HEY'},  # series not found
                {'series_id': '$not sid:system'},  # too many series returned
                {'series_id': '=1/0', 'max_points': 1}  # zero division error in formula
            ]
        })

        self.assertEqual([s['error'] for s in resp['series']],
                         [u'invalid_parameters', u'invalid_parameters',
                          u'invalid_parameters', u'series_not_found', u'too_many_series_returned',
                          u'expression'])

    def test_charts(self):
        chart_settings = {
            "sort": "custom",
            "color_mode": "default",
            "title": "New Chart22232",
            "fields": [
                "description",
                "test_field",
                "timezone"
            ],
            "query_limit": 200,
            "series_settings_obj": [
                {
                    "label": "test 2",
                    "query": 'sid={}'.format(series_ids[2])
                },
                {
                    "label": "test 1",
                    "query": 'sid={}'.format(series_ids[1])
                }
            ],
            "value": "",
            "last_points": "y",
            "custom_sort": ['sid={}'.format(l) for l in series_ids],
            "value_type": "point",
            "chart_type": "line",
            "query": "sid:sjvirt\\tests and not set:xpr",
            "localize": "n"
        }

        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        }, facets='set')
        self.assertEqual(
            resp['facets'],
            {'set': {'missing': 0,
                     'other': 0,
                     'terms': [{'count': 5, 'term': 'aspect'},
                               {'count': 5, 'term': 'meta.fieldsbytes_num'},
                               {'count': 5, 'term': 'meta.points_count'},
                               {'count': 3, 'term': 'vrt'},
                               {'count': 3, 'term': 'vrt.source'},
                               {'count': 1, 'term': 'meta.dates_max'},
                               {'count': 1, 'term': 'meta.dates_min'},
                               {'count': 1, 'term': 'meta.freq'},
                               {'count': 1, 'term': 'meta.points_max'},
                               {'count': 1, 'term': 'meta.points_min'},
                               {'count': 1, 'term': 'timezone'}],
                     'total': 27}})

        self.assertEqual(resp['series'],
                         [{u'columns': [u''],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'',
                           u'numformat': None,
                           u'points': [[978307200000, 100.1],
                                       [980985600000, -1.9],
                                       [983404800000, 0.0],
                                       [1012521600000, -1000.22],
                                       [1051747200000, 909.0],
                                       [1072828800000, 103.0000009],
                                       [1075507200000, 1e-15]],
                           u'series_id': u'sid=sjvirt\\tests\\0'},
                          {u'columns': [u'test 1'],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'test 1',
                           u'numformat': None,
                           u'points': [[1262307600000, 20.1],
                                       [1262311200000, 30.1],
                                       [1262314800000, 40.1],
                                       [1262318400000, 0.1],
                                       [1262322000000, -80.0],
                                       [1262325600000, -2.0],
                                       [1262329200000, 10.0]],
                           u'series_id': u'sid=sjvirt\\tests\\1'},
                          {u'columns': [u'test 2'],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'test 2',
                           u'numformat': None,
                           u'points': [[978307200000, 1000.0],
                                       [1009843200000, 2000.0],
                                       [1041379200000, 3000.0],
                                       [1072915200000, 4000.0],
                                       [1104537600000, 5000.0],
                                       [1136073600000, 6000.0],
                                       [1167609600000, 7000.0]],
                           u'series_id': u'sid=sjvirt\\tests\\2'},
                          {u'columns': [u''],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'',
                           u'numformat': None,
                           u'points': [[978307200000, 1.0],
                                       [980985600000, 2.0],
                                       [983404800000, 3.0],
                                       [1012521600000, 3.0],
                                       [1051747200000, 5.0],
                                       [1072828800000, 6.0],
                                       [1075507200000, 7.0]],
                           u'series_id': u'sid=sjvirt\\tests\\3'},
                          {u'columns': [u''],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'',
                           u'numformat': None,
                           u'series_id': u'sid=sjvirt\\tests\\4'}])

        chart_settings['localize'] = 'y'

        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        })

        self.assertEqual(resp['series'],
                         [{u'columns': [u''],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'',
                           u'numformat': None,
                           u'points': [[978285600000, 100.1],
                                       [980964000000, -1.9],
                                       [983383200000, 0.0],
                                       [1012500000000, -1000.22],
                                       [1051729200000, 909.0],
                                       [1072807200000, 103.0000009],
                                       [1075485600000, 1e-15]],
                           u'series_id': u'sid=sjvirt\\tests\\0@localize',
                           u'tz': {u'timezone': u'America/Chicago'}},
                          {u'columns': [u'test 1'],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'test 1',
                           u'numformat': None,
                           u'points': [[1262307600000, 20.1],
                                       [1262311200000, 30.1],
                                       [1262314800000, 40.1],
                                       [1262318400000, 0.1],
                                       [1262322000000, -80.0],
                                       [1262325600000, -2.0],
                                       [1262329200000, 10.0]],
                           u'series_id': u'sid=sjvirt\\tests\\1@localize'},
                          {u'columns': [u'test 2'],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'test 2',
                           u'numformat': None,
                           u'points': [[978307200000, 1000.0],
                                       [1009843200000, 2000.0],
                                       [1041379200000, 3000.0],
                                       [1072915200000, 4000.0],
                                       [1104537600000, 5000.0],
                                       [1136073600000, 6000.0],
                                       [1167609600000, 7000.0]],
                           u'series_id': u'sid=sjvirt\\tests\\2@localize'},
                          {u'columns': [u''],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'',
                           u'numformat': None,
                           u'points': [[978307200000, 1.0],
                                       [980985600000, 2.0],
                                       [983404800000, 3.0],
                                       [1012521600000, 3.0],
                                       [1051747200000, 5.0],
                                       [1072828800000, 6.0],
                                       [1075507200000, 7.0]],
                           u'series_id': u'sid=sjvirt\\tests\\3@localize'},
                          {u'columns': [u''],
                           u'excel_numformat': u'#,##0.#0',
                           u'label': u'',
                           u'numformat': None,
                           u'series_id': u'sid=sjvirt\\tests\\4@localize'}])

        # check globals_obj
        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        }, per_page=1, sort='sid desc')

        self.assertEqual(len(resp['series']), 1)
        self.assertEqual(resp['series'][0], {u'series_id': u'sid=sjvirt\\tests\\4@localize',
                                             u'numformat': None, u'excel_numformat': u'#,##0.#0',
                                             u'columns': [u''], u'label': u''})

        # try query
        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        }, per_page=1, sort='sid desc', query='sid=%s' % series_ids[0])

        self.assertEqual(len(resp['series']), 1)
        self.assertEqual(resp['series'][0],
                         {u'tz': {u'timezone': u'America/Chicago'},
                          u'excel_numformat': u'#,##0.#0',
                          u'label': u'', u'series_id': u'sid=sjvirt\\tests\\0@localize',
                          u'points': [[978285600000, 100.1],
                                      [980964000000, -1.9],
                                      [983383200000, 0.0],
                                      [1012500000000, -1000.22],
                                      [1051729200000, 909.0],
                                      [1072807200000, 103.0000009],
                                      [1075485600000, 1e-15]],
                          u'numformat': None, u'columns': [u'']})

        # try another way to get series
        chart_settings['series_queries'] = ['sid={}'.format(series_ids[0]), 'sid={}'.format(series_ids[3])]
        chart_settings['localize'] = 'n'
        chart_settings.pop('query')

        resp = self.post('/series', max_points=-1, data_json={
            'chart_settings': chart_settings,
        })['series']

        self.assertEqual(resp, [{u'columns': [u''],
                                 u'excel_numformat': u'#,##0.#0',
                                 u'label': u'',
                                 u'numformat': None,
                                 u'points': [[978307200000, 100.1],
                                             [980985600000, -1.9],
                                             [983404800000, 0.0],
                                             [1012521600000, -1000.22],
                                             [1051747200000, 909.0],
                                             [1072828800000, 103.0000009],
                                             [1075507200000, 1e-15]],
                                 u'series_id': u'sid=sjvirt\\tests\\0'},
                                {u'columns': [u''],
                                 u'excel_numformat': u'#,##0.#0',
                                 u'label': u'',
                                 u'numformat': None,
                                 u'points': [[978307200000, 1.0],
                                             [980985600000, 2.0],
                                             [983404800000, 3.0],
                                             [1012521600000, 3.0],
                                             [1051747200000, 5.0],
                                             [1072828800000, 6.0],
                                             [1075507200000, 7.0]],
                                 u'series_id': u'sid=sjvirt\\tests\\3'}])

        chart_settings['series_queries'] = ['sid={}'.format(series_ids[0]), 'sid={}'.format(series_ids[3])]
        chart_settings['localize'] = 'y'

        resp = self.post('/series', max_points=-1, data_json={
            'chart_settings': chart_settings,
        })['series']

        self.assertEqual(resp, [{u'columns': [u''],
                                 u'excel_numformat': u'#,##0.#0',
                                 u'label': u'',
                                 u'numformat': None,
                                 u'points': [[978285600000, 100.1],
                                             [980964000000, -1.9],
                                             [983383200000, 0.0],
                                             [1012500000000, -1000.22],
                                             [1051729200000, 909.0],
                                             [1072807200000, 103.0000009],
                                             [1075485600000, 1e-15]],
                                 u'series_id': u'sid=sjvirt\\tests\\0@localize',
                                 u'tz': {u'timezone': u'America/Chicago'}},
                                {u'columns': [u''],
                                 u'excel_numformat': u'#,##0.#0',
                                 u'label': u'',
                                 u'numformat': None,
                                 u'points': [[978307200000, 1.0],
                                             [980985600000, 2.0],
                                             [983404800000, 3.0],
                                             [1012521600000, 3.0],
                                             [1051747200000, 5.0],
                                             [1072828800000, 6.0],
                                             [1075507200000, 7.0]],
                                 u'series_id': u'sid=sjvirt\\tests\\3@localize'}])

        # check facets for mget in charts
        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        }, facets='l3')
        self.assertEqual(
            resp['facets'],
            {u'l3': {u'total': 2, u'terms': [
                {u'count': 1, u'term': u'0'},
                {u'count': 1, u'term': u'3'}
            ], u'other': 0, u'missing': 0}}
        )

        # chart settings with query
        chart_settings.pop('series_queries')
        chart_settings['query'] = 'sid:sjvirt\\tests'
        chart_settings['localize'] = 'y'

        resp = self.post('/series', max_points=-1, data_json={
            'chart_settings': chart_settings,
        }, per_page=1)['series']

        self.assertEqual(resp, [
            {u'tz': {u'timezone': u'America/Chicago'}, u'excel_numformat': u'#,##0.#0', u'label': u'',
             u'series_id': u'sid=sjvirt\\tests\\0@localize',
             u'points': [[978285600000, 100.1], [980964000000, -1.9], [983383200000, 0.0], [1012500000000, -1000.22],
                         [1051729200000, 909.0], [1072807200000, 103.0000009], [1075485600000, 1e-15]],
             u'numformat': None, u'columns': [u'']}
        ])

        resp = self.post('/series', max_points=-1, data_json={
            'chart_settings': chart_settings,
        }, query='sid={}'.format(series_ids[-3]))['series']

        self.assertEqual(resp, [{u'excel_numformat': u'#,##0.#0', u'label': u'', u'series_id': u'sid=sjvirt\\tests\\3@localize',
                                 u'points': [[978307200000, 1.0],
                                             [980985600000, 2.0],
                                             [983404800000, 3.0],
                                             [1012521600000, 3.0],
                                             [1051747200000, 5.0],
                                             [1072828800000, 6.0],
                                             [1075507200000, 7.0]],
                                 u'numformat': None, u'columns': [u'']}])

        # test expressions
        chart_settings = {
            "sort": "custom",
            "color_mode": "default",
            "title": "New Chart22232",
            "fields": [
                "description",
                "test_field",
                "timezone"
            ],
            "query_limit": 200,
            "series_settings_obj": [
                {
                    "label": "test 2",
                    "query": 'sid={}'.format(series_ids[2])
                },
                {
                    "label": "test 1",
                    "query": 'sid={}'.format(series_ids[1])
                }
            ],
            "value": "",
            "last_points": "y",
            "custom_sort": ['sid={}'.format(l) for l in series_ids],
            "value_type": "point",
            "chart_type": "line",
            "series_queries": ["={{sid=%s}} * G.two" % series_ids[0]],
            'g_expression': 'G.two=2',
            "localize": "n"
        }

        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        }, facets='set')

        self.assertEqual(resp, {'column_headers': [''],
                                'date_format': 'YYYY-MM-DD',
                                'facets': {'set': {'missing': 0,
                                                   'other': 0,
                                                   'terms': [{'count': 1, 'term': 'aspect'},
                                                             {'count': 1, 'term': 'meta.fieldsbytes_num'},
                                                             {'count': 1, 'term': 'meta.points_count'},
                                                             {'count': 1, 'term': 'timezone'},
                                                             {'count': 1, 'term': 'vrt'},
                                                             {'count': 1, 'term': 'vrt.source'}],
                                                   'total': 6}},
                                'series': [{'columns': [''],
                                            'excel_numformat': '#,##0.#0',
                                            'label': '',
                                            'numformat': None,
                                            'points': [[978307200000, 200.2],
                                                       [980985600000, -3.8],
                                                       [983404800000, 0.0],
                                                       [1012521600000, -2000.44],
                                                       [1051747200000, 1818.0],
                                                       [1072828800000, 206.0000018],
                                                       [1075507200000, 2e-15]],
                                            'series_id': '={{sid=sjvirt\\tests\\0}} * G.two',
                                            'xpr': {'series_queries': ['sid=sjvirt\\tests\\0']}}],
                                'success': True,
                                'total': 1})

        chart_settings = {
            "sort": "custom",
            "color_mode": "default",
            "title": "New Chart22232",
            "fields": [
                "description",
                "test_field",
                "timezone"
            ],
            "query_limit": 200,
            "series_settings_obj": [
                {
                    "label": "test 2",
                    "query": 'sid={}'.format(series_ids[2])
                },
                {
                    "label": "test 1",
                    "query": 'sid={}'.format(series_ids[1])
                }
            ],
            "value": "",
            "last_points": "y",
            "custom_sort": ['sid={}'.format(l) for l in series_ids],
            "value_type": "point",
            "chart_type": "line",
            "query": "={{sid=%s}} * G.two" % series_ids[0],
            'g_expression': 'G.two=2',
            "localize": "n"
        }

        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        }, facets='set')

        self.assertEqual(resp, {'all_queries': ['sid=sjvirt\\tests\\0'],
                                'column_headers': [''],
                                'date_format': 'YYYY-MM-DD',
                                'facets': {'set': {'missing': 0,
                                                   'other': 0,
                                                   'terms': [{'count': 1, 'term': 'aspect'},
                                                             {'count': 1, 'term': 'meta.fieldsbytes_num'},
                                                             {'count': 1, 'term': 'meta.points_count'},
                                                             {'count': 1, 'term': 'timezone'},
                                                             {'count': 1, 'term': 'vrt'},
                                                             {'count': 1, 'term': 'vrt.source'}],
                                                   'total': 6}},
                                'series': [{'columns': [''],
                                            'excel_numformat': '#,##0.#0',
                                            'label': '',
                                            'numformat': None,
                                            'points': [[978307200000, 200.2],
                                                       [980985600000, -3.8],
                                                       [983404800000, 0.0],
                                                       [1012521600000, -2000.44],
                                                       [1051747200000, 1818.0],
                                                       [1072828800000, 206.0000018],
                                                       [1075507200000, 2e-15]],
                                            'series_id': 'sid=sjvirt\\tests\\0',
                                            'xpr': {'series_queries': ['sid=sjvirt\\tests\\0']}}],
                                'success': True,
                                'total': 1})

    def test_scroll(self):
        resp = self.get('/series', query='set:aspect AND sid:sjvirt', scroll_batch_size=2, scroll='y', fields='aspect',
                        operators='@YA', date_format='iso', df='2001-01-01', dt='2016-01-01',
                        max_points=1, sort='aspect asc')

        self.assertEqual(resp['series'][0], {u'series_id': u'sjvirt\\tests\\1@YA',
                                             u'fields': {u'aspect': u'Gas'},
                                             u'points': [[u'2010-01-01T00:00:00', 2.6285714285714294]]})

        self.assertEqual(resp['series'][1], {u'series_id': u'sjvirt\\tests\\0@YA',
                                             u'fields': {u'aspect': u'Oil'},
                                             u'points': [[u'2001-01-01T00:00:00', 32.73333333333333]]})

        self.assertEqual(resp['total'], len(series_ids))
        scroll_id = resp['scroll_id']

        resp = self.get('/series', scroll_id=scroll_id, date_format='iso',)

        self.assertIn('scroll_id', resp)

        self.assertEqual(len(resp['series']), 2)

        self.assertEqual(resp['series'][0], {u'fields': {u'aspect': u'Sun'},
                                             u'points': [[u'2001-01-01T00:00:00', 2.0]],
                                             u'series_id': u'sjvirt\\tests\\3@YA'})

    def test_expressions_autocomplete(self):
        def assert_valid_autocomplete(res):
            self.assertIn('hint', res)
            for o in res.get('options', []):
                self.assertIn('complete', o)
                self.assertIn('value', o)

        # =query
        assert_valid_autocomplete(self.get('/series/autocomplete', query='='))
        assert_valid_autocomplete(self.get('/series/autocomplete', query='=sjquery('))

        # =series_id
        assert_valid_autocomplete(self.get('/series/autocomplete', series_id='='))
        assert_valid_autocomplete(self.get('/series/autocomplete', series_id='=sj'))

        self.assertEqual(self.get('/series/autocomplete', query='={{}}.abs(', )['function']['hint'], 'abs()')
        self.get('/series/autocomplete', query=u'=((¡', )

    def test_jinja_operator(self):
        # virt series
        res = self.get('/series',
                       series_id=series_ids[0],
                       fields='=aspect is {{aspect}}, aspect')['series'][0]
        self.assertEqual(res['fields'], {'=aspect is {{aspect}}': 'aspect is Oil', 'aspect': 'Oil'})

        # non-virt series
        res = self.get('/series',
                       series_id=series_ids[-3],
                       fields='=aspect is {{aspect}}, aspect')['series'][0]
        self.assertEqual(res['fields'], {'=aspect is {{aspect}}': 'aspect is Sun', 'aspect': 'Sun'})

    def test_expressions_edge_cases(self):
        # here are misc regressions tests
        # should be not-found on this
        self.assertEqual(
            self.get('/series', series_id='={{sid=unknown}}', max_points=-1, fields='*')['series'][0],
            {u'error': u'series_not_found'}
        )

        self.assertEqual(
            self.post('/series', data_json={
                'series_queries': ['={{sid=unknown}}']
            })['series'][0],
            {u'error': u'series_not_found'}
        )

    def test_debug_query(self):
        r = self.get('/series', max_points=-1, query='sid=%s' % series_ids[0], fields='*,pts.max', debug=True)
        self.assertIn('debug', r)
        self.assertIn('_query', r['debug'])

    def test_fields_stats(self):
        # try single get virt
        ser = self.get('/series', max_points=-1, series_id=series_ids[0], fields='*,pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'pts.max': 909.0,
                                         u'timezone': u'America/Chicago',
                                         u'aspect': u'Oil',
                                         u'vrt': {u'source': u'test'}})

        # single get non-virt
        ser = self.get('/series', max_points=-1, series_id=series_ids[3], fields='aspect,pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'pts.max': 7.0, u'aspect': u'Sun'})

        # expression series
        ser = self.get('/series', max_points=-1, series_id=series_ids[5], fields='pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'pts.max': 1000.22})

        ser = self.get('/series', max_points=-1, series_id='={{sid=%s}} * 100' % series_ids[1], fields='*,pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'aspect': u'Gas', u'pts.max': 4010.0, u'vrt': {u'source': u'test'}})

        ser = self.get('/series', max_points=-1, query='={{sid=%s}} * 100' % series_ids[1], fields='*,pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'aspect': u'Gas', u'pts.max': 4010.0, u'vrt': {u'source': u'test'}})

        # regression, got exception on empty points
        ser = self.get('/series', max_points=-1, query='=sjsordf(r"sid=%s", df="2001-01-01", dt="2018-01-01")' % series_ids[0],
                       fields='*,pts.max', df='0yb', dt='0yb', )['series'][0]
        self.assertEqual(ser['fields'], {u'aspect': u'Oil',
                                         u'timezone': u'America/Chicago',
                                         u'vrt': {u'source': u'test'}})

        # query
        ser = self.get('/series', max_points=-1, query='sid=%s' % series_ids[0], fields='*,pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'pts.max': 909.0,
                                         u'timezone': u'America/Chicago',
                                         u'aspect': u'Oil',
                                         u'vrt': {u'source': u'test'}})

        # single get non-virt
        ser = self.get('/series', max_points=-1, query='sid=%s' % series_ids[3], fields='aspect,pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'pts.max': 7.0, u'aspect': u'Sun'})

        # expression series
        ser = self.get('/series', max_points=-1, query='sid=%s' % series_ids[5], fields='pts.max')['series'][0]
        self.assertEqual(ser['fields'], {u'pts.max': 1000.22})

        # multiget
        ser = self.post('/series', data_json={
            'series_queries': [
                'sid=%s' % series_ids[0],
                'sid=%s' % series_ids[3],
                'sid=%s' % series_ids[5],

            ]
        }, fields='pts.max,pts.min,sid,aspect', max_points=-1, date_format='iso')['series']
        self.assertEqual(ser, [
            {u'series_id': u'$sid=sjvirt\\tests\\0', u'fields': {u'pts.max': 909.0,
                                                        u'aspect': u'Oil',
                                                        u'pts.min': -1000.22,
                                                        u'sid': u'sjvirt\\tests\\0'},
             u'points': [[u'2001-01-01T00:00:00', 100.1],
                         [u'2001-02-01T00:00:00', -1.9],
                         [u'2001-03-01T00:00:00', 0.0],
                         [u'2002-02-01T00:00:00', -1000.22],
                         [u'2003-05-01T00:00:00', 909.0],
                         [u'2003-12-31T00:00:00', 103.0000009],
                         [u'2004-01-31T00:00:00', 1e-15]]},

            {u'series_id': u'$sid=sjvirt\\tests\\3',
             u'fields': {u'pts.max': 7.0, u'aspect': u'Sun', u'pts.min': 1.0, u'sid': u'sjvirt\\tests\\3'},
             u'points': [[u'2001-01-01T00:00:00', 1.0],
                         [u'2001-02-01T00:00:00', 2.0],
                         [u'2001-03-01T00:00:00', 3.0],
                         [u'2002-02-01T00:00:00', 3.0],
                         [u'2003-05-01T00:00:00', 5.0],
                         [u'2003-12-31T00:00:00', 6.0],
                         [u'2004-01-31T00:00:00', 7.0]]},

            {u'series_id': u'$sid=sjvirt\\tests\\5',
             u'xpr': {u'series_queries': [u'sid=sjvirt\\tests\\0']},
             u'points': [[u'2001-01-01T00:00:00', -100.1],
                         [u'2001-02-01T00:00:00', 1.9],
                         [u'2001-03-01T00:00:00', -0.0],
                         [u'2002-02-01T00:00:00', 1000.22],
                         [u'2003-05-01T00:00:00', -909.0],
                         [u'2003-12-31T00:00:00', -103.0000009],
                         [u'2004-01-31T00:00:00', -1e-15]],
             u'fields': {u'pts.max': 1000.22,
                         u'aspect': u'Water',
                         u'pts.min': -909.0,
                         u'sid': u'sjvirt\\tests\\5'}}])

        # try charts
        chart_settings = {
            "sort": "custom",
            "color_mode": "default",
            "title": "New Chart22232",
            "fields": [
                "description",
                "test_field",
                "timezone",
                "pts.max"
            ],
            "query_limit": 200,
            "series_settings_obj": [
                {
                    "label": "test 2",
                    "query": 'sid={}'.format(series_ids[2])
                },
                {
                    "label": "test 1",
                    "query": 'sid={}'.format(series_ids[1])
                }
            ],
            "value": "",
            "last_points": "y",
            "custom_sort": ['sid={}'.format(l) for l in series_ids],
            "value_type": "mixed",
            "value_fields": "timezone,pts.min",
            "chart_type": "line",
            "query": "sid:sjvirt\\tests and not set:xpr",
            "localize": "n"
        }

        resp = self.post('/series', data_json={
            'chart_settings': chart_settings
        })

        self.assertEqual(resp['series'],
                         [
                             {u'fields': {u'timezone': u'America/Chicago',
                                          u'pts.min': -1000.22},
                              u'excel_numformat': u'#,##0.#0',
                              u'label': u'',
                              u'series_id': u'sid=sjvirt\\tests\\0',
                              u'points': [[978307200000, 100.1],
                                          [980985600000, -1.9],
                                          [983404800000, 0.0],
                                          [1012521600000, -1000.22],
                                          [1051747200000, 909.0],
                                          [1072828800000, 103.0000009],
                                          [1075507200000, 1e-15]],
                              u'numformat': None,
                              u'columns': [u'', u'America/Chicago', -1000.22]},

                             {u'fields': {u'pts.min': -80.0},
                              u'excel_numformat': u'#,##0.#0',
                              u'label': u'test 1',
                              u'series_id': u'sid=sjvirt\\tests\\1',
                              u'points': [[1262307600000, 20.1],
                                          [1262311200000, 30.1],
                                          [1262314800000, 40.1],
                                          [1262318400000, 0.1],
                                          [1262322000000, -80.0],
                                          [1262325600000, -2.0],
                                          [1262329200000, 10.0]],
                              u'numformat': None,
                              u'columns': [u'test 1', None, -80.0]},

                             {u'fields': {u'pts.min': 1000.0},
                              u'excel_numformat': u'#,##0.#0',
                              u'label': u'test 2',
                              u'series_id': u'sid=sjvirt\\tests\\2',
                              u'points': [[978307200000, 1000.0],
                                          [1009843200000, 2000.0],
                                          [1041379200000, 3000.0],
                                          [1072915200000, 4000.0],
                                          [1104537600000, 5000.0],
                                          [1136073600000, 6000.0],
                                          [1167609600000, 7000.0]],
                              u'numformat': None,
                              u'columns': [u'test 2', None, 1000.0]},

                             {u'fields': {u'pts.min': 1.0},
                              u'excel_numformat': u'#,##0.#0',
                              u'label': u'',
                              u'series_id': u'sid=sjvirt\\tests\\3',
                              u'points': [[978307200000, 1.0],
                                          [980985600000, 2.0],
                                          [983404800000, 3.0],
                                          [1012521600000, 3.0],
                                          [1051747200000, 5.0],
                                          [1072828800000, 6.0],
                                          [1075507200000, 7.0]],
                              u'numformat': None,
                              u'columns': [u'', None, 1.0]},

                             {u'series_id': u'sid=sjvirt\\tests\\4',
                              u'numformat': None,
                              u'excel_numformat': u'#,##0.#0',
                              u'columns': [u'', None, None],
                              u'label': u''}])

    @patch('virt.utils.get_account_settings', return_value={'aggs_default_e': ['y']})
    def test_default_operators_behaviour(self, acc_stgs):
        yearly_end = [[u'2000-01-01T00:00:00', 100.1]]
        # making sure A:y behaves as @A:ye
        pts = self.get('/series', series_id=series_ids[0],
                       operators='@A:y',
                       date_format='iso', max_points=1)['series'][0]['points']
        self.assertEqual(pts, yearly_end)

        # explicit period end
        pts = self.get('/series', series_id=series_ids[0],
                       operators='@A:ye',
                       date_format='iso', max_points=1)['series'][0]['points']
        self.assertEqual(pts, yearly_end)

        # explicit period begin
        pts = self.get('/series', series_id=series_ids[0],
                       operators='@A:yb',
                       date_format='iso', max_points=1)['series'][0]['points']

        self.assertAlmostEqual(pts[0][1], 32.73, 2)

    def test_read_vrt_with_reversed_df_dt(self):
        res = self.post('/series',
                        data_json={
                            'series_queries': [
                                {
                                    'query': 'sid={}'.format(series_ids[0]),
                                    'max_points': 2,
                                    'df': 'MIN',
                                    'dt': 'MAX'
                                },
                                {
                                    'query': 'sid={}'.format(series_ids[0]),
                                    'max_points': 2,
                                    'df': 'MAX',
                                    'dt': 'MIN'
                                },
                            ]
                        },
                        max_points=2,
                        date_format='iso'
                        )['series']

        self.assertEqual(res[0]['points'], [['2001-01-01T00:00:00', 100.1], ['2001-02-01T00:00:00', -1.9]])
        self.assertEqual(res[1]['points'], [['2004-01-31T00:00:00', 1e-15], ['2003-12-31T00:00:00', 103.0000009]])

    def test_viz_export(self):
        viz_settings = {
            "viz": {
                "query": {
                    "query": "",
                    "series_queries": [f"sid={series_ids[0]}"],
                    "sort": "alpha", "label_field": "description"
                },
                "points": {
                    "date_format": "YYYY-MM-DD hh:mm"
                },
                "chart": {
                    "on": True, "value_type": "point", "chart_type": "line", "default_legend": "none",
                    "series_settings_obj": [
                        {"label": "TEST",
                         "query": f"sid={series_ids[0]}"}
                    ],
                    "editor": True
                },
                "table": {"columns": [], "points": True},
                "title": "Viz"
            }
        }
        data = self.post('/series/excel', data_json=viz_settings, binary=True, tab=1)
        wb = xlrd.open_workbook(file_contents=data)
        self.assertEqual(wb.sheet_names(), ['Intro', 'Data'])

        data_sheet = wb.sheet_by_index(1)
        self.assertEqual(
            [r.value for r in data_sheet.row_slice(1)],
            [100.1, -1.9, 0.0, -1000.22, 909.0, 103.0000009, 1e-15]
        )

    def test_pre_post_operators(self):
        # test with a regular expression
        # by default it's pre
        resp = self.get('/series',
                        series_id=r'={{sid=%s}}@filter:>0@S:y' % series_ids[0],
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', 100.1],
                                          ['2003-01-01T00:00:00', 1012.0000009],
                                          ['2004-01-01T00:00:00', 1e-15]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(resp['xpr']['trace'], {
            'expression': '={{sid=sjvirt\\tests\\0}}',
            'post_operators': [],
            'pre_operators': ['filter:>0', 'S:y'],
            'reads': [{'query': 'sid=sjvirt\\tests\\0',
                       'results': [{'sid': 'sjvirt\\tests\\0@filter:>0@S:y'}],
                       'results_num': 1}]
        })

        # now filter is post
        resp = self.get('/series',
                        series_id=r'={{sid=%s}}@pre@S:y@post@filter:>0' % series_ids[0],
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', 98.19999999999999],
                                          ['2003-01-01T00:00:00', 1012.0000009],
                                          ['2004-01-01T00:00:00', 1e-15]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(resp['xpr']['trace'], {'expression': '={{sid=sjvirt\\tests\\0}}',
                                                'post_operators': ['post', 'filter:>0'],
                                                'pre_operators': ['pre', 'S:y'],
                                                'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                                           'results': [{'sid': 'sjvirt\\tests\\0@pre@S:y'}],
                                                           'results_num': 1}]})

        # this throws an error because @pre applied after @post
        self.get('/series',
                 series_id=r'={{sid=%s}}@post@filter:>0@pre@S:y' % series_ids[0],
                 max_points=-1,
                 date_format='iso', debug='y', expect_error='expression')

        # this should end up with error because filter is before pre
        self.get('/series',
                 series_id=r'={{sid=%s}}@filter:>0@pre@S:y' % series_ids[0],
                 max_points=-1,
                 date_format='iso', debug='y', expect_error='expression')
        # test xpr series
        resp = self.get('/series',
                        series_id='{}@filter:>0@S:y'.format(series_ids[5]),
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        # for xpr series default is post
        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', 1.9],
                                          ['2002-01-01T00:00:00', 1000.22]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(resp['xpr']['trace'], {'expression': '={{sid=sjvirt\\tests\\0}} * G.a',
                                                'g_expression': 'G.a = -1',
                                                'post_operators': ['filter:>0', 'S:y'],
                                                'pre_operators': [],
                                                'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                                           'results': [{'sid': 'sjvirt\\tests\\0'}],
                                                           'results_num': 1}]})

        # make all pre
        resp = self.get('/series',
                        series_id='{}@pre@filter:>0@S:y'.format(series_ids[5]),
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', -100.1],
                                          ['2003-01-01T00:00:00', -1012.0000009],
                                          ['2004-01-01T00:00:00', -1e-15]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(resp['xpr']['trace'], {'expression': '={{sid=sjvirt\\tests\\0}} * G.a',
                                                'g_expression': 'G.a = -1',
                                                'post_operators': [],
                                                'pre_operators': ['pre', 'filter:>0', 'S:y'],
                                                'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                                           'results': [{'sid': 'sjvirt\\tests\\0@pre@filter:>0@S:y'}],
                                                           'results_num': 1}]})

        resp = self.get('/series',
                        series_id='{}@pre@S:y@post@filter:>0'.format(series_ids[5]),
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2002-01-01T00:00:00', 1000.22]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(resp['xpr']['trace'], {'expression': '={{sid=sjvirt\\tests\\0}} * G.a',
                                                'g_expression': 'G.a = -1',
                                                'post_operators': ['post', 'filter:>0'],
                                                'pre_operators': ['pre', 'S:y'],
                                                'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                                           'results': [{'sid': 'sjvirt\\tests\\0@pre@S:y'}],
                                                           'results_num': 1}]})

        # this should throw an error because @pre applied after @post
        self.get('/series',
                 series_id='{}@post@filter:>0@pre@S:y'.format(series_ids[5]),
                 max_points=-1,
                 date_format='iso', debug='y', expect_error='expression')

        # expression with xpr series
        resp = self.get('/series',
                        series_id=r'={{sid=%s}} * -1@filter:>0@S:y' % series_ids[5],
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', -1.9],
                                          ['2002-01-01T00:00:00', -1000.22]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(resp['xpr']['trace'], {'expression': '={{sid=sjvirt\\tests\\5}} * -1',
                                                'post_operators': [],
                                                'pre_operators': ['filter:>0', 'S:y'],
                                                'reads': [{'query': 'sid=sjvirt\\tests\\5',
                                                           'results': [{'expression': '={{sid=sjvirt\\tests\\0}} * G.a',
                                                                        'g_expression': 'G.a = -1',
                                                                        'post_operators': ['filter:>0', 'S:y'],
                                                                        'pre_operators': [],
                                                                        'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                                                                   'results': [
                                                                                       {'sid': 'sjvirt\\tests\\0'}],
                                                                                   'results_num': 1}]}],
                                                           'results_num': 1}]})

        resp = self.get('/series',
                        series_id=r'={{sid=%s}} * -1@pre@filter:>0@S:y' % series_ids[5],
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', 100.1],
                                          ['2003-01-01T00:00:00', 1012.0000009],
                                          ['2004-01-01T00:00:00', 1e-15]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr']['trace'],
            {
                'expression': '={{sid=sjvirt\\tests\\5}} * -1',
                'post_operators': [],
                'pre_operators': ['pre', 'filter:>0', 'S:y'],
                'reads': [{'query': 'sid=sjvirt\\tests\\5',
                           'results': [{'expression': '={{sid=sjvirt\\tests\\0}} * G.a',
                                        'g_expression': 'G.a = -1',
                                        'post_operators': [],
                                        'pre_operators': ['pre', 'filter:>0', 'S:y'],
                                        'reads': [
                                            {
                                                'query': 'sid=sjvirt\\tests\\0',
                                                'results': [
                                                    {
                                                        'sid': 'sjvirt\\tests\\0@pre@filter:>0@S:y',
                                                    },
                                                ],
                                                'results_num': 1,
                                            },
                                        ]}],
                           'results_num': 1}],
            },
        )

        resp = self.get('/series',
                        series_id=r'={{sid=%s}} * -1@post@filter:>0@S:y' % series_ids[5],
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', 100.1],
                                          ['2003-01-01T00:00:00', 1012.0000009],
                                          ['2004-01-01T00:00:00', 1e-15]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr']['trace'],
            {'expression': '={{sid=sjvirt\\tests\\5}} * -1',
             'post_operators': ['post', 'filter:>0', 'S:y'],
             'pre_operators': [],
             'reads': [{'query': 'sid=sjvirt\\tests\\5',
                        'results': [{'expression': '={{sid=sjvirt\\tests\\0}} * G.a',
                                     'g_expression': 'G.a = -1',
                                     'post_operators': [],
                                     'pre_operators': [],
                                     'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                                'results': [{'sid': 'sjvirt\\tests\\0'}],
                                                'results_num': 1}]}],
                        'results_num': 1}]},
        )

        resp = self.get('/series',
                        series_id=r'={{sid=%s}} * -1@pre@max_points:1@post@S:y' % series_ids[5],
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', 100.1]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr'],
            {'series_queries': ['sid=sjvirt\\tests\\0'],
             'trace': {'expression': '={{sid=sjvirt\\tests\\5}} * -1',
                       'post_operators': ['post', 'S:y'],
                       'pre_operators': ['pre', 'max_points:1'],
                       'reads': [{'query': 'sid=sjvirt\\tests\\5',
                                  'results': [{'expression': '={{sid=sjvirt\\tests\\0}} * '
                                                             'G.a',
                                               'g_expression': 'G.a = -1',
                                               'post_operators': [],
                                               'pre_operators': ['pre', 'max_points:1'],
                                               'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                                          'results': [{
                                                              'sid': 'sjvirt\\tests\\0@pre@max_points:1'}],
                                                          'results_num': 1}]}],
                                  'results_num': 1}]}},
        )

        resp = self.get('/series',
                        series_id=r'%s@pre@max_points:4@post@max_points:1@S:y' % series_ids[5],
                        max_points=-1,
                        date_format='iso', debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2001-01-01T00:00:00', -98.19999999999999]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr'],
            {'series_queries': ['sid=sjvirt\\tests\\0'],
             'trace': {'expression': '={{sid=sjvirt\\tests\\0}} * G.a',
                       'g_expression': 'G.a = -1',
                       'post_operators': ['post', 'max_points:1', 'S:y'],
                       'pre_operators': ['pre', 'max_points:4'],
                       'reads': [{'query': 'sid=sjvirt\\tests\\0',
                                  'results': [{'sid': 'sjvirt\\tests\\0@pre@max_points:4'}],
                                  'results_num': 1}]}},
        )

        # test @pre/@post of @asof/@repdate-like
        sid_xpr = 'sjvirt\\tests\\pre_operators\\xpr'
        sid_regular = 'sjvirt\\tests\\pre_operators\\regular'
        reported_date = datetime(2020, 1, 1)

        with self.sj.register_job('test') as job:
            job.write(f'sid={sid_regular}', points=[
                Point(datetime(2020, 1, 1), 10),
                Point(datetime(2020, 1, 2), 20),
                Point(datetime(2020, 1, 3), 30),
            ])
            job.write_reported(f'sid={sid_regular}',
                               reported_date=reported_date,
                               points=[
                                   Point(datetime(2020, 1, 1), 1),
                                   Point(datetime(2020, 1, 2), 2),
                                   Point(datetime(2020, 1, 3), 3),
                               ])

            job.write(f'sid={sid_xpr}', fields={
                'xpr.expression': f'={{{{sid={sid_regular}}}}} * 2'
            })

        self.sj.raw.post('/series/refresh')

        resp = self.get('/series', series_id=f'{sid_xpr}',
                        max_points=-1,
                        date_format='iso',
                        debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2020-01-01T00:00:00', 20.0],
                                          ['2020-01-02T00:00:00', 40.0],
                                          ['2020-01-03T00:00:00', 60.0]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr']['trace'],
            {'expression': '={{sid=sjvirt\\tests\\pre_operators\\regular}} * 2',
             'post_operators': [],
             'pre_operators': [],
             'reads': [{'query': 'sid=sjvirt\\tests\\pre_operators\\regular',
                        'results': [{'sid': 'sjvirt\\tests\\pre_operators\\regular'}],
                        'results_num': 1}]},
        )

        resp = self.get('/series', series_id=f'{sid_xpr}@filter:=20',
                        max_points=-1,
                        date_format='iso',
                        debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2020-01-01T00:00:00', 20.0]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr']['trace'],
            {'expression': '={{sid=sjvirt\\tests\\pre_operators\\regular}} * 2',
             'post_operators': ['filter:=20'],
             'pre_operators': [],
             'reads': [{'query': 'sid=sjvirt\\tests\\pre_operators\\regular',
                        'results': [{'sid': 'sjvirt\\tests\\pre_operators\\regular'}],
                        'results_num': 1}]},
        )

        resp = self.get('/series', series_id=f'{sid_xpr}@pre@filter:=20',
                        max_points=-1,
                        date_format='iso',
                        debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2020-01-02T00:00:00', 40.0]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr']['trace'],
            {'expression': '={{sid=sjvirt\\tests\\pre_operators\\regular}} * 2',
             'post_operators': [],
             'pre_operators': ['pre', 'filter:=20'],
             'reads': [{'query': 'sid=sjvirt\\tests\\pre_operators\\regular',
                        'results': [{'sid': 'sjvirt\\tests\\pre_operators\\regular@pre@filter:=20'}],
                        'results_num': 1}]},
        )

        # this throw an error
        self.get('/series', series_id=f'{sid_xpr}@repdate:{reported_date.isoformat()}',
                 max_points=-1,
                 date_format='iso',
                 debug='y', expect_error='expression')

        # this one is working fine
        resp = self.get('/series', series_id=f'{sid_xpr}@pre@repdate:{reported_date.isoformat()}',
                        max_points=-1,
                        date_format='iso',
                        debug='y', )['series'][0]

        self.assertEqual(resp['points'], [['2020-01-01T00:00:00', 2.0],
                                          ['2020-01-02T00:00:00', 4.0],
                                          ['2020-01-03T00:00:00', 6.0]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr']['trace'],
            {'expression': '={{sid=sjvirt\\tests\\pre_operators\\regular}} * 2',
             'post_operators': [],
             'pre_operators': ['pre', 'repdate:2020-01-01T00:00:00'],
             'reads': [{'query': 'sid=sjvirt\\tests\\pre_operators\\regular',
                        'results': [{'sid': 'sjvirt\\tests\\pre_operators\\regular@pre@repdate:2020-01-01T00:00:00'}],
                        'results_num': 1}]})

        # this throw an error
        resp = self.get('/series', series_id=f'={{{{{sid_xpr}}}}}@pre@repdate:{reported_date.isoformat()}',
                        max_points=-1,
                        date_format='iso',
                        debug='y')['series'][0]

        self.assertEqual(resp['points'], [['2020-01-01T00:00:00', 2.0],
                                          ['2020-01-02T00:00:00', 4.0],
                                          ['2020-01-03T00:00:00', 6.0]])

        self._remove_duration_from_trace(resp['xpr']['trace'])
        self.assertEqual(
            resp['xpr']['trace'],
            {'expression': '={{sjvirt\\tests\\pre_operators\\xpr}}',
             'post_operators': [],
             'pre_operators': ['pre', 'repdate:2020-01-01T00:00:00'],
             'reads': [{'query': 'sjvirt\\tests\\pre_operators\\xpr',
                        'results': [{'expression': '={{sid=sjvirt\\tests\\pre_operators\\regular}} '
                                                   '* 2',
                                     'post_operators': [],
                                     'pre_operators': ['pre',
                                                       'repdate:2020-01-01T00:00:00'],
                                     'reads': [{'query': 'sid=sjvirt\\tests\\pre_operators\\regular',
                                                'results': [{
                                                    'sid': 'sjvirt\\tests\\pre_operators\\regular@pre@repdate:2020-01-01T00:00:00'}],
                                                'results_num': 1}]}],
                        'results_num': 1}]},
        )

    def test_xpr_with_not_found_expression(self):
        xpr_sid = 'sjvirt\\tests\\xpr_series\\1'
        with self.sj.register_job('test') as job:
            job.put_fields(xpr_sid, {'xpr.expression': '={{sid=xxxxxx}}', 'description': 'xpr series'})

        self.sj.raw.post('/series/refresh')

        # this was raising not found, but just shouldn't return points
        resp = self.get('/series', series_id=xpr_sid, max_points=-1, fields='*')['series'][0]
        self.assertEqual(resp, {'fields': {'description': 'xpr series',
                                           'xpr': {'expression': '={{sid=xxxxxx}}'}},
                                'series_id': xpr_sid})

    def test_filter_by_job(self):
        pts = self.get('/series',
                       series_id='%s@filter:job>0' % (series_ids[3]),
                       max_points=-1,
                       date_format='iso',
                       include_job='y',
                       )['series'][0]['points']

        self.assertEqual(len(pts),7)

        self.assertNotIn(
            'points',
            self.get('/series',
                     series_id='%s@filter:job=0' % (series_ids[3]),
                     max_points=-1,
                     date_format='iso',
                     include_job='y',
                     )['series'][0],
        )

    @staticmethod
    def _remove_duration_from_trace(t):
        t.pop('duration', None)
        reads = t.get('reads', [])
        for rd in reads:
            rd.pop('duration')
            if 'results' in rd:
                for res in rd['results']:
                    TestSeries._remove_duration_from_trace(res)


class TestSeriesSjts(TestSeries):
    request_executor_cls = SjtsRequestExecutor

    def test_formulas(self):
        super().test_formulas()

        def _pts_to_iso(pts, indices):
            for p in pts:
                for ix in indices:
                    p[ix] = render_date(p[ix], 'iso')

        # testing how sjts behaves if one series contains job_id component and the other doesn't
        sers = self.post(
            '/series',
            data_json={
                'series_queries': [
                    '={{sid=%s}} * 100' % (series_ids[3],),
                    '={{sid=%s@B:M}} * 100' % (series_ids[3],),
                ]
            },
            max_points=10,
            df='2001-01-01',
            dt='2015-01-01',
            include_job='y',
            include_timestamp='y'
        )['series']

        # both will not include
        _pts_to_iso(sers[0]['points'], [0, 2])
        self.assertTrue(sers[0]['points'][0][2].startswith(datetime.today().isoformat()[:10]))
        _pts_to_iso(sers[1]['points'], [0, 2])
        self.assertTrue(sers[1]['points'][0][2].startswith(datetime.today().isoformat()[:10]))


class TestSeriesSnapshot(FlaskTestCase):
    JOB_1_FIELDS = {
        'aspect': 'Oil',
        'timezone': 'America/Chicago'
    }
    JOB_2_FIELDS = {
        'aspect': 'Gas',
        'unit': 'gallon',
    }
    VIRT_FIELDS = {'vrt': {'source': 'test'}}
    JOB_1_VIRT_FIELDS = dict(list(JOB_1_FIELDS.items()) + list(VIRT_FIELDS.items()))
    JOB_2_VIRT_FIELDS = dict(list(JOB_2_FIELDS.items()) + list(VIRT_FIELDS.items()))
    JOB_1_POINTS = [
        [u'2001-01-01T00:00:00', 100.1],
        [u'2001-02-01T00:00:00', -1.90],
        [u'2001-03-01T00:00:00', 0.],
        [u'2002-02-01T00:00:00', -1000.22],
        [u'2003-05-01T00:00:00', 909],
        [u'2003-12-31T00:00:00', 103.0000009],
        [u'2004-01-31T00:00:00', 0.000000000000001],
    ]
    JOB_2_POINTS = [
        [u'2001-01-01T00:00:00', 9100.1],
        [u'2001-02-01T00:00:00', -91.90],
        [u'2001-03-01T00:00:00', 0.],
        [u'2002-02-01T00:00:00', -91000.22],
        [u'2003-05-01T00:00:00', 909],
        [u'2003-12-31T00:00:00', 103.0000009],
        [u'2004-01-31T00:00:00', 0.000000000000001],
    ]
    # Used in sjvirt.sources.test for test virt source
    TEST_DATA = {
        'sjvirt\\tests\\sjvirt\\vrt_snapshot_sid': {
            'fields': JOB_2_VIRT_FIELDS,
            'points': JOB_2_POINTS,
        }
    }

    def setUp(self):
        super(TestSeriesSnapshot, self).setUp()

        try:
            self.snapshot_sid = 'sjvirt\\tests\\sjvirt\\snapshot_sid'
            self.vrt_snapshot_sid = 'sjvirt\\tests\\sjvirt\\vrt_snapshot_sid'

            with self.sj.register_job('test snapshot') as job:
                job.delete_by_query('sid:{}'.format(self.snapshot_sid), True)
                job.delete_by_query('sid:{}'.format(self.vrt_snapshot_sid), True)

            with self.sj.register_job('test snapshot job 1') as job:
                self.job_1 = job.job_id
                job.put_fields(self.snapshot_sid, self.JOB_1_FIELDS)
                job.put_fields(self.vrt_snapshot_sid, self.JOB_1_VIRT_FIELDS)

                job.put_points(self.snapshot_sid, [Point(parse(p[0]), p[1]) for p in self.JOB_1_POINTS])
                self.job_1_date = datetime.utcnow()
            time.sleep(5.)
            with self.sj.register_job('test snapshot job 2') as job:
                self.job_2 = job.job_id
                job.put_fields(self.snapshot_sid, self.JOB_2_FIELDS)
                job.put_fields(self.vrt_snapshot_sid, self.JOB_2_VIRT_FIELDS)

                job.put_points(self.snapshot_sid, [Point(parse(p[0]), p[1]) for p in self.JOB_2_POINTS])
        except:
            self.tearDown()
            raise

    def test_snapshot_with_non_virt_fields_works(self):
        res = self.get('/series',
                       series_id=self.snapshot_sid + f'@asof:j{self.job_1}',
                       fields='*',
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]

        self.assertEqual(res['points'], self.JOB_1_POINTS)
        self.assertEqual(res['fields'], self.JOB_1_FIELDS)

    def test_snapshot_with_virt_fields_without_points(self):
        res = self.get('/series',
                       series_id=self.vrt_snapshot_sid + f'@asof:j{self.job_1}',
                       fields='*',
                       max_points=0)['series'][0]
        self.assertEqual(res['fields'], self.JOB_1_VIRT_FIELDS)

    @retry(3)
    def test_snapshot_with_virt_fields_and_points(self):
        res = self.get('/series',
                       series_id=self.vrt_snapshot_sid + f'@asof:j{self.job_1}',
                       fields='*',
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')

        self.assertEqual(len(res['series']), 1)
        self.assertEqual(res['series'][0]['error'], 'invalid_parameters')

        job_date = datetime.utcnow()
        res = self.get('/series',
                       series_id=self.vrt_snapshot_sid + f'@asof:{self.job_1_date.isoformat()}',
                       fields='*',
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']
        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])
        # the same with asof operator
        res = self.get('/series',
                       series_id='%s@asof:%s' % (self.vrt_snapshot_sid, self.job_1_date.isoformat()),
                       fields='*',
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']
        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])

        res = self.get('/series',
                       series_id=self.vrt_snapshot_sid,
                       fields='*',
                       operators='@asof:%s' % self.job_1_date.isoformat(),
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']
        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])

        res = self.get('/series',
                       query='sid="%s"' % self.vrt_snapshot_sid,
                       fields='*',
                       operators='@asof:%s' % self.job_1_date.isoformat(),
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']
        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])

        # with @asof operator using timezone
        res = self.get('/series',
                       query='sid="%s"' % self.vrt_snapshot_sid,
                       fields='*',
                       operators='@asof:%s:UTC' % self.job_1_date.isoformat(),
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']
        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])

        # with @asof operator using ambiguous timezone
        self.get('/series',
                 query=r'={{sid=%s}}' % self.vrt_snapshot_sid,
                 fields='*',
                 operators='@asof:2009-10-31T23:30:America/St_Johns',
                 max_points=-1,
                 df='2001-01-01',
                 dt='2016-01-01',
                 date_format='iso',
                 expect_error='expression')  # raise internally in expression

        # with @asof operator using ambiguous timezone, no error when dst flag set
        self.get('/series',
                 query=r'={{sid=%s}}' % self.vrt_snapshot_sid,
                 fields='*',
                 operators='@asof:2009-10-31T23:30:America/St_Johns:n',
                 max_points=-1,
                 df='2001-01-01',
                 dt='2016-01-01',
                 date_format='iso')

        # with @asof operator using ambiguous timezone, no error when dst flag set
        self.get('/series',
                 query=r'={{sid=%s}}' % self.vrt_snapshot_sid,
                 fields='*',
                 operators='@asof:2009-10-31T23:30:America/St_Johns:y',
                 max_points=-1,
                 df='2001-01-01',
                 dt='2016-01-01',
                 date_format='iso')

        # also try expressions
        res = self.get('/series',
                       query=r'={{sid=%s}}' % self.vrt_snapshot_sid,
                       fields='*',
                       operators='@asof:%s' % self.job_1_date.isoformat(),
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']

        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])

        # also try expressions
        res = self.get('/series',
                       series_id=r'={{sid=%s}}' % self.vrt_snapshot_sid,
                       fields='*',
                       operators='@asof:%s' % self.job_1_date.isoformat(),
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']

        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])

        res = self.get('/series',
                       query=r'={{sid=%s}}@asof:%s' % (self.vrt_snapshot_sid, self.job_1_date.isoformat()),
                       fields='*',
                       max_points=-1,
                       df='2001-01-01',
                       dt='2016-01-01',
                       date_format='iso')['series'][0]['points']

        self.assertEqual(res, [[u'2001-01-01T00:00:00', 9100.1 + job_date.month],
                               [u'2001-02-01T00:00:00', -91.9 + job_date.month],
                               [u'2001-03-01T00:00:00', 0.0 + job_date.month],
                               [u'2002-02-01T00:00:00', -91000.22 + job_date.month],
                               [u'2003-05-01T00:00:00', 909.0 + job_date.month],
                               [u'2003-12-31T00:00:00', 103.0000009 + job_date.month],
                               [u'2004-01-31T00:00:00', 1e-15 + job_date.month]])

        # the snapshot date was just ignored, so we should get latest fields
        fields = self.get('/series',
                          series_id=self.vrt_snapshot_sid + f'@asof:{self.job_1_date.isoformat()}',
                          fields='*',
                          max_points=-1,
                          df='2001-01-01',
                          dt='2016-01-01',
                          date_format='iso')['series'][0]['fields']

        self.assertEqual(fields, {u'timezone': u'America/Chicago',
                                  u'unit': u'gallon', u'aspect': u'Gas',
                                  u'vrt': {u'source': u'test'}})
        # the same with asof
        fields = self.get('/series',
                          series_id='%s@asof:%s' % (self.vrt_snapshot_sid, self.job_1_date.isoformat()),
                          fields='*',
                          max_points=-1,
                          df='2001-01-01',
                          dt='2016-01-01',
                          date_format='iso')['series'][0]['fields']

        self.assertEqual(fields, {u'timezone': u'America/Chicago',
                                  u'unit': u'gallon', u'aspect': u'Gas',
                                  u'vrt': {u'source': u'test'}})

        # but getting snapshot fields if not points requested by user
        fields = self.get('/series',
                          series_id=self.vrt_snapshot_sid + f'@asof:{self.job_1_date.isoformat()}',
                          fields='*',
                          date_format='iso')['series'][0]['fields']
        self.assertEqual(fields, {u'timezone': u'America/Chicago',
                                  u'aspect': u'Oil',
                                  u'vrt': {u'source': u'test'}})

        # the same with asof

        fields = self.get('/series',
                          series_id='%s@asof:%s' % (self.vrt_snapshot_sid, self.job_1_date.isoformat()),
                          fields='*',
                          date_format='iso')['series'][0]['fields']
        self.assertEqual(fields, {u'timezone': u'America/Chicago',
                                  u'aspect': u'Oil',
                                  u'vrt': {u'source': u'test'}})

    def test_snapshot_scroll_with_virt_fields_and_points(self):
        res = self.get('/series',
                       series_id=self.vrt_snapshot_sid + f'@asof:j{self.job_1}',
                       scroll_batch_size=2,
                       scroll='y',
                       fields='aspect',
                       operators='@YA',
                       date_format='iso',
                       df='2001-01-01',
                       dt='2016-01-01',
                       max_points=100,
                       sort='aspect asc')
        self.assertEqual(len(res['series']), 1)
        self.assertEqual(res['series'][0]['error'], 'invalid_parameters')

        job_date = datetime.utcnow()
        pts = self.get('/series',
                       series_id=self.vrt_snapshot_sid + f'@asof:{job_date.isoformat()}',
                       scroll_batch_size=2,
                       scroll='y',
                       fields='aspect',
                       operators='@YA',
                       date_format='iso',
                       df='2001-01-01',
                       dt='2016-01-01',
                       max_points=100,
                       sort='aspect asc')['series'][0]['points']
        self.assertEqual(pts,
                         [[u'2001-01-01T00:00:00', 3002.7333333333336 + job_date.month],
                          [u'2002-01-01T00:00:00', -91000.22 + job_date.month],
                          [u'2003-01-01T00:00:00', 506.00000045 + job_date.month],
                          [u'2004-01-01T00:00:00', 1e-15 + job_date.month]])

    def test_snapshot_scroll_with_virt_fields_without_points(self):
        res = self.get('/series',
                       series_id=self.vrt_snapshot_sid + f'@asof:j{self.job_1}',
                       scroll_batch_size=2,
                       scroll='y',
                       fields='*',
                       operators='@YA',
                       date_format='iso',
                       df='2001-01-01',
                       dt='2016-01-01',
                       max_points=0,
                       sort='aspect asc')['series'][0]
        self.assertEqual(res['fields'], self.JOB_1_VIRT_FIELDS)


class TestSeriesSnapshotSjts(TestSeriesSnapshot):
    request_executor_cls = SjtsRequestExecutor


class TestRelativeDeltaOperators(FlaskTestCase):
    def setUp(self):
        self.request_executor = self.request_executor_cls(self)
        self.client = app.test_client(use_cookies=True)
        self.sj = Connection(config.REMOTE_URL, config.TEST_USER, config.TEST_API_KEY)

        self.calendar_id = 'SJVIRT_TEST_CAL'
        self.sj.raw.delete('/series/cache')

        with self.sj.register_job('virt test job', batch_size=10) as job:
            job.put_fields('calendars\\%s' % self.calendar_id, {'cal_id': self.calendar_id})
            job.remove_points('calendars\\%s' % self.calendar_id)
            job.put_points('calendars\\%s' % self.calendar_id,
                           [
                               Point(parse('2001-01-01'), 1),
                               Point(parse('2001-03-01'), 1),
                               Point(parse('2002-05-01'), 1),
                               Point(parse('2006-07-01'), 1),
                               Point(parse('2007-09-01'), 1),
                           ])

        self.job_id = job.job_id
        self.sj.raw.post('/series/refresh')
        self.sj.raw.delete('/series/cache')

    def tearDown(self):
        with self.sj.register_job('virt test job') as job:
            job.delete_by_query('sid="%s"' % ('calendars\\%s' % self.calendar_id))

    def test_relative_delta_operators(self):
        pts = self.get('/series',
                       series_id=series_ids[0],
                       df='-3d@cal:%s' % self.calendar_id,
                       dt='-2d@cal:%s' % self.calendar_id,
                       max_points=-1,
                       date_format='iso'
                       )['series'][0]['points']

        self.assertEqual(pts, [[u'2001-03-01T00:00:00', 0.0],
                               [u'2002-02-01T00:00:00', -1000.22]])

    def test_asof_cal_operator(self):
        self.sj.raw.delete('/series/cache')
        pts = self.get('/series',
                       series_id='%s@%s' % (series_ids[0], '@asof:-1d@cal:%s' % self.calendar_id),
                       max_points=2,
                       date_format='iso'
                       )['series'][0]['points']

        self.assertEqual(pts, [[u'2001-01-01T00:00:00', 107.1], [u'2001-02-01T00:00:00', 5.1]])

        pts = self.get('/series',
                       query='sid=%s@%s' % (series_ids[0], '@asof:-2d@cal:%s' % self.calendar_id),
                       max_points=2,
                       date_format='iso'
                       )['series'][0]['points']

        self.assertEqual(pts, [[u'2001-01-01T00:00:00', 105.1],
                               [u'2001-02-01T00:00:00', 3.1], ])

        pts = self.post('/series',
                        data_json={
                            'series_queries': [
                                'sid=%s@%s' % (series_ids[0], '@asof:-2d@cal:%s' % self.calendar_id)
                            ]
                        },
                        max_points=2,
                        date_format='iso'
                        )['series'][0]['points']

        self.assertEqual(pts, [[u'2001-01-01T00:00:00', 105.1],
                               [u'2001-02-01T00:00:00', 3.1], ])

    def test_filter_by_cal_id(self):
        pts = self.get('/series',
                       series_id='%s@filter:%s' % (series_ids[0], self.calendar_id),
                       max_points=-1,
                       date_format='iso'
                       )['series'][0]['points']

        self.assertEqual(pts, [[u'2001-01-01T00:00:00', 100.1], [u'2001-03-01T00:00:00', 0.0]])

    def test_excl_date_operator(self):
        job_id = self.job_id

        pts = self.get('/series', series_id=series_ids[1], max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T01:00:00', 20.1],
                                [u'2010-01-01T02:00:00', 30.1],
                                [u'2010-01-01T03:00:00', 40.1],
                                [u'2010-01-01T04:00:00', 0.1],
                                [u'2010-01-01T05:00:00', -80.0],
                                [u'2010-01-01T06:00:00', -2.0],
                                [u'2010-01-01T07:00:00', 10.0] ])

        pts = self.get('/series', series_id=series_ids[1], df='2010-01-01T02:00:00@excl', dt='2010-01-01T06:00:00',
                       max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T03:00:00', 40.1],
                                [u'2010-01-01T04:00:00', 0.1],
                                [u'2010-01-01T05:00:00', -80.0],
                                [u'2010-01-01T06:00:00', -2.0] ])

        pts = self.get('/series', series_id=series_ids[1], df='2010-01-01T02:00:00', dt='2010-01-01T06:00:00@excl',
                       max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T02:00:00', 30.1],
                                [u'2010-01-01T03:00:00', 40.1],
                                [u'2010-01-01T04:00:00', 0.1],
                                [u'2010-01-01T05:00:00', -80.0] ])

        pts = self.get('/series', series_id=series_ids[1], df='2010-01-01T02:00:00@excl', dt='2010-01-01T06:00:00@excl',
                       max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T03:00:00', 40.1],
                                [u'2010-01-01T04:00:00', 0.1],
                                [u'2010-01-01T05:00:00', -80.0] ])

        pts = self.get('/series', series_id="{}@df:{}".format(series_ids[1], '2010-01-01T02:00:00'), df='2010-01-01T02:00:00@excl',
                       dt='2010-01-01T06:00:00', max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T03:00:00', 40.1],
                                [u'2010-01-01T04:00:00', 0.1],
                                [u'2010-01-01T05:00:00', -80.0],
                                [u'2010-01-01T06:00:00', -2.0] ])

        pts = self.get('/series', series_id="{}@dt:{}".format(series_ids[1], '2010-01-01T06:00:00'), df='2010-01-01T02:00:00',
                       dt='2010-01-01T06:00:00@excl', max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T02:00:00', 30.1],
                                [u'2010-01-01T03:00:00', 40.1],
                                [u'2010-01-01T04:00:00', 0.1],
                                [u'2010-01-01T05:00:00', -80.0] ])

        pts = self.get('/series', series_id="{}@df:{}".format(series_ids[1], '2010-01-01T04:00:00'), df='2010-01-01T02:00:00@excl',
                       dt='2010-01-01T06:00:00', max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T04:00:00', 0.1],
                                [u'2010-01-01T05:00:00', -80.0],
                                [u'2010-01-01T06:00:00', -2.0] ])

        pts = self.get('/series', series_id="{}@dt:{}".format(series_ids[1], '2010-01-01T04:00:00'), df='2010-01-01T02:00:00',
                       dt='2010-01-01T06:00:00@excl', max_points=-1, date_format='iso')['series'][0]['points']
        self.assertEqual(pts, [ [u'2010-01-01T02:00:00', 30.1],
                                [u'2010-01-01T03:00:00', 40.1],
                                [u'2010-01-01T04:00:00', 0.1] ])


class TestRelativeDeltaOperatorsSjts(TestRelativeDeltaOperators):
    request_executor_cls = SjtsRequestExecutor


class TestOnDiskCache(TestCase):
    def test_cache(self):
        cached.clear()

        @cached(ttl=3.)
        def foo(*args, **kwargs):
            time.sleep(1.)
            return random.random()

        a = foo()
        b = foo()
        self.assertEqual(a, b)

        # should be different result in ttl sec
        time.sleep(4.)
        b = foo()
        self.assertNotEqual(a, b)

        # also different if we reset the cache
        a = foo()
        cached.clear()
        b = foo()
        self.assertNotEqual(a, b)

        cached.clear()

        # test with params
        args = 10.10, 'hey', datetime.utcnow(), {'some': {'nested': 'dict'}}
        kwargs = {
            'param1': 1.,
            'param2': datetime.utcnow(),
            'param3': {'some': {'other': {'nested': 'dict'}}}
        }
        a = foo(*args, **kwargs)
        b = foo(*args, **kwargs)
        c = foo('other', 'param')
        self.assertEqual(a, b)
        self.assertNotEqual(a, c)

        # should expire
        time.sleep(4.)

        b = foo(*args, **kwargs)
        self.assertNotEqual(a, b)

        cached.clear()
        a = foo(*args, **kwargs)
        cached.clear()
        b = foo(*args, **kwargs)
        self.assertNotEqual(a, b)


class TestInvokeFunc(FlaskTestCase):
    def test_invoke(self):
        # this should work b/c it correctly configured
        res = self.post('/source/test/invoke/source_function', data_json={
            'args': [10],
            'kwargs': {'b': 20},
            'vrt': {'source': 'test'}
        })['result']
        self.assertEqual(res['result'], 30)

        # this one is not wrapped by @invocable
        self.post('/source/test/invoke/source_function_non_invocable', data_json={
            'args': [10],
            'kwargs': {'b': 20},
            'vrt': {'source': 'test'}
        }, expect_error='invalid_parameters')

        # this is allowed for all
        res = self.post('/source/test/invoke/source_function_all_users', data_json={
            'args': [10],
            'vrt': {'source': 'test'}
        })['result']
        self.assertEqual(res['result'], 10)

        # this isn't allowed to anyone
        self.post('/source/test/invoke/source_function_for_no_one', data_json={
            'args': [10],
            'vrt': {'source': 'test'}
        }, expect_error='not_allowed')


class TestCallableRunner(FlaskTestCase):
    def setUp(self):
        self.request_executor = self.request_executor_cls(self)
        self.client = app.test_client(use_cookies=True)
        self.sj = Connection(config.REMOTE_URL, config.TEST_USER, config.TEST_API_KEY)
        with self.sj.register_job('virt test job') as job:
            job.delete_by_query('sid:sjvirt\\tests')
            for i, s in enumerate(test_data):
                job.put_fields('sjvirt\\tests\\{}'.format(i), s['fields'])
                if 'vrt' not in s['fields'] and 'xpr' not in s['fields']:
                    job.put_points('sjvirt\\tests\\{}'.format(i), [Point(parse(p[0]), p[1]) for p in s['points']])

        self.job_id = job.job_id
        self.sj.raw.delete('/series/cache')

        env = {
            'VIRT_CALLABLE_RUNNER_ACCOUNT': "http://localhost:5001",
            'VIRT_CALLABLE_RUNNER_USER': config.TEST_USER,
            'VIRT_CALLABLE_RUNNER_API_KEY': config.TEST_API_KEY,
            'VIRT_ENV': 'testing'
        }
        self.callable_runner = Popen([get_python_bin(), 'bin/callables_runner.py'],
                                     cwd=os.path.abspath(os.path.join(__file__, '../../')),
                                     env=env)
        self.addCleanup(self.callable_runner.kill)

        @retry(times=15, delay=2, backoff=1)
        def ping():
            requests.get("http://localhost:5016/ping")
        ping()

    def test_callable(self):

        code = '''
@sjutils.callable
def foo():
    return f'{sjclient.shooju_api.client._headers["Host"]} - {sjclient_direct.shooju_api.client._headers["Host"]}'

def not_callable():
    pass

@sjutils.callable
def double():
    return settings['val'] * settings['mult']

@sjutils.callable
def return_series_prefix():
    return settings['series_prefix']

@sjutils.callable(as_admin=False)
def run_as_non_admin():
    return "non_admin"


@sjutils.callable(as_admin=True)
def run_as_admin():
    return "admin"


@sjutils.callable(public=True)
def custom_public_func():
    return 5


@sjutils.callable(public=False)
def custom_non_public_func():
    return 5
    
def event_check():
    pass    
        '''
        importer_path = '/processors/test'
        series_prefix = 'test'
        self.post(importer_path, data_json={'description': 'something'})
        self.post(importer_path, data_json={'code': code,
                                            'save_description': 'init',
                                            'series_prefix': series_prefix,
                                            'settings': {'mult': 3, }})
        res = self.post('{}/call/foo'.format(importer_path))['result']
        self.assertEqual(res, 'localhost:5001 - local.localhost:5000')

        # using GET request
        res = self.get('{}/call/foo'.format(importer_path))['result']
        self.assertEqual(res, 'localhost:5001 - local.localhost:5000')

        self.post('{}/call/not_callable'.format(importer_path), expect_error='func_call')

        # using GET request
        self.get('{}/call/not_callable'.format(importer_path), expect_error='func_call')

        res = self.post('{}/call/double'.format(importer_path), data_json={'settings': {'val': 10}})['result']
        self.assertEqual(res, 30)

        # using GET request
        res = self.get('{}/call/double'.format(importer_path), data_json={'settings': {'val': 10}})['result']
        self.assertEqual(res, 30)

        res = self.post('{}/call/return_series_prefix'.format(importer_path), data_json={})['result']
        self.assertEqual(res, series_prefix)

        # using GET request
        res = self.get('{}/call/return_series_prefix'.format(importer_path), data_json={})['result']
        self.assertEqual(res, series_prefix)

        res = self.post('{}/call/run_as_non_admin'.format(importer_path), data_json={})['result']
        self.assertEqual(res, "non_admin")

        # using GET request
        res = self.get('{}/call/run_as_non_admin'.format(importer_path), data_json={})['result']
        self.assertEqual(res, "non_admin")

        res = self.post('{}/call/run_as_admin'.format(importer_path), data_json={}, expect_error='func_call')
        self.assertEqual(res['description'], "ImporterException: Run function as admin in sjvirt forbidden")

        # using GET request
        res = self.get('{}/call/run_as_admin'.format(importer_path), data_json={}, expect_error='func_call')
        self.assertEqual(res['description'], "ImporterException: Run function as admin in sjvirt forbidden")

        # public functions
        res = self.post('{}/call/custom_public_func'.format(importer_path), data_json={})['result']
        self.assertEqual(res, 5)

        res = self.post('{}/call/custom_non_public_func'.format(importer_path), data_json={})['result']
        self.assertEqual(res, 5)

        with patch.object(self.request_executor_cls, 'auth_headers', new_callable=PropertyMock) as mock_auth_headers:
            mock_auth_headers.return_value = []
            res = self.post('{}/call/custom_public_func'.format(importer_path), data_json={})['result']
            self.assertEqual(res, 5)

        with patch.object(self.request_executor_cls, 'auth_headers', new_callable=PropertyMock) as mock_auth_headers:
            mock_auth_headers.return_value = []
            res = self.post('{}/call/custom_non_public_func'.format(importer_path), data_json={}, expect_error='func_call')
            self.assertIn("function is not allowed to call", res['description'])


class TestExpressionSeriesRecursionCircuitBreaker(FlaskTestCase):
    def setUp(self):
        self.request_executor = self.request_executor_cls(self)
        self.client = app.test_client(use_cookies=True)
        self.sj = Connection(config.REMOTE_URL, config.TEST_USER, config.TEST_API_KEY)
        with self.sj.register_job('virt test job') as job:
            self.sj.raw.delete('/series/cache')
            job.delete_by_query('sid:sjvirt\\tests')
            self.sj.raw.delete('/series/cache')
            self.sj.raw.post('/series/refresh')
            job.put_fields('sjvirt\\tests\\xpr\\1', {'xpr': {'expression': '={{sid=sjvirt\\tests\\xpr\\1}}'}})

        self.sj.raw.delete('/series/cache')

        self._run_expression_runner()

    def test_expression_series_recursion_circuit_breaker(self):
        resp = self.get('/series', series_id='sjvirt\\tests\\xpr\\1', max_points=-1, expect_error='expression')
        self.assertEqual(resp['description'], 'request call depth exceeded limit')


class TestAsofAgainstXprSeries(FlaskTestCase):
    def setUp(self):
        self.request_executor = self.request_executor_cls(self)
        self.client = app.test_client(use_cookies=True)
        self.sj = Connection(config.REMOTE_URL, config.TEST_USER, config.TEST_API_KEY)
        self.sj.raw.delete('/series/cache')
        with self.sj.register_job('virt test job') as job:
            job.delete_by_query('sid:sjvirt\\tests')

        self.sj.raw.delete('/series/cache')
        self.sj.raw.post('/series/refresh')

        self._run_expression_runner()

    def test_asof_against_xpr_series(self):
        with self.sj.register_job('test') as job:
            job.put_fields('sjvirt\\tests\\xpr_asof\\1', {'xpr': {'expression': '=1'}})

        with self.sj.register_job('test') as job:
            job_id = job.job_id
            job.put_fields('sjvirt\\tests\\xpr_asof\\1', {'xpr': {'expression': '=2'}})

        with self.sj.register_job('test') as job:
            job.put_fields('sjvirt\\tests\\xpr_asof\\1', {'xpr': {'expression': '=3'}})

        # test latest
        resp = self.get('/series', series_id='sjvirt\\tests\\xpr_asof\\1',
                        max_points=-1, df='2020-01-01', dt='2020-02-01',
                        date_format='iso')['series'][0]
        self.assertEqual(resp['points'], [['2020-01-01T00:00:00', 3.0], ['2020-02-01T00:00:00', 3.0]])

        # points are failing
        self.get('/series', series_id=f'sjvirt\\tests\\xpr_asof\\1@asof:j{job_id}',
                 max_points=-1, df='2020-01-01', dt='2020-02-01',
                 date_format='iso', expect_error='expression')
        # but fields are supported
        resp = self.get('/series', series_id=f'sjvirt\\tests\\xpr_asof\\1@asof:j{job_id}', fields='*')['series'][0]
        self.assertEqual(resp['fields'], {'xpr': {'expression': '=2'}})


class TestDfDtWithPrePost(FlaskTestCase):
    def setUp(self):
        self.request_executor = self.request_executor_cls(self)
        self.client = app.test_client(use_cookies=True)
        self.sj = Connection(config.REMOTE_URL, config.TEST_USER, config.TEST_API_KEY)
        self.sj.raw.delete('/series/cache')
        with self.sj.register_job('virt test job') as job:
            job.delete_by_query('sid:sjvirt\\tests')

        self.sj.raw.delete('/series/cache')
        self.sj.raw.post('/series/refresh')

        self._run_expression_runner()

    def test_df_dt_with_pre_post(self):
        g_code = '''
from datetime import timedelta, timezone

def add_point_at(series, dt, value: float):
    series[dt] = value
    return series

def repeat_last_point_at(series, dt):
    localized_dt = dt.replace(tzinfo=series.index.tz)
    return add_point_at(series, localized_dt, series[-1])

G.repeat_last_point_at = repeat_last_point_at
        '''
        objs = [
            {
                "fields": {
                    'xpr': {
                        'Expression': r'''=G.repeat_last_point_at({{id_field=SERIES_1}}, dt(2013,1,2)).resample('H').ffill()''',
                        'G': g_code
                    }
                },
                "points": {}
            },
            {
                "fields": {'timezone': 'America/Chicago', 'id_field': 'SERIES_1'},
                "points": [
                    Point(datetime(2013, 1, 1), 1),
                    Point(datetime(2013, 1, 1, 12), 6)
                ],
            },
        ]
        series_ids = [r'sjvirt\test_pre_post\{}'.format(i) for i in range(2)]
        with self.sj.register_job('test') as job:
            for sid, obj in zip(series_ids, objs):
                job.put_fields(sid, obj['fields'])
                job.put_points(sid, obj['points'])
        self.sj.raw.post('/series/refresh')
        resp = self.get('/series', series_id=series_ids[0], max_points=-1, date_format='iso', )['series'][0]['points']
        self.assertEqual(resp, [['2013-01-01T00:00:00', 1.0],
                                ['2013-01-01T01:00:00', 1.0],
                                ['2013-01-01T02:00:00', 1.0],
                                ['2013-01-01T03:00:00', 1.0],
                                ['2013-01-01T04:00:00', 1.0],
                                ['2013-01-01T05:00:00', 1.0],
                                ['2013-01-01T06:00:00', 1.0],
                                ['2013-01-01T07:00:00', 1.0],
                                ['2013-01-01T08:00:00', 1.0],
                                ['2013-01-01T09:00:00', 1.0],
                                ['2013-01-01T10:00:00', 1.0],
                                ['2013-01-01T11:00:00', 1.0],
                                ['2013-01-01T12:00:00', 6.0],
                                ['2013-01-01T13:00:00', 6.0],
                                ['2013-01-01T14:00:00', 6.0],
                                ['2013-01-01T15:00:00', 6.0],
                                ['2013-01-01T16:00:00', 6.0],
                                ['2013-01-01T17:00:00', 6.0],
                                ['2013-01-01T18:00:00', 6.0],
                                ['2013-01-01T19:00:00', 6.0],
                                ['2013-01-01T20:00:00', 6.0],
                                ['2013-01-01T21:00:00', 6.0],
                                ['2013-01-01T22:00:00', 6.0],
                                ['2013-01-01T23:00:00', 6.0],
                                ['2013-01-02T00:00:00', 6.0]])

        # filter by @post
        resp = self.get('/series',
                        series_id='{}@post@df:2013-01-01T04:00:00@dt:2013-01-01T21:00:00'.format(series_ids[0]),
                        max_points=-1, date_format='iso', )['series'][0]['points']

        self.assertEqual(resp, [['2013-01-01T04:00:00', 1.0],
                                ['2013-01-01T05:00:00', 1.0],
                                ['2013-01-01T06:00:00', 1.0],
                                ['2013-01-01T07:00:00', 1.0],
                                ['2013-01-01T08:00:00', 1.0],
                                ['2013-01-01T09:00:00', 1.0],
                                ['2013-01-01T10:00:00', 1.0],
                                ['2013-01-01T11:00:00', 1.0],
                                ['2013-01-01T12:00:00', 6.0],
                                ['2013-01-01T13:00:00', 6.0],
                                ['2013-01-01T14:00:00', 6.0],
                                ['2013-01-01T15:00:00', 6.0],
                                ['2013-01-01T16:00:00', 6.0],
                                ['2013-01-01T17:00:00', 6.0],
                                ['2013-01-01T18:00:00', 6.0],
                                ['2013-01-01T19:00:00', 6.0],
                                ['2013-01-01T20:00:00', 6.0],
                                ['2013-01-01T21:00:00', 6.0]])

        # the same with @pre will give differnet result
        resp = self.get('/series',
                        series_id='{}@pre@df:2013-01-01T04:00:00@dt:2013-01-01T21:00:00'.format(series_ids[0]),
                        max_points=-1, date_format='iso', )['series'][0]['points']

        self.assertEqual(resp, [['2013-01-01T12:00:00', 6.0],
                                ['2013-01-01T13:00:00', 6.0],
                                ['2013-01-01T14:00:00', 6.0],
                                ['2013-01-01T15:00:00', 6.0],
                                ['2013-01-01T16:00:00', 6.0],
                                ['2013-01-01T17:00:00', 6.0],
                                ['2013-01-01T18:00:00', 6.0],
                                ['2013-01-01T19:00:00', 6.0],
                                ['2013-01-01T20:00:00', 6.0],
                                ['2013-01-01T21:00:00', 6.0],
                                ['2013-01-01T22:00:00', 6.0],
                                ['2013-01-01T23:00:00', 6.0],
                                ['2013-01-02T00:00:00', 6.0]])

        # test max points
        resp = self.get('/series', series_id='{}@post@max_points:1'.format(series_ids[0]),
                        max_points=-1, date_format='iso', )['series'][0]['points']
        self.assertEqual(resp, [['2013-01-01T00:00:00', 1.0]])

        resp = self.get('/series', series_id='{}@pre@max_points:1@df:MAX@dt:MIN'.format(series_ids[0]),
                        max_points=-1, date_format='iso', )['series'][0]['points']
        self.assertEqual(resp, [['2013-01-01T12:00:00', 6.0],
                                ['2013-01-01T13:00:00', 6.0],
                                ['2013-01-01T14:00:00', 6.0],
                                ['2013-01-01T15:00:00', 6.0],
                                ['2013-01-01T16:00:00', 6.0],
                                ['2013-01-01T17:00:00', 6.0],
                                ['2013-01-01T18:00:00', 6.0],
                                ['2013-01-01T19:00:00', 6.0],
                                ['2013-01-01T20:00:00', 6.0],
                                ['2013-01-01T21:00:00', 6.0],
                                ['2013-01-01T22:00:00', 6.0],
                                ['2013-01-01T23:00:00', 6.0],
                                ['2013-01-02T00:00:00', 6.0]])
        # this returns last 2 points
        resp = self.get('/series', series_id='{}@pre@max_points:1@df:MAX@dt:MIN'.format(series_ids[0]),
                        max_points=2, date_format='iso', df='MAX', dt='MIN')['series'][0]['points']
        self.assertEqual(resp, [['2013-01-02T00:00:00', 6.0], ['2013-01-01T23:00:00', 6.0]])

        # just checking if the same works if we use xpr inside expression
        resp = self.get('/series',
                        series_id='={{%s}}@post@df:2013-01-01T04:00:00@dt:2013-01-01T21:00:00' % series_ids[0],
                        max_points=-1, date_format='iso', )['series'][0]['points']
        self.assertEqual(resp, [['2013-01-01T04:00:00', 1.0],
                                ['2013-01-01T05:00:00', 1.0],
                                ['2013-01-01T06:00:00', 1.0],
                                ['2013-01-01T07:00:00', 1.0],
                                ['2013-01-01T08:00:00', 1.0],
                                ['2013-01-01T09:00:00', 1.0],
                                ['2013-01-01T10:00:00', 1.0],
                                ['2013-01-01T11:00:00', 1.0],
                                ['2013-01-01T12:00:00', 6.0],
                                ['2013-01-01T13:00:00', 6.0],
                                ['2013-01-01T14:00:00', 6.0],
                                ['2013-01-01T15:00:00', 6.0],
                                ['2013-01-01T16:00:00', 6.0],
                                ['2013-01-01T17:00:00', 6.0],
                                ['2013-01-01T18:00:00', 6.0],
                                ['2013-01-01T19:00:00', 6.0],
                                ['2013-01-01T20:00:00', 6.0],
                                ['2013-01-01T21:00:00', 6.0]])

        resp = self.get('/series',
                        series_id='={{%s}}@pre@df:2013-01-01T04:00:00@dt:2013-01-01T21:00:00' % series_ids[0],
                        max_points=-1, date_format='iso', )['series'][0]['points']

        self.assertEqual(resp, [['2013-01-01T12:00:00', 6.0],
                                ['2013-01-01T13:00:00', 6.0],
                                ['2013-01-01T14:00:00', 6.0],
                                ['2013-01-01T15:00:00', 6.0],
                                ['2013-01-01T16:00:00', 6.0],
                                ['2013-01-01T17:00:00', 6.0],
                                ['2013-01-01T18:00:00', 6.0],
                                ['2013-01-01T19:00:00', 6.0],
                                ['2013-01-01T20:00:00', 6.0],
                                ['2013-01-01T21:00:00', 6.0],
                                ['2013-01-01T22:00:00', 6.0],
                                ['2013-01-01T23:00:00', 6.0],
                                ['2013-01-02T00:00:00', 6.0]])

        with self.sj.register_job('virt test job') as job:
            job.delete_by_query('sid:sjvirt\\tests')
