"""
The application monitoring script.

This collects and writes containers metrics to Shooju.
It also pings Shooju host and writes responses latency and whether was it successful.
To disable it just don't pass either of the following env vars:
- VIRT_MON_DOCKER_URL
- VIRT_MON_ACCOUNT_URL
- VIRT_MON_WRITER_USER
- VIRT_MON_WRITER_API_KEY
- VIRT_MON_HOSTNAME
"""
from dataclasses import dataclass
import docker
import os
from datetime import datetime
from threading import Lock
from dateutil.parser import parse

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

from sjtoolkit.utils.decorators import retry
from sjtoolkit.utils.series import flattify
from shooju import Point, Connection
import requests
import time
import logging
import re

log_level = (os.environ.get('VIRT_LOG_LEVEL') or 'INFO').upper()

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=log_level)
logger = logging.getLogger('monitor')
logger.setLevel(log_level)

DOCKER_HOST = os.environ.get('VIRT_MON_DOCKER_URL')  # Url to docker host to pull containers stats
ACCOUNT_URL = os.environ.get('VIRT_MON_ACCOUNT_URL')  # Shooju account url to send metrics to

# Shooju user/key that will be writing metrics
USER = os.environ.get('VIRT_MON_WRITER_USER')
API_KEY = os.environ.get('VIRT_MON_WRITER_API_KEY')

# Hostname to distinguish metrics
MONITOR_HOSTNAME = os.environ.get('VIRT_MON_HOSTNAME')

assert DOCKER_HOST and ACCOUNT_URL and USER and API_KEY and MONITOR_HOSTNAME, "Required config is missing"

client = docker.DockerClient(DOCKER_HOST)
COLLECT_DOCKER_METRICS_CRON = CronTrigger(minute='*')  # collect docker stats every 15 sec
COLLECT_NETWORK_METRICS_CRON = CronTrigger(minute='*')  # make ping call every 15 sec

sj = Connection(ACCOUNT_URL, USER, API_KEY)
try:
    sj.raw.get('/ping')
except Exception as e:
    raise Exception(f'failed to connect to Shooju: {e}')

# check docker api is working
try:
    client.ping()
except Exception as e:
    raise Exception(f'unable to connect to docker host: {e}')


def collect_docker_metrics():
    docker_metrics = list()
    for container in client.containers.list():
        docker_metrics.append({
            'container': container.name,
            'stats': container.stats(stream=False),
        })
    sj.raw.post(
        '/processors/util_metrics/call/report_docker_metrics', data_json={
            'settings': {
                'docker_metrics': docker_metrics
            }
        })


def collect_network_metrics():
    dt = datetime.utcnow()
    _s = time.perf_counter()
    try:
        resp = requests.get(f'{ACCOUNT_URL}/ping', timeout=30.).content
        ok = int(resp == b'ok')
    except Exception:
        ok = 0
    finally:
        latency = time.perf_counter() - _s
    sj.raw.post(
        '/processors/util_metrics/call/report_latency_metrics', data_json={
            'settings': {
                'ts_milli': dt.timestamp() * 1000,
                'ok': ok,
                'latency': latency,
            }
        })


if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    scheduler.add_job(collect_docker_metrics, COLLECT_DOCKER_METRICS_CRON, coalesce=True)
    scheduler.add_job(collect_network_metrics, COLLECT_NETWORK_METRICS_CRON, coalesce=True)
    scheduler.start()
    logging.info('monitoring started')
    while True:
        time.sleep(60)
