from gevent.monkey import patch_all
patch_all()
import os
from virt.metrics import init_metrics_reporting

# need to init before uwsgi forked
init_metrics_reporting(
    'virt_expression_runner',
    os.environ.get('EXP_RUNNER_ACCOUNT'),
    os.environ.get('EXP_RUNNER_USER'),
    os.environ.get('EXP_RUNNER_API_KEY'),
)

from sjtoolkit.bin.expression_runner import app, serve_app

if __name__ == "__main__":
    serve_app()
