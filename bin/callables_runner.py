from gevent.monkey import patch_all

patch_all()

import imp
import json
import jwt
import logging
import msgpack
import os
import tempfile
import time
import sys

from shooju import Connection, Point, options

from flask import Flask, request, send_file, g
from sjtoolkit.utils.logging import CallablesLogHandler
from sjtoolkit.exceptions import ExpressionException, ImporterException, NotAllowedException
from sjtoolkit.utils.sjutils import BaseSJUtils, AllowedToCall, BytesCallResponse, ReturnBytesFunc
from sjtoolkit.utils.utils import get_request_auth, id_generator
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
from virt.metrics import metrics, init_metrics_reporting
from sjtoolkit.expressions.trace import SimpleApiCallMetricsHook

from virt import config
import requests

from diskcache.core import Cache

options.return_series_errors = True

app = Flask(__name__)

log_level = (os.environ.get('VIRT_LOG_LEVEL') or 'INFO').upper()
stderr_log_handler = logging.StreamHandler(stream=sys.stderr)
stderr_log_handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger = logging.getLogger('callable_runner')
logger.setLevel(log_level)
logger.addHandler(stderr_log_handler)

account = os.environ.get('VIRT_CALLABLE_RUNNER_ACCOUNT')
user = os.environ.get('VIRT_CALLABLE_RUNNER_USER')
api_key = os.environ.get('VIRT_CALLABLE_RUNNER_API_KEY')
sjvirt_shared_secret = os.environ.get('VIRT_SHARED_SECRET')

assert account and user and api_key, 'In order to run, must set CALLABLE_RUNNER_ACCOUNT, ' \
                                     'CALLABLE_RUNNER_USER and CALLABLE_RUNNER_API_KEY env variables'

processor_cache_size = int(os.environ.get('VIRT_CALLABLE_CACHE_SIZE', '1073741824'))
processor_cache_ttl = float(os.environ.get('VIRT_CALLABLE_CACHE_TTL', '300'))
processor_cache = Cache(tempfile.gettempdir(), size_limit=processor_cache_size)

JWT_TOKEN_MAX_DELTA = 60. * 5
CONNECTION_FAILURE_RETRY = 3

init_metrics_reporting('virt_callables_runner', config.REMOTE_URL, user, api_key)


def retry_policy():
    return Retry(
        CONNECTION_FAILURE_RETRY,
        status_forcelist={502, 503, 504},
        method_whitelist={
            'HEAD', 'GET', 'PUT', 'DELETE', 'OPTIONS', 'TRACE', 'POST'
        }, backoff_factor=2
    )


def make_session():
    _session = requests.Session()
    _session.mount('http://', HTTPAdapter(max_retries=retry_policy()))
    _session.mount('https://', HTTPAdapter(max_retries=retry_policy()))
    _session.headers.update({
        'Sj-Virt-Call': 'true'
    })
    return _session


class DataStore(object):

    def __getattribute__(self, name):
        raise NotImplementedError('datastore is not available in sjvirt')


def load_processor_namespace(global_ns, loads_proc_obj, session, import_path=None):
    """
    Loads existing libs code into importer global namespace.
    :param loads_proc_obj: Array of processors to load into current namespace.
    :param sj: Connection to account to load processors from
    :param import_path: helper array to detect recursive imports
    """
    import_path = import_path or list()
    processor_ns = dict(global_ns)
    if not loads_proc_obj:
        return processor_ns

    revision_by_proc_id = {p['proc_id']: p.get('rev_job_id') for p in loads_proc_obj}
    proc_ids = revision_by_proc_id.keys()
    query = 'sid:system\\importers l3=({})'.format(','.join(proc_ids))
    sj = Connection(account, user, api_key, requests_session=session)

    libs = [p for p in sj.raw.get('/processors', params={
        'per_page': 10000,
        'query': query,
        'fields': 'l3,code,loads_proc_obj',
    })['results']]
    libs = [lib for lib in libs if 'code' in lib]

    does_not_exist = set(proc_ids).difference(set(i['id'].lower() for i in libs))
    if does_not_exist:
        raise ImportError('could not load importers: {}'.format(','.join(does_not_exist)))

    for lib in libs:
        lib_id = lib['id']

        if lib_id in import_path:
            # generating good message to help find recursive import
            msg = '"{}" could not import "{}" - recursive import detected (import chain: {})'.format(
                import_path[-1], lib_id, ' > '.join(import_path + [lib_id]))
            raise ImporterException(msg)

        code = lib['code']
        lib_module = imp.new_module(lib_id)  # create empty module

        # filling lib namespace
        lib_loads_proc_obj = lib.get('loads_proc_obj') or list()
        if lib_loads_proc_obj:
            # recursively imports dependencies
            lib_module_ns = load_processor_namespace(global_ns, lib_loads_proc_obj,
                                                     session=session, import_path=import_path + [lib_id])
        else:
            lib_module_ns = dict(global_ns)
        lib_module.__dict__.update(lib_module_ns)
        importer_code = compile(code, '<{}>'.format(lib_id), 'exec')

        exec(importer_code, lib_module.__dict__)
        processor_ns[lib_id.upper()] = lib_module
    return processor_ns


@app.route('/call', methods=('POST',))
def call():
    """
    Executes processors function that wrapped by @sjutils.callable
    """
    params = dict(json.loads(request.form['payload']))
    params.update({k: v.read() for k, v in request.files.items()})

    processor_id = params.get('processor_id')
    func = params.get('func')
    call_settings = params.get('settings') or dict()

    try:
        username, password = get_request_auth(request)
    except NotAllowedException:
        username, password = None, None
    auth = username, password
    caller_user = username

    logger.debug(f'calling {processor_id} {func} with settings: \n{call_settings}')
    session = make_session()
    sj = Connection(account, user, api_key,
                    requests_session=session, timeout=config.SJCLIENT_DIRECT_TIMEOUT)

    processor_cache_key = processor_id + '_' + func
    processor_data = processor_cache.get(processor_cache_key)
    if processor_data is None:
        processor_data = dict(id=processor_id, **sj.raw.get('/processors/{}'.format(processor_id))['importer'])
        processor_cache.set(processor_cache_key, processor_data, expire=processor_cache_ttl)

    if username:
        sjclient = Connection(account, *auth, requests_session=session,
                              timeout=config.SJCLIENT_DIRECT_TIMEOUT,
                              hooks=[SimpleApiCallMetricsHook(destination='app')])
        sjclient_direct = Connection(config.REMOTE_URL,
                                     *auth,
                                     timeout=config.SJCLIENT_DIRECT_TIMEOUT,
                                     requests_session=session,
                                     hooks=[SimpleApiCallMetricsHook(destination='upstream')])
    else:
        sjclient = None
        sjclient_direct = None

    call_logger = logging.Logger('callable_runner')  # creating directly to not waist logger system
    call_logger.setLevel(log_level)
    call_logger.addHandler(stderr_log_handler)

    loads_proc_obj = processor_data.get('loads_proc_obj') or []
    if 'code' not in processor_data:
        return msgpack.dumps({'success': False,
                              'error': 'func_call',
                              'description': "Unable to load processor {}".format(processor_id)})

    datastore = DataStore()
    settings = processor_data.get('settings') or dict()
    settings['series_prefix'] = processor_data.get('series_prefix')
    settings.update(call_settings)

    global_ns = {
        'logger': call_logger,
        'sjclient': sjclient,
        'sjclient_direct': sjclient_direct,
        'datastore': datastore,
        'Point': Point,
        'settings': settings,
        'return_bytes': ReturnBytesFunc('', params.get('callable_post_headers')),
        'ImporterException': ImporterException,
        'ExpressionException': ExpressionException,
        'tempfile': tempfile,
        'importer': processor_data,
        'caller_user': caller_user,
        'callable_post_body': params.get('callable_post_body'),
        'callable_post_headers': params.get('callable_post_headers'),
        'callable_url_params': params.get('callable_url_params'),
        'callable_post_json': params.get('callable_post_json'),
    }

    global_ns['sjutils'] = BaseSJUtils(global_ns)
    remote_logs_handler = None

    try:
        ns = load_processor_namespace(global_ns, loads_proc_obj,
                                      session=session, import_path=[processor_id])
        code = compile(processor_data['code'], '<importer>', 'exec')  # compiling bytecode
        exec(code, ns)
        if func not in ns:
            raise ImporterException('unknown function "{}"'.format(func))

        f = ns[func]

        if not isinstance(f, AllowedToCall):
            raise ImporterException('"{}" function is not allowed to call'.format(func))

        if not username and not f.public:
            raise ImporterException('"{}" function is not allowed to call by anonymous user'.format(func))

        if f.as_admin:
            raise ImporterException("Run function as admin in sjvirt forbidden")

        if sjvirt_shared_secret and f.collect_logs:
            call_id = id_generator()
            remote_logs_handler = CallablesLogHandler(
                sj=sjclient_direct, processor_id=processor_id,
                function=func, user=caller_user, request_id=call_id,
                token=_generate_logs_token()
            )
            call_logger.addHandler(remote_logs_handler)

        success, res = True, ns[func]()
    except Exception as e:
        call_logger.exception(f'failed to execute {processor_id}.{func}')
        metrics.count('error', error_type=f'callable_exec', error=type(e).__name__)
        success, res = False, u'{}: {}'.format(type(e).__name__, e)
    finally:
        session.close()

    if remote_logs_handler is not None:
        remote_logs_handler.ensure_logs_sent()

    if success:
        if not isinstance(res, BytesCallResponse):
            return msgpack.dumps({'success': success, 'result': res})

        response = send_file(res.payload, 'application/octet-stream')
        for k, v in (res.headers or {}).items():
            if v is not None:
                response.headers[f'Callable-Binary-Response-Header-{k}'] = v

        response.headers[f'Callable-Binary-Response'] = 'true'
        return response
    else:
        return msgpack.dumps({'success': success, 'error': 'func_call', 'description': str(res)})


@app.before_request
def before_request():
    g.request_timer = metrics.timer('request')
    if request.path == "/ping":
        return "OK"

@app.after_request
def stop_timer(resp):
    _stop_timer()
    return resp

def _stop_timer():
    if hasattr(g, 'request_timer') and g.request_timer:
        g.request_timer.stop()
        g.request_timer = None

def _generate_logs_token():
    return jwt.encode({'source': 'virt_callables_runner', 'exp': time.time() + JWT_TOKEN_MAX_DELTA},
                      sjvirt_shared_secret)


def serve_app(runner_app=app, port=5016):
    from gevent.pywsgi import WSGIServer
    if sjvirt_shared_secret is None:
        logger.warning('SjVirt shared secret is not set. Logs will not be sent to Shooju.')

    http_server = WSGIServer(('', port), runner_app)
    logger.info('started expression runner. listening at 0.0.0.0:{}'.format(port))
    http_server.serve_forever()


if __name__ == '__main__':
    serve_app()
