from setuptools import setup

setup(
    name="sjvirt",
    version="0.1",
    packages=["virt", "tests", "sources"],
)
